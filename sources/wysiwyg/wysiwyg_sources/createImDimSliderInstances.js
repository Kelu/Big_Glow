
inlets = 1;
outlets = 1;
var bpatcherInstances = [];
var length = 37;
var height = 125;
var xPosInit = 40;
var yPosInit = 500;
var xPos = xPosInit;
var yPos = yPosInit;

function genSliders(nbChannels)
{
	delBpatchers();
	genBpatchers(nbChannels);
	outlet(0, "bang");
}

function genBpatchers(nbInstances)
{
	arrayLen = bpatcherInstances.length;
	
	for(var i=arrayLen+1;i<=nbInstances;i++)
	{
			
		bpatcherInstances[i-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "channelImageDimSlider",
			"@patching_rect", xPos, yPos, length, height,
			//"@presentation_rect", xPosPres, yPosPres, length, height,
			"@border", "0",
			//"@varname", varname,
			"@args", i,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
		
		xPos += 40;
	}
}

function delBpatchers()
{
	for(var i=1;i<=bpatcherInstances.length;i++)
	{
		this.patcher.remove(bpatcherInstances[i-1])
	}
	xPos = xPosInit
	yPos = yPosInit
	bpatcherInstances = []
}

function getNbIntances()
{
	outlet(0, bpatcherInstances.length);
}

