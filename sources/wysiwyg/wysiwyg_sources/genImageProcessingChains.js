
var abstractionsInstances = [];
var xPos = 16;
var yPos = 170;

function genPatch(nbChannels)
{
	delObjects();
	genAbstractions(nbChannels);
	yPos += 35;
	outlet(0,nbChannels);
}

function genAbstractions(nbInstances)
{	
	for(var i=1;i<=nbInstances;i++)
	{		
		abstractionsInstances[i-1] = this.patcher.newdefault(xPos, yPos, "imageProcessChain", i);
		
		yPos += 35;
	}
}

function delObjects()
{
	for(var i=1; i<=abstractionsInstances.length; i++)
	{
		this.patcher.remove(abstractionsInstances[i-1]);
	}
	abstractionsInstances = [];
	xPos = 16;
	yPos = 170;
}

