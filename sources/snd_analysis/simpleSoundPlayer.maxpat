{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 998.0, 303.0, 503.0, 622.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 293.0, 527.156799, 110.0, 22.0 ],
					"style" : "",
					"text" : "send~ testSoundR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 170.0, 527.156799, 108.0, 22.0 ],
					"style" : "",
					"text" : "send~ testSoundL"
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 29.0,
					"data" : 					{
						"clips" : [ 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/115kv dragon.mp3",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/115kv dragon-converted.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/19486__halleck__jacobsladdersingle1.flac",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/101334__timbre__remix-of-101127-cgeffex-bug-zapper-long-moth-electrocution-remix.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/103687__chimerical__melt-down-1.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/163621__wacapou__2-dern-mix.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/248251__jameswrowles__electrocution.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/341611__pureaudioninja__electricity-3.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/393822__wakerone__electric-arc-sparks.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/396240__weaveofkev__welding-electric-sparks (1).wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/396240__weaveofkev__welding-electric-sparks.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/396511__kev-durr__arc-welding.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/398274__flashtrauma__electricity.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/425327__kev-durr__electric-shock.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/449013__parabolix__jacobs-ladder-from-side.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
, 							{
								"filename" : "Macintosh HD:/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/arc_elec.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-11",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 261.0, 22.0, 150.0, 480.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 129.0, 187.0, 34.0, 22.0 ],
					"style" : "",
					"text" : "/~ 2."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 129.0, 159.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "+~"
				}

			}
, 			{
				"box" : 				{
					"hidden" : 1,
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 6.0, 251.0, 89.0, 22.0 ],
					"style" : "",
					"text" : "sw sspGainVal"
				}

			}
, 			{
				"box" : 				{
					"hidden" : 1,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 109.75, 251.0, 89.0, 22.0 ],
					"style" : "",
					"text" : "sw sspGainVal"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 70.75, 159.0, 25.0, 78.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "gain~",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 38.0, 159.0, 25.0, 78.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"hidden" : 1,
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 38.0, 10.884056, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.454902, 0.462745, 0.482353, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.454902, 0.462745, 0.482353, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-5",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 38.0, 47.0, 57.0, 22.0 ],
					"style" : "",
					"text" : "loop 1, 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 129.0, 219.156799, 131.0, 22.0 ],
					"style" : "",
					"text" : "send~ testSoundMono"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 38.0, 291.281799, 51.75, 51.75 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"data" : 					{
						"clips" : [ 							{
								"filename" : "/Users/Kelu/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds/115kv dragon-converted.wav",
								"filekind" : "audiofile",
								"loop" : 0,
								"content_state" : 								{
									"originaltempo" : [ 120.0 ],
									"originallengthms" : [ 0.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"speed" : [ 1.0 ],
									"slurtime" : [ 0.0 ],
									"timestretch" : [ 0 ],
									"formant" : [ 1.0 ],
									"followglobaltempo" : [ 0 ],
									"play" : [ 0 ],
									"formantcorrection" : [ 0 ],
									"basictuning" : [ 440 ],
									"quality" : [ "basic" ],
									"mode" : [ "basic" ],
									"pitchcorrection" : [ 0 ],
									"pitchshift" : [ 1.0 ]
								}

							}
 ]
					}
,
					"id" : "obj-2",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 38.0, 83.0, 150.0, 30.0 ],
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"hidden" : 1,
					"source" : [ "obj-10", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 1 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"order" : 2,
					"source" : [ "obj-11", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 0,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-11", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"order" : 1,
					"source" : [ "obj-11", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 1,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 2,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"hidden" : 1,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"hidden" : 1,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"order" : 1,
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 1 ],
					"order" : 0,
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 1,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"hidden" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"hidden" : 1,
					"source" : [ "obj-9", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "115kv dragon-converted.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "sw.maxpat",
				"bootpath" : "/Users/Shared/Max 7/Examples/max-tricks/send-receive-tricks/lib",
				"patcherrelativepath" : "../../../../Shared/Max 7/Examples/max-tricks/send-receive-tricks/lib",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "115kv dragon.mp3",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "Mp3 ",
				"implicit" : 1
			}
, 			{
				"name" : "19486__halleck__jacobsladdersingle1.flac",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "FLAC",
				"implicit" : 1
			}
, 			{
				"name" : "101334__timbre__remix-of-101127-cgeffex-bug-zapper-long-moth-electrocution-remix.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "103687__chimerical__melt-down-1.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "163621__wacapou__2-dern-mix.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "248251__jameswrowles__electrocution.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "341611__pureaudioninja__electricity-3.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "393822__wakerone__electric-arc-sparks.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "396240__weaveofkev__welding-electric-sparks (1).wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "396240__weaveofkev__welding-electric-sparks.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "396511__kev-durr__arc-welding.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "398274__flashtrauma__electricity.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "425327__kev-durr__electric-shock.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "449013__parabolix__jacobs-ladder-from-side.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "arc_elec.wav",
				"bootpath" : "~/BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"patcherrelativepath" : "../../../BGXP/2020/kilowatt/sons_arc_electrique/orig_snds",
				"type" : "WAVE",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBrown-1",
				"default" : 				{
					"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjCyan-1",
				"default" : 				{
					"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "panelViolet",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.372549, 0.196078, 0.486275, 0.2 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 1.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
