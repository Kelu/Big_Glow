{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 312.0, 340.0, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 173.0, 4.0, 150.0, 20.0 ],
					"style" : "",
					"text" : "sound sources"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 309.75, 10.0, 71.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 309.75, 32.0, 71.0, 20.0 ],
					"style" : "",
					"text" : "source 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 82.0, 10.0, 71.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 82.0, 32.0, 71.0, 20.0 ],
					"style" : "",
					"text" : "source 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 236.75, 162.0, 83.0, 21.0 ],
					"style" : "",
					"text" : "send~ source2"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-28",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "spat.input~.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 236.75, 32.0, 217.0, 125.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 236.75, 54.0, 217.0, 125.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 9.0, 162.0, 83.0, 21.0 ],
					"style" : "",
					"text" : "send~ source1"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-7",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "spat.input~.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 9.0, 32.0, 217.0, 125.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 9.0, 54.0, 217.0, 125.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.862745, 0.684242, 0.170953, 0.518091 ],
					"id" : "obj-4",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 368.0, 236.0, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -1.0, 1.0, 474.0, 194.0 ],
					"proportion" : 0.39,
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-28::obj-2" : [ "live.gain~", " ", 0 ],
			"obj-28::obj-12::obj-4" : [ "live.numbox", "live.numbox", 0 ],
			"obj-28::obj-23" : [ "live.toggle", "live.toggle", 0 ],
			"obj-7::obj-46::obj-6" : [ "live.text[4]", "live.text[1]", 0 ],
			"obj-28::obj-46::obj-36" : [ "select folder[1]", "select folder", 0 ],
			"obj-7::obj-26::obj-56" : [ "live.button[2]", "live.button[1]", 0 ],
			"obj-28::obj-43::obj-8" : [ "live.dial", "freq", 0 ],
			"obj-28::obj-26::obj-59" : [ "live.numbox[1]", "live.numbox[1]", 0 ],
			"obj-7::obj-46::obj-36" : [ "select folder[2]", "select folder", 0 ],
			"obj-7::obj-2" : [ "live.gain~[1]", " ", 0 ],
			"obj-28::obj-5::obj-12" : [ "live.button", "live.button", 0 ],
			"obj-7::obj-5::obj-12" : [ "live.button[3]", "live.button", 0 ],
			"obj-7::obj-182" : [ "live.text[5]", "live.text[1]", 0 ],
			"obj-28::obj-1" : [ "live.menu", "live.menu", 0 ],
			"obj-7::obj-1" : [ "live.menu[1]", "live.menu", 0 ],
			"obj-7::obj-43::obj-8" : [ "live.dial[1]", "freq", 0 ],
			"obj-7::obj-12::obj-4" : [ "live.numbox[3]", "live.numbox", 0 ],
			"obj-28::obj-26::obj-56" : [ "live.button[1]", "live.button[1]", 0 ],
			"obj-7::obj-23" : [ "live.toggle[3]", "live.toggle", 0 ],
			"obj-28::obj-182" : [ "live.text[1]", "live.text[1]", 0 ],
			"obj-7::obj-26::obj-59" : [ "live.numbox[2]", "live.numbox[1]", 0 ],
			"obj-7::obj-26::obj-58" : [ "live.toggle[2]", "live.toggle[1]", 0 ],
			"obj-28::obj-26::obj-58" : [ "live.toggle[1]", "live.toggle[1]", 0 ],
			"obj-28::obj-46::obj-6" : [ "live.text[3]", "live.text[1]", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "spat.input~.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/ircam-spat/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
