/* This program allows you to set DMX channels over the serial port.
**
** After uploading to Arduino, switch to Serial Monitor and set the baud rate
** to 9600. You can then set DMX channels using these commands:
**
** <number>c : Select DMX channel
** <number>w : Set DMX channel to new value
**
** These can be combined. For example:
** 100c355w : Set channel 100 to value 255.
**
** For more details, and compatible Processing sketch,
** visit http://code.google.com/p/tinkerit/wiki/SerialToDmx
**
** Help and support: http://groups.google.com/group/DmxMaster */

#include <DmxMaster.h>
#include <math.h>
#include "DmxOut.h"
#include "BG_Utils.h"
#include "CommonChasers.h"


// global control parameters :
const bool SIMULATION = true;
const int nbLights = 36;
CommonChasers cc(nbLights, SIMULATION);

// programs management :
int programChoice = 4;

// some variables needed by the various function :
bool doInit = true;
int channel;
int prevChannel = 1;
int startTime;
int fps = 33; // frames per second = number of update per second to poll values for continuous effects like sinusoid

void setup() {
  // standard baud rates are 1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
  Serial.begin(9600);
  //Serial.println("SerialToDmx ready");
  cc.turnAllLightsOff();
}

void loop() {
  // TODO : 
  // - choisir le programme avec des boutons poussoirs
  // - régler la vitesse avec un potard

  int maxValue = 255;
  int minValue = 0;
  int timeStep = 250; // à contrôler avec un potard

  float freq = 0.5;
  boolean directionRight = true;
  float nbPeriods = 1.;

  // fixed value :
  if(programChoice==0){
    cc.turnAllLightsOff();
  }

  // fixed value :
  if(programChoice==1){
    cc.fixedValue(maxValue);
  }

  // simple chaser :
  else if(programChoice==2){
    bool directionRight = true; // if true, chaser goes from left to right; else, from right to left.
    cc.simpleChaser(timeStep, directionRight, minValue, maxValue);
  }
  
  // simple chaser random :
  else if(programChoice==3){
     cc.simpleChaserRandom(timeStep, minValue, maxValue);
  }
  
  // chaser random with multiple lights changing with a random dimmer value at each step :
  else if(programChoice==4){
    int nbLightsPerStep = 5;
    cc.randomChaserMultipleLightsPerStep(nbLightsPerStep, timeStep, minValue, maxValue);
  }
  
  // flickering around a mean value :
  else if(programChoice==5){
    int meanValue = 150;
    cc.flickering(meanValue, minValue, maxValue);
  }
  
  // any chaser from pre-programmed sequence:
  else if(programChoice==6){
    cc.anyChaser(timeStep, minValue, maxValue);
  }
  
  // strobe :
  else if(programChoice==7){
    timeStep = 125; // à contrôler avec un potard
    cc.strobe(timeStep, minValue, maxValue);
  }

  /*// test read 2d array from txt file :
  else if(programChoice==7){
    String** outArray;
    String fileName = "/Users/Kelu/Big_Glow/sources/arduino_sketch/chasers/testchaser.txt";
    importCSV(fileName, outArray);
  }*/

  /*
  // sinusoid :
  else if(programChoice==8){
    sinusoid(nbLights, freq, nbPeriods, minValue, maxValue, directionRight);
  }

  // TODO : verify and finish the following functions :
  //sawtooth :
  else if(programChoice==9){
    sawtooth(nbLights, freq, nbPeriods, minValue, maxValue, directionRight);
  }

  //triangle :
  else if(programChoice==10){
    freq = 0.5;
    nbPeriods = 2.;
    directionRight = false;
    triangle(nbLights, freq, nbPeriods, minValue, maxValue, directionRight);
  }

  //square :
  else if(programChoice==11){
    float width = 0.5;
    freq = 0.5;
    nbPeriods = 1.;
    square(nbLights, width, freq, nbPeriods, minValue, maxValue, directionRight);
  }
  */
}
