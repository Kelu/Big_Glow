/*
 * Define some chasers and effects that may be suitable for any configuration (independantly of number of lights and positions)
 */

/*
 * WARNNING!! This file is not up to date anymore!! => see directly file CommonChasers.cpp to modify the library 
 * (libraries are cpp only, .ino file is not used)
 */
 
#include "Arduino.h"
#include "BG_Utils.h"
#include "CommonChasers.h"
#include "DmxOut.h"

// TODO: make nbLight a global/shared variable (in constructor)

DmxOut dmxOut;
BG_Utils utils;

CommonChasers::CommonChasers(int nbLights, bool simulation){
  _nbLights = nbLights;
  _SIMULATION = simulation;
  _prevChannel = 0;
  _minIntensityVal = 0;
  _maxIntensityVal = 255;
  _timeStep = 250;
  _fps = 33;
  _channelsToDmxAddressArray;
}

void CommonChasers::setTimeStep(int timeStep){
  _timeStep = timeStep;
}

void CommonChasers::setMinIntensityVal(int minIntensityVal){
  _minIntensityVal = minIntensityVal;
}

int CommonChasers::getMinIntensityVal(){
  return _minIntensityVal;
}

void CommonChasers::setMaxIntensityVal(int maxIntensityVal){
  _maxIntensityVal = _maxIntensityVal;
}

int CommonChasers::getMaxIntensityVal(){
  return _maxIntensityVal;
}

void CommonChasers::fixedValue(int value){
  int channel;
  for(channel=1; channel<=_nbLights; channel++){
    dmxOut.writeDMXOutput(channel, value, _SIMULATION);
  }
}

void CommonChasers::turnAllLightsOff(){
  fixedValue(0);
}

void CommonChasers::simpleChaser(int timeStep, bool directionRight, int minValue, int maxValue){
  // TODO : implémenter option aller-retour
  int channel;
  
  if(directionRight){ // chaser goes from left to right
    _prevChannel = _nbLights;
  } else { // chaser goes from right to left
    _prevChannel = 1;
  }

  int i;
  for(i=0; i<_nbLights; i++){
    if(directionRight){
      channel = i+1;
    } else {
      channel = _nbLights - i;
    }
    
    dmxOut.writeDMXOutput(channel, maxValue, _SIMULATION);
    dmxOut.writeDMXOutput(_prevChannel, minValue, _SIMULATION);
  
    delay(_timeStep);
    _prevChannel = channel;
  }
}

void CommonChasers::simpleChaserRandom(int timeStep, int minValue, int maxValue){  
  int channel = random(1,_nbLights+1);
  while(channel==_prevChannel){
    channel = random(1,_nbLights+1);
  }
  dmxOut.writeDMXOutput(channel, maxValue, _SIMULATION);
  dmxOut.writeDMXOutput(_prevChannel, minValue, _SIMULATION);
  _prevChannel = channel;
  delay(_timeStep);
}

void CommonChasers::randomChaserMultipleLightsPerStep(int nbLightsPerStep, int timeStep, int minValue, int maxValue){
  int channel;
  int value;
  int selectedChannels[nbLightsPerStep];
  // init with no selected channels :
  int i;
  for(i=0; i<nbLightsPerStep; i++){
    selectedChannels[i] = -1; // -1 means no channel value selected yet
  }
  // pick randomly nbLightsPerStep different channels and pick random dimmer value for each :
  for(i=0; i<nbLightsPerStep; i++){
    channel = random(1,_nbLights+1);
    while(utils.valueIsInArray(selectedChannels, nbLightsPerStep, channel)){
      channel = random(1,_nbLights+1);
    }
    selectedChannels[i] = channel;
    value = random(minValue,maxValue+1);
    dmxOut.writeDMXOutput(channel, value, _SIMULATION);
  }
  delay(_timeStep);
}

void CommonChasers::flickering(int meanValue, int minValue, int maxValue){
  int timeStep = 40;
  int deviation = 20;
  int lowValue = max(meanValue - deviation, minValue);
  int highValue = min(meanValue + deviation, maxValue);
  randomChaserMultipleLightsPerStep(_nbLights, timeStep, lowValue, highValue);
}

void CommonChasers::anyChaser(int timeStep, int minValue, int maxValue){
  // TODO : implement possibility to read chaser from a text file
  // TODO : implémenter option aller-retour
  int value;
  /*boolean chaser[4][10]={
  1,1,0,0,0,0,0,0,1,1,
  0,0,1,1,0,0,1,1,0,0,
  0,0,0,0,1,1,0,0,0,0,
  0,0,1,1,0,0,1,1,0,0};
  */
  
  //uneSurDeux :
  boolean chaser[2][10]={
  1,0,1,0,1,0,1,0,1,0,
  0,1,0,1,0,1,0,1,0,1};
  
  int i;
  int j;
  int channel;
  int nbSteps = sizeof(chaser)/sizeof(chaser[0]);
  int nbLightsPerStep = sizeof(chaser[0])/sizeof(chaser[0][0]);
  for(i=0; i<nbSteps; i++){
    for(j=0; j<nbLightsPerStep; j++){
      channel = j+1;
      if(chaser[i][j]==1){
        value = maxValue;
      } else {
        value = minValue;
      }
      dmxOut.writeDMXOutput(channel, value, _SIMULATION);
    }
    delay(_timeStep);
  }
}

void CommonChasers::strobe(int timeStep, int minValue, int maxValue){ 
  int channel; 
  for(channel=1; channel<=_nbLights; channel++){
    dmxOut.writeDMXOutput(channel, maxValue, _SIMULATION);
  }
  delay(timeStep);

  for(channel=1; channel<=_nbLights; channel++){
    dmxOut.writeDMXOutput(channel, minValue, _SIMULATION);
  }
  delay(timeStep);
}

void CommonChasers::sinusoid(float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float t;
  float channelPhase;
  float sinVal;
  int value;
  t = millis()/1000.;
  for(channel=1; channel<=_nbLights; channel++){
    if(directionRight){
      channelPhase = (nbPeriods*2*PI - ((channel-1)*nbPeriods*2*PI)/_nbLights);
    } else {
      channelPhase = (channel-1)*nbPeriods*PI/_nbLights;
    }
    sinVal = (sin(2*PI*freq*t+channelPhase)+1)/2.;
    value = int(round(max(sinVal*(maxValue-minValue)+minValue, minValue)));
    dmxOut.writeDMXOutput(channel, value, _SIMULATION);
  }
  delay(1000/_fps);
}
