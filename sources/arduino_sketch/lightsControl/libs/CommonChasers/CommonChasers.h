/*
 * Define some chasers and effects that may be suitable for any configuration (independantly of number of lights and positions)
 */

#ifndef CommonChasers_h
#define CommonChasers_h

#include "Arduino.h"
#include "DmxOut.h"

class CommonChasers
{
  public:
    CommonChasers(int nbLights, bool simulation);
    void setTimeStep(int timeStep);
    int getTimeStep();
    void setMinIntensityVal(int minIntensityVal);
    int getMinIntensityVal();
    void setMaxIntensityVal(int maxIntensityVal);
    int getMaxIntensityVal();
    void setEffectParam1(float effectParam1);
    float getEffectParam1();
    int* getChannelsToDmxAddressArray();
    void setChannelsToDmxAddressArray(int* channelsToDmxAddressArray);
    bool getBreakFunction();
    void fixedValue(int value);
    void fixedSawTooth(int value, int shift, float nbPer, bool reversed);
    void sawToothWave(int value, bool reversed);
    void turnAllLightsOff();
    void simpleChaser(bool directionRight);
    void simpleChaserRandom();
    void randomChaserMultipleLightsPerStep(int nbLightsPerStep, bool onOffMode);
    void flickering(int meanValue);
    void anyChaser(bool **chaser, int nbSteps, int nbLightsPerStep, bool AR, float speedFactor);
    void anyChaser_fadeTest(bool **chaser, int nbSteps, int nbLightsPerStep, bool AR, float speedFactor);
    void strobe();
    void sinusoid(float freq, float nbPeriods, boolean directionRight);
    void sinRectInterp(float freq, float nbPeriods, boolean directionRight, float interpFactor, float threshold);
    void readSequence(int dmxSequence[][3], int nbSteps);
    void breakFunction();
  private:
    int _nbLights;
    bool _SIMULATION;
    int _prevChannel;
    int _minIntensityVal;
    int _maxIntensityVal;
    int _timeStep;
    float _effectParam1 = 0.;
    unsigned long _timer;
    int _fps;
    unsigned long _dmxOutTime;
    float _channelPhase;
    int* _channelsToDmxAddressArray;
    int* _currentValues;
    bool _breakFunction;
};

#endif
