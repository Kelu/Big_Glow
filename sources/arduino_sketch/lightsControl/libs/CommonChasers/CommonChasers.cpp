/*
 * Define some chasers and effects that may be suitable for any configuration (independantly of number of lights and positions)
 */
 
// TODO: 
//  - build a fade-out (and fade-in?) machanism with a fadeTime parameter (or both _fadeInTime and _fadeOutTime)
//  - fix change of speed during sinusoid or sinRectInterp effect

#include "Arduino.h"
#include "BG_Utils.h"
#include "CommonChasers.h"
#include "DmxOut.h"

DmxOut dmxOut;
BG_Utils utils;

CommonChasers::CommonChasers(int nbLights, bool simulation){
  _nbLights = nbLights;
  _SIMULATION = simulation;
  _prevChannel = 0;
  _minIntensityVal = 0;
  _maxIntensityVal = 255;
  _timeStep = 250;
  _effectParam1 = 0.;
  _timer = millis();
  _fps = 33;
  _dmxOutTime = millis();
  _channelsToDmxAddressArray = new int[_nbLights];
  for(int i; i<_nbLights; i++){
   _channelsToDmxAddressArray[i] = i+1; // initialize array in normal order by default
  }
  _currentValues = new int[_nbLights]; // to retain current values of each dmx channel
  for(int i; i<_nbLights; i++){
   _currentValues[i] = 0; // initialize array with all values set to 0
  }
  _channelPhase = 0.;
  dmxOut.setSimulationState(_SIMULATION);
  _breakFunction = false; // variable to break from running function from interrupt
}

void CommonChasers::breakFunction(){
  _breakFunction = true;
}

void CommonChasers::setTimeStep(int timeStep){
  _timeStep = timeStep;
}

int CommonChasers::getTimeStep(){
  return _timeStep;
}

void CommonChasers::setMinIntensityVal(int minIntensityVal){
  if(minIntensityVal!=_minIntensityVal){
    _minIntensityVal = minIntensityVal;
    dmxOut.setMinIntensityVal(_minIntensityVal);
    for(int i; i<_nbLights; i++){
      dmxOut.writeDMXOutput(_channelsToDmxAddressArray[i], _currentValues[i]);
    }
  }
}

int CommonChasers::getMinIntensityVal(){
  return _minIntensityVal;
}

void CommonChasers::setMaxIntensityVal(int maxIntensityVal){
  if(maxIntensityVal!=_maxIntensityVal){
    _maxIntensityVal = maxIntensityVal;
    dmxOut.setMaxIntensityVal(_maxIntensityVal);
    for(int i; i<_nbLights; i++){
      dmxOut.writeDMXOutput(_channelsToDmxAddressArray[i], _currentValues[i]);
    }
  }
}

int CommonChasers::getMaxIntensityVal(){
  return _maxIntensityVal;
}

void CommonChasers::setEffectParam1(float effectParam1){
  _effectParam1 = effectParam1;
}

float CommonChasers::getEffectParam1(){
  return _effectParam1;
}

int* CommonChasers::getChannelsToDmxAddressArray(){
  return _channelsToDmxAddressArray;
}

void CommonChasers::setChannelsToDmxAddressArray(int* channelsToDmxAddressArray){
  _channelsToDmxAddressArray = channelsToDmxAddressArray;
}

bool CommonChasers::getBreakFunction(){
  return _breakFunction;
}

void CommonChasers::fixedValue(int value){
  int channel;
  for(channel=1; channel<=_nbLights; channel++){
    _currentValues[channel-1] = value;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
  }
}

void CommonChasers::fixedSawTooth(int value, int shift, float nbPer, bool reversed){
  int channel;
  for(channel=1; channel<=_nbLights; channel++){
  	//float idx = (((channel-1)+_nbLights-shift)%_nbLights);
  	float idx = float(int(((channel-1)*nbPer+(_nbLights*nbPer)-shift)*100)%(_nbLights*100))/100.; // use 100 factor as the % operator only works with int and I want a float
  	int targetValue = value*idx/(_nbLights-1);
    if(reversed){
      targetValue = value - targetValue;
  	}
    _currentValues[channel-1] = targetValue;
	dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], targetValue);
  }
}

void CommonChasers::sawToothWave(int value, bool reversed){
  for(int i=0; i<_nbLights; i++){
    float nbPer = utils.map(_effectParam1, 0., 1., 1., _nbLights/4.); // use knob _effectParam1 to control number of periods
    fixedSawTooth(value, i*nbPer, nbPer, reversed);
    //delay(_timeStep);
    while(millis()-_timer<_timeStep){
      if(_breakFunction){ // if one should break this function in order to call another programm
        turnAllLightsOff(); // turn all lights off before starting a new effect
        _timer = millis();
        _breakFunction = false;
        return;
      }
      // otherwise do nothing ...
    }
    _timer = millis();
  }
}

void CommonChasers::turnAllLightsOff(){ // set all channels to value 0 (light off)
  int channel;
  for(channel=1; channel<=_nbLights; channel++){
    _currentValues[channel-1] = 0;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], 0);
  }
}

void CommonChasers::simpleChaser(bool directionRight){
  // TODO : implémenter option aller-retour
  int channel;
  
  if(directionRight){ // chaser goes from left to right
    _prevChannel = _nbLights;
  } else { // chaser goes from right to left
    _prevChannel = 1;
  }

  int i;
  for(i=0; i<_nbLights; i++){
    if(directionRight){
      channel = i+1;
    } else {
      channel = _nbLights - i;
    }

    _currentValues[channel-1] = 255; // _maxIntensityVal;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], 255);
    _currentValues[_prevChannel-1] = 0; //_minIntensityVal;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[_prevChannel-1], 0);
  
    _prevChannel = channel;
    //delay(_timeStep);
    while(millis()-_timer<_timeStep){ // replace "delay" function by measuring time difference usign millis(), so that the delay time can be adjusted continuously when changing this parameter with a knob
      if(_breakFunction){ // if one should break this function in order to call another programm
        turnAllLightsOff(); // turn all lights off before starting a new effect
        _timer = millis();
        _breakFunction = false;
        return;
      }
      // otherwise do nothing ...
    }
    _timer = millis();
  }
}

void CommonChasers::simpleChaserRandom(){  
  int channel = random(1,_nbLights+1);
  while(channel==_prevChannel){
    channel = random(1,_nbLights+1);
  }
  
  _currentValues[channel-1] = 255; //_maxIntensityVal;
  dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], 255);
  _currentValues[_prevChannel-1] = 0; //_minIntensityVal;
  dmxOut.writeDMXOutput(_channelsToDmxAddressArray[_prevChannel-1], 0);
  
  _prevChannel = channel;
  //delay(_timeStep);
  while(millis()-_timer<_timeStep){
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
    // otherwise do nothing ...
  }
  _timer = millis();
}

void CommonChasers::randomChaserMultipleLightsPerStep(int nbLightsPerStep, bool onOffMode){
  int channel;
  int value;
  int selectedChannels[nbLightsPerStep];
  // init with no selected channels :
  int i;
  for(i=0; i<nbLightsPerStep; i++){
    selectedChannels[i] = -1; // -1 means no channel value selected yet
  }
  // pick randomly nbLightsPerStep different channels and pick random dimmer value for each :
  for(i=0; i<nbLightsPerStep; i++){
    channel = random(1,_nbLights+1);
    while(utils.valueIsInArray(selectedChannels, nbLightsPerStep, channel)){
      channel = random(1,_nbLights+1);
    }
    selectedChannels[i] = channel;
    //value = random(_minIntensityVal, _maxIntensityVal+1);
    if(onOffMode){
      // value = random(0, 2)*255;
      value = (255 - _currentValues[channel-1]); // if light on turn it off, and if light off turn it on
    } else {
      value = random(0, 255);
    }
    _currentValues[channel-1] = value;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
  }

  //delay(_timeStep);
  while(millis()-_timer<_timeStep){
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
    // otherwise do nothing ...
  }
  _timer = millis();
}

void CommonChasers::flickering(int meanValue){
  //meanValue = (_minIntensityVal + _maxIntensityVal)/2; // TODO : better to keep as function parameter or to compute like this?
  int flickeringTimeStep = 40;
  int deviation = 20;
  int lowValue = max(meanValue - deviation, _minIntensityVal);
  int highValue = min(meanValue + deviation, _maxIntensityVal);
  
  int channel;
  int value;
  for(channel=0; channel<_nbLights; channel++){
    value = random(lowValue, highValue+1);
    _currentValues[channel-1] = value;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
  }
  //delay(flickeringTimeStep);  
  while(millis()-_timer<flickeringTimeStep){
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
    // otherwise do nothing ...
  }
  _timer = millis();
}

void CommonChasers::anyChaser(bool **chaser, int nbSteps, int nbLightsPerStep, bool AR, float speedFactor){
  //Serial.print("in anyChaser\n");
  // TODO : implémenter fade-time
    // TODO : read chaser array from PROGMEM to reduce dynamic memory usage
  //float _effectParam1Old = _effectParam1;
  int value;
  
  int fadeTime = 1000;
  
  /*
  boolean chaser[4][10]={{1,1,0,0,0,0,0,0,1,1},
  						 {0,0,1,1,0,0,1,1,0,0},
  						 {0,0,0,0,1,1,0,0,0,0},
  						 {0,0,1,1,0,0,1,1,0,0}};
  */
  
  //uneSurDeux :
  /*
  boolean chaser[2][10]={{1,0,1,0,1,0,1,0,1,0},
						  {0,1,0,1,0,1,0,1,0,1}};*/
  
  int i;
  int j;
  int channel;
  //int nbSteps = sizeof(chaser)/sizeof(chaser[0]);
  //int nbLightsPerStep = sizeof(chaser[0])/sizeof(chaser[0][0]);
  
  for(i=0; i<nbSteps; i++){
    for(j=0; j<nbLightsPerStep; j++){
      /*if(_effectParam1!=_effectParam1Old){ // if chaser is changed, return to start new chaser immediately
        _effectParam1Old = _effectParam1;
        return;
      }*/
  	  
      channel = j+1;
      /*if(chaser[i][j]==1){
        value = 255; //_maxIntensityVal;
      } else {
        value = 0; //_minIntensityVal;
      }*/
      //value = chaser[i][j]*255;
      value = pgm_read_byte_near(chaser[i]+j)*255; // read array from progmem
      //int fadeValue = value * ((millis()-_timer)/float(min(fadeTime, _timeStep)));
      //_currentValues[channel-1] = fadeValue;
      _currentValues[channel-1] = value;
      //dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], fadeValue);
      dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
    }
    //delay(_timeStep);
    while(millis()-_timer<_timeStep/speedFactor){
      /*for(j=0; j<nbLightsPerStep; j++){
      	value = chaser[i][j]*255;
      	int fadeValue = value * ((millis()-_timer)/float(min(fadeTime, _timeStep)));
      	dmxOut.writeDMXOutput(_channelsToDmxAddressArray[j], fadeValue); //value);
      }*/
      if(_breakFunction){ // if one should break this function in order to call another program
        //Serial.println("break function from anyChaser!!");
        turnAllLightsOff(); // turn all lights off before starting a new effect
        _timer = millis();
        _breakFunction = false;
        return;
      }
      // otherwise do nothing ...
    }
    _timer = millis();
  }
  if(AR){
    for(i=nbSteps-2; i>0; i--){ // run the chaser in reverse order
      for(j=0; j<nbLightsPerStep; j++){
        /*if(_effectParam1!=_effectParam1Old){ // if chaser is changed, return to start new chaser immediately
          _effectParam1Old = _effectParam1;
          return;
        }*/
  	    
        channel = j+1;
        //if(chaser[i][j]==1){
        if(pgm_read_byte_near(chaser[i]+j)==1){
          value = 255; //_maxIntensityVal;
        } else {
          value = 0; //_minIntensityVal;
        }
        _currentValues[channel-1] = value;
        dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
      }
      //delay(_timeStep);
      while(millis()-_timer<_timeStep/speedFactor){
        if(_breakFunction){ // if one should break this function in order to call another program
          //Serial.println("break function from anyChaser!!");
          turnAllLightsOff(); // turn all lights off before starting a new effect
          _timer = millis();
          _breakFunction = false;
          return;
        }
        // otherwise do nothing ...
      }
      _timer = millis();
    }
  }
}

void CommonChasers::anyChaser_fadeTest(bool **chaser, int nbSteps, int nbLightsPerStep, bool AR, float speedFactor){
  // TODO : implémenter fade-time
  //float _effectParam1Old = _effectParam1;
  
  bool targetOnOffValues[_nbLights];
  for(int i_init=0; i_init<_nbLights; i_init++){
  	targetOnOffValues[i_init] = 0;
  }
  
  int value;
  
  int fadeTime = 1000;
  int fadeOutTimeStamp = millis();
  
  /*
  boolean chaser[4][10]={{1,1,0,0,0,0,0,0,1,1},
  						 {0,0,1,1,0,0,1,1,0,0},
  						 {0,0,0,0,1,1,0,0,0,0},
  						 {0,0,1,1,0,0,1,1,0,0}};
  */
  
  //uneSurDeux :
  /*
  boolean chaser[2][10]={{1,0,1,0,1,0,1,0,1,0},
						  {0,1,0,1,0,1,0,1,0,1}};*/
  
  int i;
  int j;
  int channel;
  //int nbSteps = sizeof(chaser)/sizeof(chaser[0]);
  //int nbLightsPerStep = sizeof(chaser[0])/sizeof(chaser[0][0]);
  
  for(i=0; i<nbSteps; i++){
    for(j=0; j<nbLightsPerStep; j++){
      /*if(_effectParam1!=_effectParam1Old){ // if chaser is changed, return to start new chaser immediately
        _effectParam1Old = _effectParam1;
        return;
      }*/
  	  
      channel = j+1;
      /*if(chaser[i][j]==1){
        value = 255; //_maxIntensityVal;
      } else {
        value = 0; //_minIntensityVal;
      }*/
      targetOnOffValues[j] = chaser[i][j];
      value = chaser[i][j]*255;
      //int fadeValue = value * ((millis()-_timer)/float(min(fadeTime, _timeStep)));
      //_currentValues[channel-1] = fadeValue;
      //_currentValues[channel-1] = value;
      //dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], fadeValue);
      dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
    }
    //delay(_timeStep);
    while(millis()-_timer<float(_timeStep)/speedFactor){
      for(j=0; j<nbLightsPerStep; j++){
      	if(targetOnOffValues[j]==0){
      	  //value = chaser[i][j]*255;
      	  //int fadeValue = value * ((millis()-_timer)/float(min(fadeTime, _timeStep)));
      	  int fadeOutTimeStep = millis()-fadeOutTimeStamp;
      	  int fadeValue = max(0, _currentValues[j]-0);//int(255*float(fadeOutTimeStep)/float(fadeTime)));
      	  _currentValues[j] = fadeValue;
      	  dmxOut.writeDMXOutput(_channelsToDmxAddressArray[j], fadeValue); //value);
      	}
      }
      fadeOutTimeStamp = millis();
      if(_breakFunction){ // if one should break this function in order to call another programm
        turnAllLightsOff(); // turn all lights off before starting a new effect
        _timer = millis();
        _breakFunction = false;
        return;
      }
      // otherwise do nothing ...
    }
    _timer = millis();
  }
  if(AR){
    for(i=nbSteps-2; i>0; i--){ // run the chaser in reverse order
      for(j=0; j<nbLightsPerStep; j++){
        /*if(_effectParam1!=_effectParam1Old){ // if chaser is changed, return to start new chaser immediately
          _effectParam1Old = _effectParam1;
          return;
        }*/
  	    
        channel = j+1;
        if(chaser[i][j]==1){
          value = 255; //_maxIntensityVal;
        } else {
          value = 0; //_minIntensityVal;
        }
        _currentValues[channel-1] = value;
        dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
      }
      //delay(_timeStep);
      while(millis()-_timer<_timeStep/speedFactor){
        if(_breakFunction){ // if one should break this function in order to call another programm
          turnAllLightsOff(); // turn all lights off before starting a new effect
          _timer = millis();
          _breakFunction = false;
          return;
        }
        // otherwise do nothing ...
      }
      _timer = millis();
    }
  }
}

void CommonChasers::strobe(){ 
  int channel; 
  for(channel=1; channel<=_nbLights; channel++){
    _currentValues[channel-1] = 255; //_maxIntensityVal;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], 255);
  }
  //delay(_timeStep);
  while(millis()-_timer<_timeStep){
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
    // otherwise do nothing ...
  }
  _timer = millis();

  for(channel=1; channel<=_nbLights; channel++){
    _currentValues[channel-1] = 0; //_minIntensityVal;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], 0);
  }
  //delay(_timeStep);
  while(millis()-_timer<_timeStep){
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
    // otherwise do nothing ...
  }
  _timer = millis();
}

void CommonChasers::sinusoid(float freq, float nbPeriods, boolean directionRight){
  float t;
  float channelPhase;
  float sinVal;
  int value;
  t = millis()/1000.;
  for(int channel=1; channel<=_nbLights; channel++){
    if(directionRight){
      channelPhase = (nbPeriods*2*PI - ((channel-1)*nbPeriods*2*PI)/_nbLights);
    } else {
      channelPhase = (channel-1)*nbPeriods*PI/_nbLights;
    }
    sinVal = (sin(2*PI*freq*t+channelPhase)+1)/2.;
    value = sinVal*255; //formerlly was : int(round(max(sinVal*(_maxIntensityVal-_minIntensityVal)+_minIntensityVal, _minIntensityVal))); => now the mapping done already in writeDMXOutput
    _currentValues[channel-1] = value;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
  }
  //delay(1000/_fps);
  while(millis()-_timer<1000/_fps){
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
    // otherwise do nothing ...
  }
  _timer = millis();
}

/*void CommonChasers::sinRectInterp(float freq, float nbPeriods, boolean directionRight, float interpFactor, float threshold){
  // variant of sinusoid, interpolated with a rectangular wave, using both an interpolation factor and a threshold parameter
  float t;
  float channelPhase;
  float sinValue;
  float rectValue;
  float interpValue;
  int value;
  t = millis()/1000.;
  for(int channel=1; channel<=_nbLights; channel++){
    if(directionRight){
      channelPhase = (nbPeriods*2*PI - ((channel-1)*nbPeriods*2*PI)/_nbLights);
    } else {
      channelPhase = (channel-1)*nbPeriods*PI/_nbLights;
    }
    sinValue = (sin(2*PI*freq*t+channelPhase)+1)/2.;
    rectValue = float(sinValue>threshold);
    interpValue = (1-interpFactor)*sinValue + interpFactor*rectValue;
    value = interpValue*255;
    _currentValues[channel-1] = value;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
  }
  
  //delay(1000/_fps);
  while(millis()-_timer<1000/_fps){
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
    // otherwise do nothing ...
  }
  _timer = millis();
}*/

void CommonChasers::sinRectInterp(float freq, float nbPeriods, boolean directionRight, float interpFactor, float threshold){
  // variant of sinusoid, interpolated with a rectangular wave, using both an interpolation factor and a threshold parameter
  float t;
  float initChannelPhase;
  float sinValue;
  float rectValue;
  float interpValue;
  int value;
  float dt = (millis() - _dmxOutTime) / 1000.; // time, in s, since the last output
  
  _channelPhase += (2*PI*freq*dt);
  if(_channelPhase>2*PI){ // modulo 2PI
    _channelPhase = _channelPhase - 2*PI;
  }
  
  for(int channel=1; channel<=_nbLights; channel++){
    if(directionRight){
      initChannelPhase = (nbPeriods*2*PI - ((channel-1)*nbPeriods*2*PI)/_nbLights);
    } else {
      initChannelPhase = (channel-1)*nbPeriods*PI/_nbLights;
    }
    sinValue = (sin(_channelPhase+initChannelPhase)+1)/2.;
    rectValue = float(sinValue>threshold);
    interpValue = (1-interpFactor)*sinValue + interpFactor*rectValue;
    value = interpValue*255;
    _currentValues[channel-1] = value;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], value);
  }
  _dmxOutTime = millis();
  
  //delay(1000/_fps);
  while(millis()-_timer<1000/_fps){
    if(_breakFunction){ // if one should break this function in order to call another programm
      turnAllLightsOff(); // turn all lights off before starting a new effect
      _timer = millis();
      _breakFunction = false;
      return;
    }
    // otherwise do nothing ...
  }
  _timer = millis();
}

/*void CommonChasers::readSequence(){
  //int nbSteps = 10;
  //int delaysSteps[nbSteps] = {0, 1000, 250, 250, 1000, 2000, 250, 250, 1000, 1000};
  //int channelsSteps[nbSteps] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  //int valuesSteps[nbSteps] = {255, 100, 255, 200, 255, 255, 20, 255, 50, 255};
  int nbSteps = 18;
  int delaysSteps[nbSteps] = {0, 0, _timeStep, 0, _timeStep, 0, _timeStep, 0, _timeStep, 0, _timeStep, 0, _timeStep, 0, _timeStep, 0, _timeStep, 0};
  int channelsSteps[nbSteps] = {1, 18, 2, 17, 3, 16, 4, 15, 5, 14, 6, 13, 7, 12, 8, 11, 9, 10};
  int valuesSteps[nbSteps] = {255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255};

  for(int channel=1; channel<=_nbLights; channel++){ // reset all lights to 0 before restarting the sequence
    _currentValues[channel-1] = 0;
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channel-1], 0);
  }
  
  delay(1000);
  
  _timer = millis();
  
  for(int i=0; i<nbSteps; i++){
    while(millis()-_timer<delaysSteps[i]){
      if(_breakFunction){ // if one should break this function in order to call another programm
        turnAllLightsOff(); // turn all lights off before starting a new effect
        _timer = millis();
        _breakFunction = false;
        return;
      }
      // otherwise do nothing ...
    }
    _timer = millis();
    _currentValues[i] = valuesSteps[i];
    dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channelsSteps[i]-1], valuesSteps[i]);
  }
  
  delay(1000);

}*/

void CommonChasers::readSequence(int dmxSequence[][3], int nbSteps){
  int delaysSteps = dmxSequence[0];
  int channelsSteps = dmxSequence[1];
  int valuesSteps = dmxSequence[2];
  int minRandVal;
  int maxRandVal;
  int minRandDelayStep;
  int maxRandDelayStep;
  float randomizationFactor;

  turnAllLightsOff(); // reset all lights to 0 before restarting the sequence
  
  _timer = millis();
  
  for(int stepId=0; stepId<nbSteps; stepId++){
    randomizationFactor = _effectParam1; // read it inside the loop, as it can be modified by the timer interrupt anytime
    int delaysStep = dmxSequence[stepId][0]; // get delay time
    if(delaysStep==-1){ // if not defined (-1), replace by _timeStep variable (for speed control with knob)
      delaysStep = _timeStep;
    }
    int channelsStep = dmxSequence[stepId][1];
    int valuesStep = dmxSequence[stepId][2];

    if(channelsStep==-1){ // if -1, set value for all lights
      for(int lightId=0; lightId<_nbLights; lightId++){
        _currentValues[lightId] = valuesStep;
        dmxOut.writeDMXOutput(_channelsToDmxAddressArray[lightId], valuesStep);
      }
    } else { // set channel to given value
    
      minRandVal = max(0, valuesStep-int(255*randomizationFactor));
      maxRandVal = min(255, valuesStep+int(255*randomizationFactor));
      valuesStep = random(minRandVal,maxRandVal+1);
      _currentValues[channelsStep-1] = valuesStep;
      dmxOut.writeDMXOutput(_channelsToDmxAddressArray[channelsStep-1], valuesStep);
    }
    
    minRandDelayStep = max(0, delaysStep-int(delaysStep*randomizationFactor));
    maxRandDelayStep = min(2*delaysStep, delaysStep+int(delaysStep*randomizationFactor));
    delaysStep = random(minRandDelayStep, maxRandDelayStep);
    while(millis()-_timer<delaysStep){ // wait for given time delay before going to next step
      if(_breakFunction){ // if one should break this function in order to call another programm
        turnAllLightsOff(); // turn all lights off before starting a new effect
        _timer = millis();
        _breakFunction = false;
        return;
      }
      // otherwise do nothing ...
    }
    _timer = millis();
  }
}
