/*
 * Utility functions
 */

#ifndef BG_Utils_h
#define BG_Utils_h

#include "Arduino.h"

class BG_Utils
{
  public:
    BG_Utils();
    bool valueIsInArray(int arr[], int sizeOfArray, int valueToBeTested);
    float BG_Utils::map(float x, float in_min, float in_max, float out_min, float out_max) ;
    //private:
    //int dummy;;
};

#endif
