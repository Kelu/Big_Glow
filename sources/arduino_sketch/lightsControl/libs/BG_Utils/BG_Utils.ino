/*
 * Utility functions
 */

#include "Arduino.h"
#include "BG_Utils.h"

BG_Utils::BG_Utils(){
  //nothing else to do here into the constructor;
}

boolean BG_Utils::valueIsInArray(int arr[], int sizeOfArray, int valueToBeTested){
  for (int i=0; i<sizeOfArray; i++){
    if (arr[i] == valueToBeTested){
      return true;
    }
  }
  return false;
}

