/*
 * Utility functions
 */

#include "Arduino.h"
#include "BG_Utils.h"

BG_Utils::BG_Utils(){
  //nothing else to do here into the constructor;
}

boolean BG_Utils::valueIsInArray(int arr[], int sizeOfArray, int valueToBeTested){
  for (int i=0; i<sizeOfArray; i++){
    if (arr[i] == valueToBeTested){
      return true;
    }
  }
  return false;
}

float BG_Utils::map(float x, float in_min, float in_max, float out_min, float out_max) {
  // rewrite the map function here, since the builtin map function seem to output only integer values
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

