
#include "DmxOut.h"

DmxOut dmxOut;

void setup() {
  Serial.begin(9600);
}

void loop() {
  
  dmxOut.writeDMXOutput(1, 10, true);
  delay(1000);
  dmxOut.writeDMXOutput(2, 20, true);
  delay(1000);
  dmxOut.writeDMXOutput(3, 30, true);
  delay(1000);
  dmxOut.writeDMXOutput(4, 40, true);
  delay(1000);
  dmxOut.writeDMXOutput(5, 50, true);
  delay(1000);
}


