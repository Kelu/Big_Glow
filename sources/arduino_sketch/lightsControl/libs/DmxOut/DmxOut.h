/*
  dmxOut.h - Library for output dmx value through dmxShield with arduino,
  and also through the serial port for simulation purposes (in max BigGlow software).
  Created by Luc Ardaillon, mach 24, 2019.

  Done following tutorial for building arduino library : https://www.arduino.cc/en/Hacking/LibraryTutorial

  ** For more details on DmxMaster, and compatible Processing sketch,
  ** visit http://code.google.com/p/tinkerit/wiki/SerialToDmx
  **
  ** Help and support: http://groups.google.com/group/DmxMaster 
*/

#ifndef DmxOut_h
#define DmxOut_h

#include "Arduino.h"

class DmxOut
{
	public:
    DmxOut();
		void writeDMXOutput(int channel, int value);
    void setSimulationState(int simulation);
    void setMinIntensityVal(int minIntensityVal);
    void setMaxIntensityVal(int maxIntensityVal);
   private:
    //int dummy();
    bool _simulation;
    int _minIntensityVal;
    int _maxIntensityVal;
};

#endif
