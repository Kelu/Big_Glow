/*
  dmxOut.h - Library for output dmx value through dmxShield with arduino,
  and also through the serial port for simulation purposes (in max BigGlow software).
  Created by Luc Ardaillon, mach 24, 2019.

  Done following tutorial for building arduino library : https://www.arduino.cc/en/Hacking/LibraryTutorial

  ** For more details on DmxMaster, and compatible Processing sketch,
  ** visit http://code.google.com/p/tinkerit/wiki/SerialToDmx
  **
  ** Help and support: http://groups.google.com/group/DmxMaster 
*/

#include <DmxMaster.h>
#include "Arduino.h"
#include "DmxOut.h"

DmxOut::DmxOut(){
  // Initialize serial communication : 
  //Serial.begin(9600);
  //nothing else to do here into the constructor;
}


void DmxOut::writeDMXOutput(int channel, int value, int simulation){
  DmxMaster.write(channel, value); // write dmx value to output of dmx shield
  // if "simulation" is true: also write the values to the serial port for seeing what is happening \
  // in the monitor or observe the result as a simulation in the bigGlow renderer window
  if(simulation){ 
    Serial.print(channel);
    Serial.print(" ");
    Serial.print(value);
    Serial.println();
  }
}


