
#include <math.h>
#include <dmxOut.h>
#include <utils.h>
#include <commonChasers.h>

// grid16 lights configuration :
int nbLights = 16;
float gridDimMeter_x = 5.;
float gridDimMeter_y = 6.;
float gridResolutionMeter = 0.1;
float lightsPos_x[nbLights] = {}; //TODO : set positions
float lightsPos_y[nbLights] = {}; //TODO : set positions

// global control parameters :
const bool SIMULATION = true;
int programChoice = 1;

// some variables needed by the various function :
int channel;
int prevChannel = 1;
int fps = 33; // frames per second = number of update per second to poll values for continuous effects like sinusoid

void setup() {
  // standard baud rates are 1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
  Serial.begin(9600); // which value is necessary here? do some tests. Probably dependant on the number of lights to control and the control rate (depending on the effects used)
  turnAllLightsOff(nbLights);
}

void loop() {
  // TODO : 
  // - choisir le programme avec des boutons poussoirs
  // - régler la vitesse avec un potard

  int maxValue = 255;

  // fixed value :
  if(programChoice==0){
    fixedValue(nbLights, maxValue);
  }
  
}


