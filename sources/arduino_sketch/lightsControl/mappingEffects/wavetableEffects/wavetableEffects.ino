/*
 * Effects based on reading a waveform that is sampled according to the position (or more simply ordering) of the lights
 */

void sinusoid(int nbLights, float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float t;
  float channelPhase;
  float sinVal;
  int value;
  t = millis()/1000.;
  for(channel=1; channel<=nbLights; channel++){
    if(directionRight){
      channelPhase = (nbPeriods*2*PI - ((channel-1)*nbPeriods*2*PI)/nbLights);
    } else {
      channelPhase = (channel-1)*nbPeriods*PI/nbLights;
    }
    sinVal = (sin(2*PI*freq*t+channelPhase)+1)/2.;
    value = int(round(sinVal*maxValue));
    writeDMXOutput(channel, value, SIMULATION);
  }
  delay(1000/fps);
}

void readWavetable(float wavetable[], float resolution, int nbLights, float freq, float nbPeriods, int maxValue, boolean directionRight){
  boolean revert = true;
  float t;
  float channelPhase;
  float wavetableVal;
  int wavetableIdx;
  int value;
  t = millis()/1000.;
  for(channel=1; channel<=nbLights; channel++){
    if(directionRight){
      channelPhase = (nbPeriods*2*PI - ((channel-1)*nbPeriods*2*PI)/nbLights);
    } else {
      channelPhase = (channel-1)*nbPeriods*2*PI/nbLights;
    }
    wavetableIdx = int(round(fmod((2*PI*freq*t+channelPhase),(2*PI))*resolution/(2*PI)));
    if(revert){
      wavetableVal = wavetable[int(resolution) - wavetableIdx];      
    } else {
      wavetableVal = wavetable[wavetableIdx];
    }
    value = int(round(wavetableVal*maxValue));
    writeDMXOutput(channel, value, SIMULATION);
  }
  delay(1000/fps);
}

void sawtooth(int nbLights, float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float resolution = 255;
  float ramp[int(resolution)];
  int i;

  // initialize sawtooth wavetable : (should it be done only once outside of the function as a global variable to reduce computation load?)
  for(i=0; i<resolution; i++){
    ramp[i] = 1 - (i/(resolution-1.));
  }

  // read values from wavetable according to channel, frequency, ... :
  readWavetable(ramp, resolution, nbLights, freq, nbPeriods, maxValue, directionRight);
}

void triangle(int nbLights, float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float resolution = 255;
  float ramp[int(resolution)];
  int i;

  // initialize triangle wavetable : (should it be done only once outside of the function as a global variable to reduce computation load?)
  for(i=0; i<int(resolution)/2; i++){
    ramp[i] = 1 - (i/((int(resolution)/2)-1.));
  }
  for(i=int(resolution)/2; i<resolution; i++){
    ramp[i] = (i-int(resolution)/2)/((int(resolution)/2)-1.);
  }

  // read values from wavetable according to channel, frequency, ... :
  readWavetable(ramp, resolution, nbLights, freq, nbPeriods, maxValue, directionRight);
}

void square(int nbLights, float width, float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float resolution = 255;
  float square[int(resolution)];
  int breakPoint = int(round(width * resolution));

  // initialize sawtooth wavetable : (should it be done only once outside of the function as a global variable to reduce computation load?)
  int i;
  for(i=0; i<breakPoint; i++){
    square[i] = 1;
  }
  for(i=breakPoint; i<int(resolution); i++){
    square[i] = 0;
  }

  // read values from wavetable according to channel, frequency, ... :
  readWavetable(square, resolution, nbLights, freq, nbPeriods, maxValue, directionRight);
}

void test(int nbLights, int minValue, int maxValue, boolean directionRight){
  int resolution = 21;
  float testArray[resolution] = {0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.};

  // read values from wavetable according to channel, frequency, ... :
  readWavetable(testArray, resolution, nbLights, 1., 1., maxValue, directionRight);
}

