/* This program allows you to set DMX channels over the serial port.
**
** After uploading to Arduino, switch to Serial Monitor and set the baud rate
** to 9600. You can then set DMX channels using these commands:
**
** <number>c : Select DMX channel
** <number>w : Set DMX channel to new value
**
** These can be combined. For example:
** 100c355w : Set channel 100 to value 255.
**
** For more details, and compatible Processing sketch,
** visit http://code.google.com/p/tinkerit/wiki/SerialToDmx
**
** Help and support: http://groups.google.com/group/DmxMaster */

#include <DmxMaster.h>
#include <math.h>
#include <../dmxOut/dmxOut.h>

// global control parameters :
int nbLights = 10;
const bool SIMULATION = true; // Set to True for simulation with BigGlow software. Set to False for real usage with dimmer.
int programChoice = 9;

// some variables needed by the various function :
bool doInit = true;
int channel;
int prevChannel = 1;
int startTime;
int fps = 33; // frames per second = number of update per second to poll values for continuous effects like sinusoid

void setup() {
  // standard baud rates are 1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
  Serial.begin(115200); // 9600
  //Serial.println("SerialToDmx ready");
  //turnAllLightsOff(nbLights);
}

void loop() {
  // TODO : 
  // - choisir le programme avec des boutons poussoirs
  // - régler la vitesse avec un potard

  int maxValue = 255;
  int minValue = 0;
  int timeStep = 250; // à contrôler avec un potard

  float freq = 0.5;
  boolean directionRight = true;
  float nbPeriods = 1.;

  // fixed value :
  if(programChoice==0){
    fixedValue(nbLights, maxValue);
  }
  
  // simple chaser :
  else if(programChoice==1){
    bool directionRight = true; // if true, chaser goes from left to right; else, from right to left.
    simpleChaser(nbLights, timeStep, directionRight, minValue, maxValue);
  }

  // simple chaser random :
  else if(programChoice==2){
    prevChannel = simpleChaserRandom(prevChannel, nbLights, timeStep, minValue, maxValue);
  }

  // chaser random with multiple lights changing with a random dimmer value at each step :
  else if(programChoice==3){
    int nbLightsPerStep = 5;
    randomChaserMultipleLightsPerStep(nbLights, nbLightsPerStep, timeStep, minValue, maxValue);
  }

  // flickering around a mean value :
  else if(programChoice==4){
    int meanValue = 150;
    flickering(nbLights, meanValue, minValue, maxValue);
  }

  // any chaser from pre-programmed sequence:
  else if(programChoice==5){
    anyChaser(timeStep, minValue, maxValue);
  }
  
  // strobe :
  else if(programChoice==6){
    timeStep = 125; // à contrôler avec un potard
    strobe(nbLights, timeStep, minValue, maxValue);
  }

  /*// test read 2d array from txt file :
  else if(programChoice==7){
    String** outArray;
    String fileName = "/Users/Kelu/Big_Glow/sources/arduino_sketch/chasers/testchaser.txt";
    importCSV(fileName, outArray);
  }*/

  // sinusoid :
  else if(programChoice==8){
    sinusoid(nbLights, freq, nbPeriods, minValue, maxValue, directionRight);
  }

  // TODO : verify and finish the following functions :
  //sawtooth :
  else if(programChoice==9){
    sawtooth(nbLights, freq, nbPeriods, minValue, maxValue, directionRight);
  }

  //triangle :
  else if(programChoice==10){
    freq = 0.5;
    nbPeriods = 1.;
    directionRight = false;
    triangle(nbLights, freq, nbPeriods, minValue, maxValue, directionRight);
  }

  //square :
  else if(programChoice==11){
    float width = 0.5;
    freq = 0.5;
    nbPeriods = 1.;
    square(nbLights, width, freq, nbPeriods, minValue, maxValue, directionRight);
  }
  
}

int writeDMXOutput(int channel, int value, int simulation){
  DmxMaster.write(channel, value); // write dmx value to output of dmx shield
  // if "simulation" is true: also write the values to the serial port for seeing what is happening \
  // in the monitor or observe the result as a simulation in the bigGlow renderer window
  if(simulation){ 
    Serial.print(channel);
    Serial.print(" ");
    Serial.print(value);
    Serial.println();
  }
}

void fixedValue(int nbLights, int value){
  int channel;
  for(channel=1; channel<=nbLights; channel++){
    writeDMXOutput(channel, value, SIMULATION);
  }
}

void turnAllLightsOff(int nbLights){
  fixedValue(nbLights, 0);
}

void simpleChaser(int nbLights, int timeStep, bool directionRight, int minValue, int maxValue){
  // TODO : implémenter option aller-retour
  
  if(directionRight){ // chaser goes from left to right
    prevChannel = nbLights;
  } else { // chaser goes from right to left
    prevChannel = 1;
  }

  int i;
  for(i=0; i<nbLights; i++){
    if(directionRight){
      channel = i+1;
    } else {
      channel = nbLights - i;
    }
    
    writeDMXOutput(channel, maxValue, SIMULATION);
    writeDMXOutput(prevChannel, minValue, SIMULATION);
  
    delay(timeStep);
    prevChannel = channel;
  }
}

int simpleChaserRandom(int prevChannel, int nbLights, int timeStep, int minValue, int maxValue){  
  int channel = random(1,nbLights+1);
  while(channel==prevChannel){
    channel = random(1,nbLights+1);
  }
  writeDMXOutput(channel, maxValue, SIMULATION);
  writeDMXOutput(prevChannel, minValue, SIMULATION);
  prevChannel = channel;
  delay(timeStep);
  return prevChannel;
}

boolean valueIsInArray(int array[], int sizeOfArray, int valueToBeTested){
  for (int i=0; i<sizeOfArray; i++){
    if (array[i] == valueToBeTested){
      return true;
    }
  }
  return false;
}

void randomChaserMultipleLightsPerStep(int nbLights, int nbLightsPerStep, int timeStep, int minValue, int maxValue){
  int channel;
  int value;
  int selectedChannels[nbLightsPerStep];
  // init with no selected channels :
  int i;
  for(i=0; i<nbLightsPerStep; i++){
    selectedChannels[i] = -1; // -1 means no channel value selected yet
  }
  // pick randomly nbLightsPerStep different channels and pick random dimmer value for each :
  for(i=0; i<nbLightsPerStep; i++){
    channel = random(1,nbLights+1);
    while(valueIsInArray(selectedChannels, nbLightsPerStep, channel)){
      channel = random(1,nbLights+1);
    }
    selectedChannels[i] = channel;
    value = random(minValue,maxValue+1);
    writeDMXOutput(channel, value, SIMULATION);
  }
  delay(timeStep);
}

void flickering(int nbLights, int meanValue, int minValue, int maxValue){
  int timeStep = 40;
  int deviation = 20;
  int lowValue = max(meanValue - deviation, minValue);
  int highValue = min(meanValue + deviation, maxValue);
  randomChaserMultipleLightsPerStep(nbLights, nbLights, timeStep, lowValue, highValue);
}

void anyChaser(int timeStep, int minValue, int maxValue){
  // TODO : implement possibility to read chaser from a text file
  // TODO : implémenter option aller-retour
  int value;
  /*boolean chaser[4][10]={
  1,1,0,0,0,0,0,0,1,1,
  0,0,1,1,0,0,1,1,0,0,
  0,0,0,0,1,1,0,0,0,0,
  0,0,1,1,0,0,1,1,0,0};*/

  //uneSurDeux :
  boolean chaser[4][10]={
  1,0,1,0,1,0,1,0,1,0,
  0,1,0,1,0,1,0,1,0,1};
  
  int i;
  int j;
  int nbSteps = sizeof(chaser)/sizeof(chaser[0]);
  int nbLightsPerStep = sizeof(chaser[0])/sizeof(chaser[0][0]);
  for(i=0; i<nbSteps; i++){
    for(j=0; j<nbLightsPerStep; j++){
      channel = j+1;
      if(chaser[i][j]==1){
        value = maxValue;
      } else {
        value = minValue;
      }
      writeDMXOutput(channel, value, SIMULATION);
    }
    delay(timeStep);
  }
}

void strobe(int nbLights, int timeStep, int minValue, int maxValue){  
  for(channel=1; channel<=nbLights; channel++){
    writeDMXOutput(channel, maxValue, SIMULATION);
  }
  delay(timeStep);

  for(channel=1; channel<=nbLights; channel++){
    writeDMXOutput(channel, minValue, SIMULATION);
  }
  delay(timeStep);
}

void sinusoid(int nbLights, float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float t;
  float channelPhase;
  float sinVal;
  int value;
  t = millis()/1000.;
  for(channel=1; channel<=nbLights; channel++){
    if(directionRight){
      channelPhase = (nbPeriods*2*PI - ((channel-1)*nbPeriods*2*PI)/nbLights);
    } else {
      channelPhase = (channel-1)*nbPeriods*PI/nbLights;
    }
    sinVal = (sin(2*PI*freq*t+channelPhase)+1)/2.;
    value = int(round(sinVal*maxValue));
    writeDMXOutput(channel, value, SIMULATION);
  }
  delay(1000/fps);
}

void readWavetable(float wavetable[], float resolution, int nbLights, float freq, float nbPeriods, int maxValue, boolean directionRight){
  boolean revert = true;
  float t;
  float channelPhase;
  float wavetableVal;
  int wavetableIdx;
  int value;
  t = millis()/1000.;
  for(channel=1; channel<=nbLights; channel++){
    if(directionRight){
      channelPhase = (nbPeriods*2*PI - ((channel-1)*nbPeriods*2*PI)/nbLights);
    } else {
      channelPhase = (channel-1)*nbPeriods*2*PI/nbLights;
    }
    wavetableIdx = int(round(fmod((2*PI*freq*t+channelPhase),(2*PI))*resolution/(2*PI)));
    if(revert){
      wavetableVal = wavetable[int(resolution) - wavetableIdx];      
    } else {
      wavetableVal = wavetable[wavetableIdx];
    }
    value = int(round(wavetableVal*maxValue));
    writeDMXOutput(channel, value, SIMULATION);
  }
  delay(1000/fps);
}

void sawtooth(int nbLights, float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float resolution = 255;
  float ramp[int(resolution)];
  int i;

  // initialize sawtooth wavetable : (should it be done only once outside of the function as a global variable to reduce computation load?)
  for(i=0; i<resolution; i++){
    ramp[i] = 1 - (i/(resolution-1.));
  }

  // read values from wavetable according to channel, frequency, ... :
  readWavetable(ramp, resolution, nbLights, freq, nbPeriods, maxValue, directionRight);
}

void triangle(int nbLights, float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float resolution = 255;
  float ramp[int(resolution)];
  int i;

  // initialize triangle wavetable : (should it be done only once outside of the function as a global variable to reduce computation load?)
  for(i=0; i<int(resolution)/2; i++){
    ramp[i] = 1 - (i/((int(resolution)/2)-1.));
  }
  for(i=int(resolution)/2; i<resolution; i++){
    ramp[i] = (i-int(resolution)/2)/((int(resolution)/2)-1.);
  }

  // read values from wavetable according to channel, frequency, ... :
  readWavetable(ramp, resolution, nbLights, freq, nbPeriods, maxValue, directionRight);
}

void square(int nbLights, float width, float freq, float nbPeriods, int minValue, int maxValue, boolean directionRight){
  float resolution = 255;
  float square[int(resolution)];
  int breakPoint = int(round(width * resolution));

  // initialize sawtooth wavetable : (should it be done only once outside of the function as a global variable to reduce computation load?)
  int i;
  for(i=0; i<breakPoint; i++){
    square[i] = 1;
  }
  for(i=breakPoint; i<int(resolution); i++){
    square[i] = 0;
  }

  // read values from wavetable according to channel, frequency, ... :
  readWavetable(square, resolution, nbLights, freq, nbPeriods, maxValue, directionRight);
}

void test(int nbLights, int minValue, int maxValue, boolean directionRight){
  int resolution = 21;
  float testArray[resolution] = {0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.};

  // read values from wavetable according to channel, frequency, ... :
  readWavetable(testArray, resolution, nbLights, 1., 1., maxValue, directionRight);
}
