/*
 * tried with large aluminium foil, and resistor value of 560 ohm (green, blue, brown, gold 
 * => worked well like this with printed values from around 150 to 1023
 */

#include <DmxMaster.h>
#include "DmxOut.h"

DmxOut dmxOut;
bool SIMULATION = true;
int channel = 2;

// Analog input pin that the Velostat is connected to
const int analogInPin = A0;  

// value read from the Velostat
int val = 0;        

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);

  //init Analog Pin as PULLUP
  //(meaning it sends out voltage)
  digitalWrite(analogInPin, HIGH);

}

void loop() {
  // read the analog in value:
  val = analogRead(analogInPin);
  
  // print the results to the serial monitor:
  val = map(val, 870, 25, 0, 255);
  val = max(0, val);
  val = min(255, val);
  Serial.print("sensor = " );
  Serial.println(val);
  dmxOut.writeDMXOutput(channel, val);
}
