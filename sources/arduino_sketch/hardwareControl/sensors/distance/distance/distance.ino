#include <DmxMaster.h>
#include "DmxOut.h"

DmxOut dmxOut;
bool SIMULATION = false;
int channel = 2;

int i = 0;
int val;
int sensorpin=0;

void setup() {
  pinMode(sensorpin,INPUT);
  Serial.begin(9600);
}

void loop() {
  val=analogRead(sensorpin);
  val = map(val, 230, 650, 0, 255);
  val = max(0, val);
  val = min(255, val);
  Serial.println(val);
  dmxOut.writeDMXOutput(channel, val, SIMULATION);
  delay(10);
}


/*int i = 0;
int val;
int sensorpin=0;
const int filterSize = 100;
int values[filterSize];

void setup() {
  pinMode(sensorpin,INPUT);
  Serial.begin(9600);
  for (int j = 0; j < filterSize; j++) {
    values[j] = 0;
  }
}

void loop() {
  values[i]=analogRead(sensorpin);
  int total = 0;
  for(int j = 0; j < filterSize; j++){
    total += values[j];
  }
  float mean = total / filterSize;
  val = mean;//map(mean, 260, 550, 0, 255);
  //val = max(0, val);
  //val = min(255, val);
  Serial.println(val);
  i += 1;
  i = i%filterSize;
  //delay(10);
}*/

/*int i;
int val;
int redpin=0;
void setup() {
    pinMode(redpin,OUTPUT);
    Serial.begin(9600);
}
void loop() {
    i=analogRead(redpin);
    val=(6762/(i-9))-4;
    Serial.println(val);
}*/


/*
//We changed the code so cm is the only output, the original code had inches and cm.

//Original author: Bruce Allen
//Date: 23/07/09
//Changed by Marte and Runar 09/10/14, to only have cm as output
//Digital pin 7 for reading in the pulse width from the MaxSonar device.
//This variable is a constant because the pin will not change throughout execution of this code.
const int pwPin = 7;
//variables needed to store values
long pulse, inches, cm;
int output;

void setup(){ //This opens up a serial connection to shoot the results back to the PC console
  Serial.begin(9600);
}

void loop(){
  pinMode(pwPin, INPUT);
  //Used to read in the pulse that is being sent by the MaxSonar device.
  //Pulse Width representation with a scale factor of 147 uS per Inch.
  pulse = pulseIn(pwPin, HIGH);
  //147uS per inch
  inches = pulse/147;
  //change inches to centimetres
  cm = inches * 2.54;
  //  Serial.print(inches);
  Serial.print("in, ");
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();
  output = map(cm,0,645, 0, 255);
  Serial.println(output);
  delay(1);
}*/
