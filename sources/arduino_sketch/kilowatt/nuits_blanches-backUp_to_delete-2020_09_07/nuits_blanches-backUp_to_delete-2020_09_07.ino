
// In order to initialize dimmer, we need to first send something (e.g. value 0) to channel 32
// Be sure to have the dimmer in 4ch mode and 8bit dmx mode!!

#include <DmxMaster.h>
//#include <DmxOut.h> // not usefull if use only functions from CommonChasers to write dmx out, but could be if I write another custom function directly here
#include <CommonChasers.h>

// global control parameters :
const int nbLights = 18;
int channelsToDmxAddressArray[nbLights] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}; // dmx addresses for software simulation => adapt with order given by Sylvain
//int channelsToDmxAddressArray[nbLights] = {1,2,3,5,6,7,9,10,11,13,14,15,17,18,19,21,22,23}; // (reverse?) order of dmx channels for kilowatt installation
//int channelsToDmxAddressArray[nbLights] = {23,22,21,19,18,17,15,14,13,11,10,9,7,6,5,3,2,1}; // order of dmx channels for kilowatt installation
int nbLightsPerStep = 4; // TODO : 6?
const bool SIMULATION = true; // TODO : false // Set to True for simulation with BigGlow software. Set to False for real usage with dimmer!!
const bool TEST = false;


// create object from CommonChaser class to be able to use the functions from the library :
volatile CommonChasers cc(nbLights, SIMULATION);

// history for knobs values to filter out jitter :
const int filterHistorySize = 20;
volatile int historyIdx = 0;

// hardware controls :
const int knob1Pin = A0; // knob to control min intensity of lights
volatile int knob1Val = 0; 
volatile int knob1ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob2Pin = A1; // knob to control max intensity of lights
volatile int knob2Val = 0;
volatile int knob2ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob3Pin = A2; // knob to control speed of effect
volatile int knob3Val = 0;
volatile int knob3ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob4Pin = A3; // knob to control effect parameter (e.g. ramp speed / fade time of chaser)
volatile int knob4Val = 0;
volatile int knob4ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int commutateurPos1Pin = 2; // select 1st programm
int commutateurPos1Val = 0;

const int commutateurPos2Pin = 3; // select 2nd programm
int commutateurPos2Val = 0;

const int commutateurPos3Pin = 4; // select 3rd programm
int commutateurPos3Val = 0;

//const int commutateurPos4Pin = 5; // select 4th programm => 4th position doesn't work, so choose it when all other 3 are at 0.
//int commutateurPos4Val = 0;

int commutateurPos = 1; // position can be 1 to 4, depending on states of commutateurPos1Val, commutateurPos2Val, and commutateurPos3Val
int commutateurPosOld = 1; // retain previous position of commutateurPos

const int grosBoutonRougePin = 6;
int grosBoutonRougeVal = 0;
int grosBoutonRougeValOld = 0;

const int switchPin = 7;
int switchVal = 0;
int switchValOld = 0;

// program control variables :
int minIntensityValue = 1; // TODO : set at least to 1 for real installation, so that there is always some lights
volatile int maxIntensityValue = 255; // TODO : 100 for real installation (otherwise it is too bright, as the leds are too powerfull)
int min_time_step_ms = 50;
int max_time_step_ms = 2000;
volatile int time_step_ms = 250;
volatile int prog1State = 1;
volatile int prog2State = 0;
volatile int prog3State = 0;
volatile int prog4State = 0;

int value = 0;
int channel;

static unsigned long timer = 0;

void setup() {
  // standard baud rates are 1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
  Serial.begin(115200);

  // configure proper dmx channels order of lights :
  cc.setChannelsToDmxAddressArray(channelsToDmxAddressArray);

  // set-up interrupt to be able to read hardware input values at regular intervals without stopping running programm:
  cli();//stop interrupts
  
  //set timer1 interrupt at ~50Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 50hz increments
  OCR1A = 312;// = (16*10^6) / (50*1024) - 1 (must be <65536)
  //// set compare match register for 10hz increments
  //OCR1A = 1562;// = (16*10^6) / (10*1024) - 1 (must be <65536)
  // turn on CTC mode~
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();//allow interrupts

  // set digital I/O in "INPUT_PULLUP" mode to read buttons/switchs values from hardware interface
  pinMode(commutateurPos1Pin, INPUT_PULLUP);
  pinMode(commutateurPos2Pin, INPUT_PULLUP);
  pinMode(commutateurPos3Pin, INPUT_PULLUP);
  //pinMode(commutateurPos4Pin, INPUT_PULLUP);
  pinMode(grosBoutonRougePin, INPUT_PULLUP);
  pinMode(switchPin, INPUT_PULLUP);

  // send something on channel 32 to initialize dimmer, otherwise it won't work...
  DmxMaster.write(32, 255); // not sure if necessary...
  delay(100); // not sure if necessary ...
  DmxMaster.write(32, 0); // definitely necessary to send at least 1 value (here 0) to channel 32, otherwise nothing will happen!!
  for(int c=0; c<=32; c++){ // then set all channels to 0 before starting, in case other values were kept in memory
    DmxMaster.write(32, 0);
  }

  timer = millis();
}


int knobToSpeed(float knobVal){ // time step is in ms
  float progTimeStep_ms = map(knobVal, 0, 1023, max_time_step_ms, min_time_step_ms); 
  return progTimeStep_ms;
}


int knobToIntensity(float knobVal){ // time step is in ms
  int intensity = map(knobVal, 0, 1023, 0, 255); // map analog knob value to dmx value range
  return intensity;
}


float map(float x, float in_min, float in_max, float out_min, float out_max) {
  // rewrite the map function here, since the builtin map function seem to output only integer values
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


int averageFilter(int currentVal, int historyIdx, int valuesArray[]){
  valuesArray[historyIdx] = currentVal;
  int sum = 0;
  for (int i=0; i<filterHistorySize; i++){
    sum += valuesArray[i];
  }
  int average = sum/filterHistorySize;
  return average;
}


// interrupt function :
ISR(TIMER1_COMPA_vect){//timer1 interrupt at 50Hz (read knob and buttons states every ~20ms)

  // read the position of the 4-positions selector :
  commutateurPosOld = commutateurPos;
  commutateurPos = readCommutateurPosition();

  // read other buttons states :
  grosBoutonRougeValOld = grosBoutonRougeVal;
  grosBoutonRougeVal = 1 - digitalRead(grosBoutonRougePin);
  switchValOld = switchVal;
  switchVal = 1 - digitalRead(switchPin);

  
  // read knobs values
  historyIdx = (historyIdx+1)%filterHistorySize;
  
  knob1Val = analogRead(knob1Pin); // analogRead should give values in range [0-1023].
  knob1Val = averageFilter(knob1Val, historyIdx, knob1ValHistory); // smooth values to avoid jitter
  minIntensityValue = knobToIntensity(knob1Val); // default : 1
  
  knob2Val = analogRead(knob2Pin); // analogRead should give values in range [0-1023].
  knob2Val = averageFilter(knob2Val, historyIdx, knob2ValHistory); // smooth values to avoid jitter
  maxIntensityValue = knobToIntensity(knob2Val); // default : 100
  
  knob3Val = analogRead(knob3Pin); // analogRead should give values in range [0-1023].
  knob3Val = averageFilter(knob3Val, historyIdx, knob3ValHistory); // smooth to avoid jitter
  time_step_ms = knobToSpeed(knob2Val); // default : 2000
  
  /*knob4Val = analogRead(knob4Pin); // analogRead should give values in range [0-1023].
  knob4Val = averageFilter(knob4Val, historyIdx, knob4ValHistory); // smooth to avoid jitter
  effect_param = knob4Val; // default : ??*/

  // Forced default values : 
  //minIntensityValue = 0;
  //maxIntensityValue = 255;
  //time_step_ms = 250;
  
  cc.setMinIntensityVal(minIntensityValue);
  cc.setMaxIntensityVal(maxIntensityValue);
  cc.setTimeStep(time_step_ms);

  if(TEST){ 
    if(commutateurPos != commutateurPosOld){
      Serial.print("commutateurPos = ");
      Serial.print(commutateurPos);
      Serial.print("\n");
    }
    if(grosBoutonRougeVal != grosBoutonRougeValOld){
      Serial.print("grosBoutonRougeVal = ");
      Serial.print(grosBoutonRougeVal);
      Serial.print("\n");
    }
    if(switchVal != switchValOld){
      Serial.print("switchVal = ");
      Serial.print(switchVal);
      Serial.print("\n");
    }
  }
}

void loop() {

  ////////////////////////////////////////////////////////////////////////////////////
  // DMX control data are generated by the arduino programm :
  ////////////////////////////////////////////////////////////////////////////////////
  
  // programms available for testing :
  //cc.fixedValue(maxIntensityValue);
  //cc.simpleChaser(true, minIntensityValue, maxIntensityValue);
  //cc.simpleChaserRandom(minIntensityValue, maxIntensityValue);
  //cc.randomChaserMultipleLightsPerStep(nbLightsPerStep, -1, minIntensityValue, maxIntensityValue);
  //cc.flickering((minIntensityValue+maxIntensityValue)/2., minIntensityValue, maxIntensityValue);
  //cc.strobe(minIntensityValue, maxIntensityValue);
  //cc.sinusoid(1./(time_step_ms/1000.), 1, minIntensityValue, maxIntensityValue, true);

  //float freqSin = 1./(time_step_ms/1000.);
  float freqSin = 1/3.; // Use a default period of 3 seconds

  
  if(commutateurPos==1){
    //cc.fixedValue(maxIntensityValue); // remplacer par effet type néon qui a du mal à s'allumer (avec du son??)
    cc.flickering((minIntensityValue+maxIntensityValue)/2., minIntensityValue, maxIntensityValue);
  } else if(commutateurPos==2){
    cc.randomChaserMultipleLightsPerStep(nbLightsPerStep, -1, minIntensityValue, maxIntensityValue);
  } else if(commutateurPos==3){
      cc.sinusoid(freqSin, 1, minIntensityValue, maxIntensityValue, true);
  } else if(commutateurPos==4){
    cc.simpleChaser(true, minIntensityValue, maxIntensityValue);
  }
  
  //delay(1); //setTimeStep delay in loop for stability. Is that useful?

}

int readCommutateurPosition(){
  commutateurPosOld = commutateurPos;
  commutateurPos1Val = 1 - digitalRead(commutateurPos1Pin);
  commutateurPos2Val = 1 - digitalRead(commutateurPos2Pin);
  commutateurPos3Val = 1 - digitalRead(commutateurPos3Pin);
  //commutateurPos4Val = 1 - digitalRead(commutateurPos4Pin);
  if(commutateurPos1Val==1 && commutateurPos2Val==0 && commutateurPos3Val==0){
    commutateurPos = 1;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==1 && commutateurPos3Val==0){
    commutateurPos = 2;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==0 && commutateurPos3Val==1){
    commutateurPos = 3;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==0 && commutateurPos3Val==0){
    commutateurPos = 4;
  } else{ // other states don't exist and should not occur
    commutateurPos = commutateurPosOld;
  }
  return commutateurPos;
}
