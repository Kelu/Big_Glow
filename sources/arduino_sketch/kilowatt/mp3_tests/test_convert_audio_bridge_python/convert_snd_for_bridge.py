import scipy.io.wavfile as wavio
import numpy as np

from matplotlib import pyplot as plt

if __name__ == '__main__':
    
    sndFile = '/Users/Kelu/Big_Glow/sources/arduino_sketch/kilowatt/mp3_tests/test_convert_audio_bridge_python/arc_elec.wav'
    #sndFileMono = ''
    sndFileBridge = '/Users/Kelu/Big_Glow/sources/arduino_sketch/kilowatt/mp3_tests/test_convert_audio_bridge_python/arc_elec-for_amp_bridge.wav'
    
    (sr, snd) = wavio.read(sndFile)
    
    ch1 = snd[:,0]
    ch2 = snd[:,0]
    monoSnd = ch1+ch2/2.
    invertMonoSnd = -monoSnd
    
    outBridgeSnd = np.array([monoSnd,invertMonoSnd]).T
    
    wavio.write(sndFileBridge, sr, outBridgeSnd)
    
    #print(np.shape(snd))
    #print(np.shape(outBridgeSnd))
    
    
    plt.figure()
    plt.plot(ch1[1000:2000])
    plt.plot(ch2[1000:2000])
    plt.show()
    
    plt.figure()
    plt.plot(monoSnd[1000:2000])
    plt.plot(invertMonoSnd[1000:2000])
    plt.show()
    
    