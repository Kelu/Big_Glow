// code obtained from here : https://forum.arduino.cc/index.php?topic=427344.0

int ledNeon = 9;           // choose the pin for the LED
int val = 0;                 // variable for reading the pin status
int max_count = 80;          // counter for the flicker table
int countTwo = 0;
byte Flicker_table[] = { 10, 10, 20, 30, 30, 30, 40, 50, 60, 70, 80, 70, 70,
                           60, 60, 50, 50, 50, 60, 70, 80, 90, 100,
                           120,140,160,240,250,100,150,250,250,140,
                           240,230,220,100, 80,70,70, 70, 80, 80,
                           140,130,120,110,200,210,220,220,100, 90,
                            40, 30, 30, 30, 20, 10, 10 };
                           

void setup() {
 pinMode(ledNeon, OUTPUT);   // declare Wind led as output

  if (val == HIGH) {            // check if the input is HIGH
 } else {
      delay(30);
   for ( int i=0; i <= 150; i++) {  // This for loop runs untill the flicker table finishes
   analogWrite(ledNeon, Flicker_table[countTwo]);
     countTwo++;
     if(countTwo > max_count ){
     countTwo = 0;  // Helps makes sure our next flicker doesnt start in an arbitrary place on the table
   }
   delay(20);  // the delay for our flicker, make it faster to to make it flicker a little more violently
   digitalWrite(ledNeon, HIGH);
   }
 }
}

void loop(){
}
