
// code from youtube video https://www.youtube.com/watch?v=eiK6oeDteHA&ab_channel=NikolajRahbek showing neon flickering simulation

// other videos for inspiration :
// - https://www.youtube.com/watch?v=kxK88dgR6IU&ab_channel=FrightProps
// - https://www.shutterstock.com/video/clip-23470522-fluorescent-neon-light-tubes-flickering-when-switching
// - https://www.youtube.com/watch?v=Iu0Xl8CENeQ&ab_channel=NeonCreationsLtd
// - https://www.youtube.com/watch?v=mW7BJ8NQNnc&ab_channel=JerryAdlersfluegel%2CJr.

#define DATA_PIN 3
#define BUTTON_PIN 4

boolean lightOn = 0;

void setup() { 
  pinMode(DATA_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);     
}

const int blinkOn[] = {10, 20, 20, 240, 20, 40, 20, 100, 20, 20, 20, 260, 80, 20, 240, 60, 160, 20, 240, 20, 1000, 20, 20, 40, 100, 20, 2740, 340, 860, 20, 1400, 20, 60, 20};

void allOn(boolean on) {
  digitalWrite(DATA_PIN, on);
}  

void updateLeds() {
  allOn(lightOn);
}

boolean myDelay(unsigned int ms) {
  unsigned long startTime = millis();
  unsigned long endTime = startTime+ms; 
  

  while(millis() < endTime) {
    if(!digitalRead(BUTTON_PIN)) {
      return 1;
    }
    // Update colour and intencity of leds
    updateLeds();
    delay(1);
  }
  return 0;
}

void turnOn() {
  // Don't flicker, if the light is already on
  if(lightOn)
    return;
    
  // Start to flicker
  for(int i=0; i<sizeof(blinkOn)/sizeof(int); ++i) {
    lightOn = !(i&1);
    if(myDelay(blinkOn[i])) {
      // Button is no longer pushed
      allOn(false);
      lightOn = false;
      return;
    }
  }

  // Make sure the light is actually on
  allOn(true);
  lightOn = true;
}

void turnOff() {
  lightOn = false;
  allOn(false);
}

void loop() { 
  delay(100);
  if(digitalRead(BUTTON_PIN))
    turnOn();
  else 
    turnOff();
}
