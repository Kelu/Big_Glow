
// code to test interface

#define SHOWPOTARDS

// portards :
const int potard1Pin = A0; 
const int potard2Pin = A1; 
const int potard3Pin = A2; 
const int potard4Pin = A3; 
int potard1Val = 0;
int potard2Val = 0;
int potard3Val = 0;
int potard4Val = 0;
int potard1ValOld = -1;
int potard2ValOld = -1;
int potard3ValOld = -1;
int potard4ValOld = -1;

// switchs :
const int commutateurPos1Pin = 5; 
const int commutateurPos2Pin = 6; 
const int commutateurPos3Pin = 7; 
//const int commutateurPos4Pin = 5; 
int commutateurPos1Val = 0;
int commutateurPos2Val = 0;
int commutateurPos3Val = 0;
//int commutateurPos4Val = 0;
int commutateurPos1ValOld = -1;
int commutateurPos2ValOld = -1;
int commutateurPos3ValOld = -1;
//int commutateurPos4ValOld = 0;

const int switchPin = 8;
int switchVal = 0;
int switchValOld = -1;

const int grosBoutonRougePin = 9;
int grosBoutonRougeVal = 0;
int grosBoutonRougeValOld = -1;

void setup() {
  Serial.begin(9600);
  pinMode(commutateurPos1Pin, INPUT_PULLUP);
  pinMode(commutateurPos2Pin, INPUT_PULLUP);
  pinMode(commutateurPos3Pin, INPUT_PULLUP);
  //pinMode(commutateurPos4Pin, INPUT_PULLUP);

  pinMode(switchPin, INPUT_PULLUP);

  pinMode(grosBoutonRougePin, INPUT_PULLUP);
  
}

void loop() {
  
  potard1Val = 1023 - analogRead(potard1Pin);
  potard2Val = 1023 - analogRead(potard2Pin);
  potard3Val = 1023 - analogRead(potard3Pin);
  potard4Val = 1023 - analogRead(potard4Pin);
  
  commutateurPos1Val = 1 - digitalRead(commutateurPos1Pin);
  commutateurPos2Val = 1 - digitalRead(commutateurPos2Pin);
  commutateurPos3Val = 1 - digitalRead(commutateurPos3Pin);
  //commutateurPos4Val = 1 - digitalRead(commutateurPos4Pin);

  grosBoutonRougeVal = 1 - digitalRead(grosBoutonRougePin);

  switchVal = 1 - digitalRead(switchPin);

  #ifdef SHOWPOTARDS
  if(potard1Val != potard1ValOld){
    Serial.print("potard1Val : ");
    Serial.print(potard1Val);
    Serial.print("\n");
  }
  if(potard2Val != potard2ValOld){
    Serial.print("potard2Val : ");
    Serial.print(potard2Val);
    Serial.print("\n");
  }
  if(potard3Val != potard3ValOld){
    Serial.print("potard3Val : ");
    Serial.print(potard3Val);
    Serial.print("\n");
  }
  if(potard4Val != potard4ValOld){
    Serial.print("potard4Val : ");
    Serial.print(potard4Val);
    Serial.print("\n");
  }
  #endif


  if(commutateurPos1Val != commutateurPos1ValOld){
    Serial.print("commutateurPos1Val : ");
    Serial.print(commutateurPos1Val);
    Serial.print("\n");
  }
  if(commutateurPos2Val != commutateurPos2ValOld){
    Serial.print("commutateurPos2Val : ");
    Serial.print(commutateurPos2Val);
    Serial.print("\n");
  }
  if(commutateurPos3Val != commutateurPos3ValOld){
    Serial.print("commutateurPos3Val : ");
    Serial.print(commutateurPos3Val);
    Serial.print("\n");
  }
  /*if(commutateurPos4Val != commutateurPos4ValOld){
    Serial.print("commutateurPos4Val : ");
    Serial.print(commutateurPos4Val);
    Serial.print("\n");
  }*/


  if(switchVal != switchValOld){
    Serial.print("switchVal : ");
    Serial.print(switchVal);
    Serial.print("\n");
  }

  
  if(grosBoutonRougeVal != grosBoutonRougeValOld){
    Serial.print("grosBoutonRougeVal : ");
    Serial.print(grosBoutonRougeVal);
    Serial.print("\n");
  }

  potard1ValOld = potard1Val;
  potard2ValOld = potard2Val;
  potard3ValOld = potard3Val;
  potard4ValOld = potard4Val;

  commutateurPos1ValOld = commutateurPos1Val;
  commutateurPos2ValOld = commutateurPos2Val;
  commutateurPos3ValOld = commutateurPos3Val;
  //commutateurPos4ValOld = commutateurPos4Val;

  switchValOld = switchVal;

  grosBoutonRougeValOld = grosBoutonRougeVal;
  
  delay(100);
  
}
