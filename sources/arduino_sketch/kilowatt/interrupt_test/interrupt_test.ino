//timer interrupts
//by Amanda Ghassaei adapted by T.Nilsson July 2019 :)
//June 2012
//https://www.instructables.com/id/Arduino-Timer-Interrupts/

/*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*/

//timer setup for timer4
//For arduino Mega

//timer4 will interrupt at 1Hz

//storage variables
boolean toggle4 = LOW;
int ttt = 0;

void setup(){
 Serial.begin(9600);
 //set pins as outputs
 pinMode(13, OUTPUT);

  // set-up timer interrupts :
  if(true){ // to activate/deactivate interrupts, for debug purpose (should set to true for final version)
    // set-up interrupt to be able to read hardware input values at regular intervals without stopping running programm:
    cli();//stop interrupts
    
    //set timer1 interrupt at ~10Hz
    TCCR1A = 0;// set entire TCCR1A register to 0
    TCCR1B = 0;// same for TCCR1B
    TCNT1  = 0;//initialize counter value to 0
    // set compare match register for 50hz increments
    OCR1A = 312;// = (16*10^6) / (50*1024) - 1 (must be <65536)
    //// set compare match register for 10hz increments
    //OCR1A = 1562;// = (16*10^6) / (10*1024) - 1 (must be <65536)
    // turn on CTC mode~
    TCCR1B |= (1 << WGM12);
    // Set CS12 and CS10 bits for 1024 prescaler
    TCCR1B |= (1 << CS12) | (1 << CS10);  
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
  
    sei();//allow interrupts
  }

  if(false){
    cli();//stop interrupts

    //set timer4 interrupt at 1Hz
     TCCR4A = 0;// set entire TCCR1A register to 0
     TCCR4B = 0;// same for TCCR1B
     TCNT4  = 0;//initialize counter value to 0
     // set compare match register for 1hz increments
     OCR4A = 15624/1;// = (16*10^6) / (1*1024) - 1 (must be <65536)
     // turn on CTC mode
     TCCR4B |= (1 << WGM12);
     // Set CS12 and CS10 bits for 1024 prescaler
     TCCR4B |= (1 << CS12) | (1 << CS10);  
     // enable timer compare interrupt
     TIMSK4 |= (1 << OCIE4A);
    
    sei();//allow interrupts
  }

}//end setup

ISR(TIMER4_COMPA_vect){//timer1 interrupt 1Hz toggles pin 13 (LED)
//generates pulse wave of frequency 1Hz/2 = 0.5kHz (takes two cycles for full wave- toggle high then toggle low)

 ttt = 1-ttt;
 Serial.println("bip4");
 digitalWrite(13,toggle4);
 toggle4 = !toggle4;

}


ISR(TIMER1_COMPA_vect){//timer1 interrupt 1Hz toggles pin 13 (LED)
//generates pulse wave of frequency 1Hz/2 = 0.5kHz (takes two cycles for full wave- toggle high then toggle low)

 ttt = 1-ttt;
 Serial.println("bip1");
 digitalWrite(13,toggle4);
 toggle4 = !toggle4;

}

void loop(){
  //Serial.println(ttt);
 //do other things here
}
