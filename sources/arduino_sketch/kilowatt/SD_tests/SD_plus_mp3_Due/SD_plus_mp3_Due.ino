#define CS_pin 10

#include <SD.h>
#include <SPI.h>
#include <DmxMaster.h>
#include <DmxOut.h>
#include "Arduino.h"
#include "DFRobotDFPlayerMini.h"

using namespace std;

// global variables :
File sdcard_file;
DmxOut dmxOut;
unsigned long _timer = millis();
bool _SIMULATION = true;
DFRobotDFPlayerMini myDFPlayer;
const int nbLights = 18;
int channelsToDmxAddressArray[nbLights] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}; // for simulation
//int channelsToDmxAddressArray[nbLights] = {23,22,21,19,18,17,15,14,13,11,10,9,7,6,5,3,2,1}; // for real usage

// function prototypes :
int NumDigits(int x);

int catchTime = 0;

void setup() {
  // Initialize mp3 player :
  Serial3.begin(9600);
  Serial.println();
  Serial.println(F("DFRobot DFPlayer Mini Demo"));
  Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));

  if (!myDFPlayer.begin(Serial3)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while(true);
  }
  Serial.println(F("DFPlayer Mini online."));

  myDFPlayer.volume(15);  //Set volume value. From 0 to 30
  
  Serial.begin(115200); //Setting baudrate
  //Serial.begin(9600); //Setting baudrate
  pinMode(CS_pin, OUTPUT);//declaring CS pin as output pin
  if (SD.begin()){
    Serial.println(F("SD card is initialized and it is ready to use"));
  } else {
    Serial.println(F("SD card is not initialized"));
  return;
  }
  
  dmxOut.setSimulationState(_SIMULATION);

  // send something on channel 32 to initialize dimmer, otherwise it won't work...
  DmxMaster.write(32, 255);
  delay(100);
  DmxMaster.write(32, 0);
  for(int c=0; c<=32; c++){ // then set all channels to 0 before starting, in case other values were kept in memory
    DmxMaster.write(32, 0);
  }
  
}

  
void loop() {
  
  float speedFactor = 2.;
  byte maxFileNb = 14;
  byte fileNb = random(1,14+1);

  // get corresponding file name for dmx sequence :
  byte charCnt = 4-NumDigits(fileNb);
  String fileName = String(fileNb);
  while(charCnt>0){
    fileName = "0"+fileName;
    charCnt--;
  }
  fileName = "DMXSEQ/"+fileName+".TXT";
  //fileName = "DMXSEQ/0005.TXT";
 
  sdcard_file = SD.open(fileName);
  
  int dmxCommand[3];
  
  if (sdcard_file) {
    //Serial.println("Reading from the file"); 
    char curChar;
    String timeDelay = "";
    String dmxChannel = "";
    String dmxValue = "";
    
    curChar = char(sdcard_file.read());
  
    myDFPlayer.play(fileNb);  //Start playing the mp3 file
    //myDFPlayer.play(5);  //Start playing the mp3 file

    //int t_start = millis();
    
    while (sdcard_file.available()) {

      timeDelay = "";
      dmxChannel = "";
      dmxValue = "";
      while(isDigit(curChar)){
        timeDelay += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxChannel += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxValue += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar) && curChar!=EOF){
        curChar = char(sdcard_file.read());
      }

      /*
      int aaa = millis()-_timer;
      Serial.print(F("aaa = "));
      Serial.println(aaa);
      Serial.print(("timeDelay = "));
      Serial.println(timeDelay);
      Serial.print(("(float)timeDelay.toInt()/speedFactor"));
      Serial.println((float)timeDelay.toInt()/speedFactor);
      if(inc>100){
        while(1){};
      }
      */
      
      while(millis()-_timer<(float)timeDelay.toInt()/speedFactor){
        //do nothing ...
      }

      /*if(millis()-_timer>timeDelay.toInt()-catchTime){
        catchTime = (millis()-_timer)-(timeDelay.toInt()-catchTime);
      } else {
        while(millis()-_timer<timeDelay.toInt() - catchTime){
          //do nothing ...
        }
        catchTime = 0;
      }*/
      
      _timer = millis();
      
      /*Serial.print(dmxChannel.toInt()-1);
      Serial.print(" ");
      Serial.print(dmxValue.toInt());
      Serial.println();*/
      dmxOut.writeDMXOutput(channelsToDmxAddressArray[dmxChannel.toInt()-1], dmxValue.toInt());
      
    }
    /*Serial.print("total read time = ");
    Serial.println(millis()-t_start);
    while(1){}*/
    
    sdcard_file.close();
    myDFPlayer.pause();
    delay(5000);
  } else {
    //Serial.println("Failed to open the file");
  }

}

int NumDigits(int x){  
    x = abs(x);  
    return (x < 10 ? 1 :   
        (x < 100 ? 2 :   
        (x < 1000 ? 3 :   
        (x < 10000 ? 4 :   
        (x < 100000 ? 5 :   
        (x < 1000000 ? 6 :   
        (x < 10000000 ? 7 :  
        (x < 100000000 ? 8 :  
        (x < 1000000000 ? 9 :  
        10)))))))));  
}  
