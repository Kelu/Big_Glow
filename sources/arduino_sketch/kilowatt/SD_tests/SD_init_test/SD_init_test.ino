#include <SD.h>
#include <SPI.h>

const int CS_pin = 53; // for SD card reading on Mega

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Setting baudrate

  // init SD card :
  pinMode(CS_pin, OUTPUT);//declaring CS pin as output pin
  while(!SD.begin()){ // try until it works
    Serial.println(F("SD initialization failed... try again"));
  }
  Serial.println(F("SD card is initialized and ready to use"));
}

void loop() {
  // put your main code here, to run repeatedly:

}
