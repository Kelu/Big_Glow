#include <SD.h>
#include <SPI.h>

#include <DmxMaster.h>
#include <DmxOut.h>

using namespace std;

File sdcard_file;
DmxOut dmxOut;
  
unsigned long _timer = millis();

// int CS_pin = 10; 
int CS_pin = 53; // for arduino mega

bool _SIMULATION = true;


const int nbLights = 18;
int channelsToDmxAddressArray[nbLights] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}; // for simulation
//int channelsToDmxAddressArray[nbLights] = {23,22,21,19,18,17,15,14,13,11,10,9,7,6,5,3,2,1};

void setup() {
  Serial.begin(115200); //Setting baudrate
  pinMode(CS_pin, OUTPUT);//declaring CS pin as output pin
  if (SD.begin()){
    Serial.println("SD card is initialized and it is ready to use");
  } else {
    Serial.println("SD card is not initialized");
  return;
  }
  
  dmxOut.setSimulationState(_SIMULATION);

  // send something on channel 32 to initialize dimmer, otherwise it won't work...
  DmxMaster.write(32, 255);
  delay(100);
  DmxMaster.write(32, 0);
  for(int c=0; c<=32; c++){ // then set all channels to 0 before starting, in case other values were kept in memory
    DmxMaster.write(32, 0);
  }
  
}

  
void loop() {
 
  sdcard_file = SD.open("DMXSEQ/0005.TXT"); // 2; 5 
  
  int dmxCommand[3];
  
  if (sdcard_file) {
    Serial.println("Reading from the file"); 
    char curChar;
    String timeDelay = "";
    String dmxChannel = "";
    String dmxValue = "";
      
    curChar = char(sdcard_file.read());
    
    while (sdcard_file.available()) {

      timeDelay = "";
      dmxChannel = "";
      dmxValue = "";
      while(isDigit(curChar)){
        timeDelay += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxChannel += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxValue += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar) && curChar!=EOF){
        curChar = char(sdcard_file.read());
      }
      
      while(millis()-_timer<timeDelay.toInt()){
        //do nothing ...
      }
      _timer = millis();
      /*Serial.print(timeDelay.toInt());
      Serial.print(" ");
      Serial.print(dmxChannel.toInt());
      Serial.print(" ");
      Serial.println(dmxValue.toInt());*/
      dmxOut.writeDMXOutput(channelsToDmxAddressArray[dmxChannel.toInt()-1], dmxValue.toInt());
      
    }
    sdcard_file.close();
  } else {
    Serial.println("Failed to open the file");
  }

  Serial.println("finished reading file");
  delay(5000);
}
