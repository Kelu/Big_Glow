
#include <SD.h>
#include <SPI.h>

// const int CS_pin = 10; // for SD card reading on Uno
const int CS_pin = 53; // for SD card reading on Mega
File sdcard_file;
const int nbLights = 18;
unsigned long _timer = millis();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  // init SD card :
  pinMode(CS_pin, OUTPUT);//declaring CS pin as output pin
  while(!SD.begin()){ // try until it works, in case it fails (which happens sometimes, dependin on the arduino (UNO or MEGA) and SD card (FAT16 32MB or FAT32 8GB) used, I don't know why...)
    //Serial.println(F("SD initialization failed... try again"));
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  String fileName = "DMXSEQ2/01/005.txt";
  readSequenceFromFile_inline(fileName);
}


void readSequenceFromFile_inline(String fileName){

  sdcard_file = SD.open(fileName);
  
  int dmxCommand[3];
  
  if (sdcard_file) { 
    char curChar;
    String dmxValue = "";
    
    curChar = char(sdcard_file.read());
    
    int idx = 0;
    while (sdcard_file.available()) {
      //Serial.println(F("reading something"));
      /*idx = 0;
      while(idx<=nbLights){
        dmxValue = "";
        while(isDigit(curChar)){
          dmxValue += curChar;
          curChar = char(sdcard_file.read());
        }
  
        while(!isDigit(curChar)){
          curChar = char(sdcard_file.read());
        }

        if(idx>0){ // ignore first value which is the index
          int value = dmxValue.toInt();
          //dmxOut2.writeDMXOutput(channelsToDmxAddressArray[idx-1], value);
          Serial.print(idx);
          Serial.print(" ");
          Serial.println(value);
        }
        idx += 1;
      }
      
      while(millis()-_timer<33){
        //do nothing ...
      }*/
      
      curChar = char(sdcard_file.read());
      Serial.print(curChar);
      _timer = millis();
    }
    sdcard_file.close();
  } else {
    //Serial.println(F("failed to read SD card"));
  }
}
