
#include <SD.h>
#include <SPI.h>

File sdcard_file;
// const int CS_pin = 10; // for SD card reading on Uno
const int CS_pin = 53; // for SD card reading on Mega

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  // init SD card :
  pinMode(CS_pin, OUTPUT);//declaring CS pin as output pin
  while(!SD.begin()){ // try until it works, in case it fails (which happens sometimes, dependin on the arduino (UNO or MEGA) and SD card (FAT16 32MB or FAT32 8GB) used, I don't know why...)
    //Serial.println(F("SD initialization failed... try again"));
  }
  //Serial.println(F("SD card is initialized and ready to use"));
}

void loop() {
  // put your main code here, to run repeatedly:
  String fileName = "DMXSEQ/01/004.TXT";
  readFile(fileName);
  while(1);
}

void readFile(String fileName){

  sdcard_file = SD.open(fileName);
  unsigned long duration;
  
  if (sdcard_file) {
    unsigned long start_time = millis();
    while (sdcard_file.available()) {
      //Serial.println(F("reading something"));
      //Serial.print(char(sdcard_file.read()));
      sdcard_file.read();
    }
    duration = millis() - start_time;
    sdcard_file.close();
  } else {
    Serial.println(F("failed to read SD card"));
  }
  Serial.print("duration = ");
  Serial.println(duration);
}
