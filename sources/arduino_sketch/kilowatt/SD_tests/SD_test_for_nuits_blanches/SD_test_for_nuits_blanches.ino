#include <SD.h>
//#include <SPI.h>

#include <DmxMaster.h>
#include <DmxOut.h>
#include <CommonChasers.h>

using namespace std;
 
File sdcard_file;
DmxOut dmxOut_SD;
  
//unsigned long _timer = millis(); // timer for SD reading (different from the one from cc object)
int _timer = millis();

// global control parameters :
const int nbLights = 18;
const int channelsToDmxAddressArray[nbLights] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}; // dmx addresses for software simulation => adapt with order given by Sylvain
//const int channelsToDmxAddressArray[nbLights] = {1,2,3,5,6,7,9,10,11,13,14,15,17,18,19,21,22,23}; // (reverse?) order of dmx channels for kilowatt installation
//const int channelsToDmxAddressArray[nbLights] = {23,22,21,19,18,17,15,14,13,11,10,9,7,6,5,3,2,1}; // order of dmx channels for kilowatt installation
//int nbLightsPerStep = 4; // TODO : 6?
const bool SIMULATION = true; // Set to True for simulation with BigGlow software in max/MSP. Set to False for real usage with dimmer!!
const bool TEST = false; // use for testing (display some infos for debugging. => set to false for real program!!


// create object from CommonChaser class to be able to use the functions from the library :
volatile CommonChasers cc(nbLights, SIMULATION);

// history for knobs values to filter out jitter :
const int filterHistorySize = 20;
volatile int historyIdx = 0;

// hardware controls :
const int knob1Pin = A0; // knob to control min intensity of lights
volatile int knob1Val = 0; 
volatile int knob1ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob2Pin = A1; // knob to control max intensity of lights
volatile int knob2Val = 0;
volatile int knob2ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob3Pin = A2; // knob to control speed of effect
volatile int knob3Val = 0;
volatile int knob3ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob4Pin = A3; // knob to control effect parameter (e.g. ramp speed / fade time of chaser)
volatile int knob4Val = 0;
volatile int knob4ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


void setup() {
  Serial.begin(115200); //Setting baudrate
  char CS_pin = 10; // for SD card reading
  pinMode(CS_pin, OUTPUT);//declaring CS pin as output pin
  if (SD.begin()){
    Serial.println(F("SD card is initialized and it is ready to use")); // use the F() syntax to store strings in flash memory rather than RAM
  } else {
    Serial.println(F("SD card is not initialized"));
  return;
  }

  // send something on channel 32 to initialize dimmer, otherwise it won't work...
  DmxMaster.write(32, 255);
  delay(100);
  DmxMaster.write(32, 0);
  for(int c=0; c<=32; c++){ // then set all channels to 0 before starting, in case other values were kept in memory
    DmxMaster.write(32, 0);
  }
  
}

  
void loop() {
  dmxOut_SD.setSimulationState(SIMULATION);
  sdcard_file = SD.open("DMXSEQ/0005.TXT"); // 2; 5 
  
  int dmxCommand[3];
  
  if (sdcard_file) {
    Serial.println(F("Reading from the file")); 
    char curChar;
    String timeDelay;
    timeDelay.reserve(2); // assuming the time delay in never > 99ms (otherwise would need a longer string)
    String dmxChannel;
    dmxChannel.reserve(2); // assuming the max channel id is 99
    String dmxValue;
    dmxValue.reserve(3); // dmx values are in the range [0-255] so need 3 characters max
      
    curChar = char(sdcard_file.read());
    
    while (sdcard_file.available()) {

      timeDelay = "";
      dmxChannel = "";
      dmxValue = "";
      while(isDigit(curChar)){
        timeDelay += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxChannel += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxValue += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar) && curChar!=EOF){
        curChar = char(sdcard_file.read());
      }
      
      while(millis()-_timer<timeDelay.toInt()){
        //do nothing ...
      }
      _timer = millis();
      /*Serial.print(timeDelay.toInt());
      Serial.print(" ");
      Serial.print(dmxChannel.toInt());
      Serial.print(" ");
      Serial.println(dmxValue.toInt());*/
      dmxOut_SD.writeDMXOutput(channelsToDmxAddressArray[dmxChannel.toInt()-1], dmxValue.toInt());
      
    }
    sdcard_file.close();
  } else {
    Serial.println(F("Failed to open the file"));
  }
}
