
#include <DmxMaster.h>
#include <DmxOut.h> // not usefull if use only functions from CommonChasers to write dmx out, but could be if I write another custom function directly here
#include <CommonChasers.h>

using namespace std;

#include <SD.h>

File sdcard_file;

int timer = millis();

const char commutateurPos1Pin = 117;
const char commutateurPos2Pin PROGMEM = 24;

void setup() {
  // standard baud rates are 1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
  Serial.begin(9600); // choose high baud rate to be able to send a lot of dmx data
}


void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("commutateurPos1Pin = ");
  Serial.println(int(commutateurPos1Pin));
  Serial.print("commutateurPos2Pin = ");
  Serial.println(pgm_read_word(&commutateurPos2Pin));
  delay(1000);
}
