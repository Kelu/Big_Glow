#include <mtDueDMX.h>

uint8_t dmxData[512];
uint8_t dmxDirPin = 7;

uint8_t dmxValue = 0;

void setup()
{
  mtDueDMX[0]->begin(DMX_TX, dmxData, dmxDirPin);
  mtDueDMX[0]->startOutput();
}

void loop()
{
  dmxData[0] = 255;
  delay(1000);
  dmxData[0] = 0;
  delay(1000);
  /*dmxData[0] = dmxValue;
  dmxData[5] = 255 - dmxValue;

  dmxValue++;
  delay(50);*/
}

// /var/folders/wc/80y84t8s3v1ct7tf5q8dzvfr0000gn/T/arduino_build_755507/core/variant.cpp.o
// /Users/Kelu/Library/Arduino15/packages/arduino/hardware/sam/1.6.12/variants/arduino_due_x/variant.cpp
