
// In order to initialize dimmer, we need to first send something (e.g. value 0) to channel 32
// Be sure to have the dimmer in 4ch mode and 8bit dmx mode!!

// TODO : 
//  - effet 1 : effet type néon? (pour remplacer strobe)
//  - ajout d'un 4ème paramètre qui manque encore pour certains effets avec potard 4 (= "dureté", fade time, jitter, ...)

#define commutateurPos1Pin 5
#define commutateurPos2Pin 6
#define commutateurPos3Pin 7
#define knob1Pin A0
#define knob2Pin A1
#define knob3Pin A2
#define knob4Pin A3
#define switchPin 8
#define grosBoutonRougePin 9

#include <DmxMaster.h>
#include <DmxOut.h> // not usefull if use only functions from CommonChasers to write dmx out, but could be if I write another custom function directly here
#include <CommonChasers.h>

using namespace std;

#include <SD.h>

File sdcard_file;
DmxOut dmxOut_SD;
  
//unsigned long _timer = millis(); // timer for SD reading (different from the one from cc object)
int _timer = millis();

// global control parameters :
const int nbLights = 18;
const int channelsToDmxAddressArray[nbLights] PROGMEM = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}; // dmx addresses for software simulation => adapt with order given by Sylvain
//int channelsToDmxAddressArray[nbLights] = {1,2,3,5,6,7,9,10,11,13,14,15,17,18,19,21,22,23}; // (reverse?) order of dmx channels for kilowatt installation
//int channelsToDmxAddressArray[nbLights] = {23,22,21,19,18,17,15,14,13,11,10,9,7,6,5,3,2,1}; // order of dmx channels for kilowatt installation
int nbLightsPerStep = 4; // TODO : 6?
const bool SIMULATION PROGMEM = true; // Set to True for simulation with BigGlow software in max/MSP. Set to False for real usage with dimmer!!

// create object from CommonChaser class to be able to use the functions from the library :
volatile CommonChasers cc(nbLights, SIMULATION);

// history for knobs values to filter out jitter :
//const char filterHistorySize PROGMEM = 20;
//volatile char historyIdx = 0;

// hardware controls :
//const char knob1Pin = A0; // knob to control min intensity of lights
//volatile int knob1Val = 0; 
//volatile int knob1ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

//const char knob2Pin = A1; // knob to control max intensity of lights
//volatile int knob2Val = 0;
//volatile int knob2ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

//const char knob3Pin = A2; // knob to control speed of effect
//volatile int knob3Val = 0;
//volatile int knob3ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

//const char knob4Pin = A3; // knob to control effect parameter (e.g. ramp speed / fade time of chaser)
//volatile int knob4Val = 0;
//volatile int knob4ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

//const char commutateurPos1Pin = 5; // select 1st programm
bool commutateurPos1Val = 0;

//const char commutateurPos2Pin = 6; // select 2nd programm
bool commutateurPos2Val = 0;

//const char commutateurPos3Pin = 7; // select 3rd programm
bool commutateurPos3Val = 0;

//const int commutateurPos4Pin = NONE; // select 4th programm => 4th position doesn't work, so choose it when all other 3 are at 0.
//int commutateurPos4Val = 0;

char commutateurPos = 1; // position can be 1 to 4, depending on states of commutateurPos1Val, commutateurPos2Val, and commutateurPos3Val
char commutateurPosOld = 1; // retain previous position of commutateurPos

//const char switchPin = 8;
bool switchVal = 0;
bool switchValOld = 0;

//const char grosBoutonRougePin = 9;
bool grosBoutonRougeVal = 0;
bool grosBoutonRougeValOld = 0;

// program control variables :
volatile unsigned char minIntensityValue = 0; // TODO : set at least to 1 for real installation, so that there is always some lights
unsigned char minIntensityValueOld = 0;
volatile unsigned char maxIntensityValue = 255; // TODO : 100 for real installation (otherwise it is too bright, as the leds are too powerfull) // why volatile??
unsigned char maxIntensityValueOld = 255;
const int min_time_step_ms PROGMEM = 50;
const int max_time_step_ms PROGMEM = 1000;
volatile int time_step_ms = 250;
volatile bool prog1State = 1;
volatile bool prog2State = 0;
volatile bool prog3State = 0;
volatile bool prog4State = 0;
//volatile float effectParam1 = 0.5;
volatile unsigned char effectParam1 = 127;

volatile bool runArcElectric = false;

void setup() {
  // standard baud rates are 1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
  Serial.begin(115200); // choose high baud rate to be able to send a lot of dmx data

  // configure proper dmx channels order of lights :
  cc.setChannelsToDmxAddressArray(channelsToDmxAddressArray);

  if(true){ // to activate/deactivate interrupts, for debug purpose (should set to true for final version)
    // set-up interrupt to be able to read hardware input values at regular intervals without stopping running programm:
    cli();//stop interrupts
    
    //set timer1 interrupt at ~50Hz
    TCCR1A = 0;// set entire TCCR1A register to 0
    TCCR1B = 0;// same for TCCR1B
    TCNT1  = 0;//initialize counter value to 0
    // set compare match register for 50hz increments
    //OCR1A = 312;// = (16*10^6) / (50*1024) - 1 (must be <65536)
    // set compare match register for 10hz increments
    OCR1A = 1562;// = (16*10^6) / (10*1024) - 1 (must be <65536)
    // turn on CTC mode~
    TCCR1B |= (1 << WGM12);
    // Set CS12 and CS10 bits for 1024 prescaler
    TCCR1B |= (1 << CS12) | (1 << CS10);  
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
  
    sei();//allow interrupts
  }

  // set digital I/O in "INPUT_PULLUP" mode to read buttons/switchs values from hardware interface
  pinMode(commutateurPos1Pin, INPUT_PULLUP);
  pinMode(commutateurPos2Pin, INPUT_PULLUP);
  pinMode(commutateurPos3Pin, INPUT_PULLUP);
  //pinMode(commutateurPos4Pin, INPUT_PULLUP);
  pinMode(grosBoutonRougePin, INPUT_PULLUP);
  pinMode(switchPin, INPUT_PULLUP);
  
  char CS_pin = 10; // for SD card reading
  pinMode(CS_pin, OUTPUT);//declaring CS pin as output pin

  // send something on channel 32 to initialize dimmer, otherwise it won't work...
  DmxMaster.write(32, 255); // not sure if necessary...
  delay(100); // not sure if necessary ...
  DmxMaster.write(32, 0); // definitely necessary to send at least 1 value (here 0) to channel 32, otherwise nothing will happen!!

  // set all channels to 0 before starting, in case other values were kept in memory
  for(int c=0; c<=32; c++){
    DmxMaster.write(32, 0);
  }

}


int knobToSpeed(float knobVal){ // time step is in ms
  float progTimeStep_ms = map(knobVal, 0, 1023, max_time_step_ms, min_time_step_ms); 
  return progTimeStep_ms;
}


int knobToIntensity(float knobVal){ // time step is in ms
  int intensity = map(knobVal, 0, 1023, 0, 255); // map analog knob value to dmx value range
  return intensity;
}


float map(float x, float in_min, float in_max, float out_min, float out_max) {
  // rewrite the map function here, since the builtin map function seem to output only integer values
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


/*int averageFilter(int currentVal, int historyIdx, int valuesArray[]){ // smoothing filter to prevent from jitter on knob values
  valuesArray[historyIdx] = currentVal;
  int sum = 0;
  for (int i=0; i<filterHistorySize; i++){
    sum += valuesArray[i];
  }
  int average = sum/filterHistorySize;
  return average;
}*/


// interrupt function :
ISR(TIMER1_COMPA_vect){ // timer1 interrupt at 10Hz (read knob and buttons states every ~20ms)

  // read the position of the 4-positions selector :
  commutateurPosOld = commutateurPos;
  commutateurPos = readCommutateurPosition();
  if(commutateurPos != commutateurPosOld){
    cc.breakFunction();
  }

  // read other buttons states :
  grosBoutonRougeValOld = grosBoutonRougeVal;
  grosBoutonRougeVal = 1 - digitalRead(grosBoutonRougePin);
  if(grosBoutonRougeVal != grosBoutonRougeValOld){
    cc.breakFunction();
  }
  switchValOld = switchVal;
  switchVal = 1 - digitalRead(switchPin);
  if(switchVal != switchValOld){
    runArcElectric = true;
    cc.breakFunction();
  }
  
  // read knobs values
  //historyIdx = (historyIdx+1)%filterHistorySize;

  int knob1Val;
  knob1Val = analogRead(knob1Pin); // analogRead should give values in range [0-1023] for Vcc=5V (686 if Vcc=3.3V) => use 5V
  //knob1Val = averageFilter(knob1Val, historyIdx, knob1ValHistory); // smooth values to avoid jitter
  minIntensityValue = knobToIntensity(knob1Val); // default : 1

  int knob2Val;
  knob2Val = analogRead(knob2Pin); // analogRead should give values in range [0-1023] for Vcc=5V (686 if Vcc=3.3V) => use 5V
  //knob2Val = averageFilter(knob2Val, historyIdx, knob2ValHistory); // smooth values to avoid jitter
  maxIntensityValue = knobToIntensity(knob2Val); // default : 100
  
  int knob3Val;
  knob3Val = analogRead(knob3Pin); // analogRead should give values in range [0-1023] for Vcc=5V (686 if Vcc=3.3V) => use 5V
  //knob3Val = averageFilter(knob3Val, historyIdx, knob3ValHistory); // smooth to avoid jitter
  time_step_ms = knobToSpeed(knob3Val); // default : 1000
  
  int knob4Val;
  knob4Val = analogRead(knob4Pin); // analogRead should give values in range [0-1023] for Vcc=5V (686 if Vcc=3.3V) => use 5V
  //knob4Val = averageFilter(knob4Val, historyIdx, knob4ValHistory); // smooth to avoid jitter
  //effectParam1 = map(float(knob4Val), 0, 1023, 0., 1.); // default : depends on effect
  effectParam1 = int(map(float(knob4Val), 0, 1023, 0, 255)); // default : depends on effect


  //Force default values (only for testing) : 
  //minIntensityValue = 0;
  //maxIntensityValue = 255;
  //time_step_ms = 250;

  if(minIntensityValue!=minIntensityValueOld){
    cc.setMinIntensityVal(minIntensityValue);
    minIntensityValueOld = minIntensityValue;
  }
  if(maxIntensityValue!=maxIntensityValueOld){
    cc.setMaxIntensityVal(maxIntensityValue);
    maxIntensityValueOld = maxIntensityValue;
  }
  cc.setTimeStep(time_step_ms);
}

int readCommutateurPosition(){ // read the 4 states commutateur position
  commutateurPosOld = commutateurPos;
  commutateurPos1Val = 1 - digitalRead(commutateurPos1Pin);
  commutateurPos2Val = 1 - digitalRead(commutateurPos2Pin);
  commutateurPos3Val = 1 - digitalRead(commutateurPos3Pin);
  //commutateurPos4Val = 1 - digitalRead(commutateurPos4Pin);
  if(commutateurPos1Val==1 && commutateurPos2Val==0 && commutateurPos3Val==0){
    commutateurPos = 1;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==1 && commutateurPos3Val==0){
    commutateurPos = 2;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==0 && commutateurPos3Val==1){
    commutateurPos = 3;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==0 && commutateurPos3Val==0){
    commutateurPos = 4;
  } else{ // other states don't exist and should not occur
    commutateurPos = commutateurPosOld;
  }
  return commutateurPos;
}

void chenillardExterieurVersInterieur(){ // custom effect with "home-made" sequence
  // set dmx sequence as a list a [time delay, channel, value] triplets
  // use -1 as time delay value which will be replaced by an appropriate value according to effect speed
  
  // chenillard exterieur vers l'intérieur, avec les lampes qui restent allumées jusqu'au bout de la séquence :
  int nbSteps = 19;
  int dmxSequence[nbSteps][3] = {{0,-1,0},
                            {0,1,255},
                            {-1,18,255},
                            {0,2,255},
                            {-1,17,255},
                            {0,3,255},
                            {-1,16,255},
                            {0,4,255},
                            {-1,15,255},
                            {0,5,255},
                            {-1,14,255},
                            {0,6,255},
                            {-1,13,255},
                            {0,7,255},
                            {-1,12,255},
                            {0,8,255},
                            {-1,11,255},
                            {0,9,255},
                            {-1,10,255}};
                            
  /*
  // chenillard exterieur vers l'intérieur, avec seulement 2 lampes allumées à la fois :
  int nbSteps = 27;
  int dmxSequence[nbSteps][3] = {
                          {0,-1,0}, // set channel to -1 to address all channels at the same time
                          {0,1,255},
                          {-1,18,255},
                          {0,-1,0},
                          {0,2,255},
                          {-1,17,255},
                          {0,-1,0},
                          {0,3,255},
                          {-1,16,255},
                          {0,-1,0},
                          {0,4,255},
                          {-1,15,255},
                          {0,-1,0},
                          {0,5,255},
                          {-1,14,255},
                          {0,-1,0},
                          {0,6,255},
                          {-1,13,255},
                          {0,-1,0},
                          {0,7,255},
                          {-1,12,255},
                          {0,-1,0},
                          {0,8,255},
                          {-1,11,255},
                          {0,-1,0},
                          {0,9,255},
                          {-1,10,255}
                          };*/
                            
  cc.readSequence(dmxSequence, nbSteps);
}

void readSequenceFromFile() {
 
  sdcard_file = SD.open("DMXSEQ/0005.TXT"); // 2; 5 
  
  int dmxCommand[3];
  
  if (sdcard_file) {
    Serial.println(F("Reading from the file")); 
    char curChar;
    String timeDelay;
    timeDelay.reserve(2); // assuming the time delay in never > 99ms (otherwise would need a longer string)
    String dmxChannel;
    dmxChannel.reserve(2); // assuming the max channel id is 99
    String dmxValue;
    dmxValue.reserve(3); // dmx values are in the range [0-255] so need 3 characters max
      
    curChar = char(sdcard_file.read());
    
    while (sdcard_file.available()) {

      timeDelay = "";
      dmxChannel = "";
      dmxValue = "";
      while(isDigit(curChar)){
        timeDelay += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxChannel += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxValue += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar) && curChar!=EOF){
        curChar = char(sdcard_file.read());
      }
      
      while(millis()-_timer<timeDelay.toInt()){
        //do nothing ...
      }
      _timer = millis();
      /*Serial.print(timeDelay.toInt());
      Serial.print(" ");
      Serial.print(dmxChannel.toInt());
      Serial.print(" ");
      Serial.println(dmxValue.toInt());*/
      dmxOut_SD.writeDMXOutput(channelsToDmxAddressArray[dmxChannel.toInt()-1], dmxValue.toInt());
      
    }
    sdcard_file.close();
  } else {
    Serial.println(F("Failed to open the file"));
  }
}

void loop() {

  readSequenceFromFile();
  
  /*
  if(runArcElectric){
    readFromFile();
    runArcElectric = false;
  }*/

  /*
  if(commutateurPos==1){
    cc.strobe(); // remplacer par effet type néon qui a du mal à s'allumer (avec du son??)
  } else if(commutateurPos==2){
    bool onOffMode = true;
    nbLightsPerStep = map(effectParam1, 0, 255, 1, nbLights);
    cc.randomChaserMultipleLightsPerStep(nbLightsPerStep, onOffMode); // TODO : régler nombre de lights/step avec un potard
  } else if(commutateurPos==3){
    float freqSin = 1./(time_step_ms/1000.); // set sinusoid frequency for sinusoid effect according to effect speed (time_step_ms) value
    freqSin = freqSin / 2; // divide by 2 otherwise it's too fast
    float nbPeriods = 1.;
    bool directionRight = true;
    //cc.sinusoid(freqSin, nbPeriods, directionRight);
    float interpFactor = map(effectParam1, 0, 255, 0., 1.);
    float threshold = 0.5;
    cc.sinRectInterp(freqSin, nbPeriods, directionRight, interpFactor, threshold);
  } else if(commutateurPos==4){
    chenillardExterieurVersInterieur();
  }
  */

}
