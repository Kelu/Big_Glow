
// In order to initialize dimmer, we need to first send something (e.g. value 0) to channel 32
// Be sure to have the dimmer in 4ch mode and 8bit dmx mode!!

// TODO : 
// - effet 1 : effet type néon? (pour remplacer strobe) !!! => à mixer avec effet aléatoire?
// - param knob4 effet 1 = fade out time => pour effet extérieur/intérieur
// - bien régler les valeurs de tout les paramètres !!! => presqu'OK (?)
// - régler le problème carte SD n'est pas bien lue à tout les coups... => OK (?) (ok pour l'instant, mais est-ce que ça va persister dans le temps?)
// - rendre le switch inactif pendant 3 minutes. (Noir dure 25s après l'effet)

#include <DmxMaster.h>
#include <DmxOut.h> // not usefull if use only functions from CommonChasers to write dmx out, but could be if I write another custom function directly here
#include <CommonChasers.h>
#include <SD.h>
#include <SPI.h>
#include "DFRobotDFPlayerMini.h"

int knobToSpeed(float knobVal);
int knobToIntensity(float knobVal, int minVal, int maxVal);
float map(float x, float in_min, float in_max, float out_min, float out_max);
int readCommutateurPosition();
void chenillardExterieurVersInterieur();
int averageFilter(int currentVal, int historyIdx, int valuesArray[]);
void readSequenceFromFile(String fileName, float speedFactor);
void readSequenceFromFile_inline_old_old(String fileName, float remapValMin, float remapValMax);
void readSequenceFromFile_inline_old(String fileName, float remapValMin, float remapValMax);
void readSequenceFromFile_inline(String fileName, float remapValMin, float remapValMax);
int getNumDigits(int x);
void arcElectricFunction();
void grosButonRougeFunction();
void KW_onOff(bool KW);
void effet1Chaser(byte chaseNum);


// global control parameters :
const int nbLights = 18;
//int channelsToDmxAddressArray[nbLights] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}; // dmx addresses for software simulation => adapt with order given by Sylvain
int channelsToDmxAddressArray[nbLights] = {1,2,3,5,6,7,9,10,11,13,14,15,17,18,19,21,22,23}; // (reverse?) order of dmx channels for kilowatt installation
//int channelsToDmxAddressArray[nbLights] = {23,22,21,19,18,17,15,14,13,11,10,9,7,6,5,3,2,1}; // order of dmx channels for kilowatt installation
const int KW_channel = 19; // set channel of light(s) to light up KW sign // TODO : set to propor channel value
int nbLightsPerStep = 4; // TODO : 6?
const bool SIMULATION = false; // Set to True for simulation with BigGlow software in max/MSP. Set to False for real usage with dimmer!!
const bool TEST = false; // use for testing (display some infos for debugging. => set to false for real program!!
unsigned long _timer = millis();
DFRobotDFPlayerMini myDFPlayer;

int blackOutDelay = 25000; // 25000;
unsigned long switchResetTimer = 0;
//unsigned long switchResetDelay = 180000;
unsigned long switchResetDelay = 120000;
bool firstTime = true;

// create object from CommonChaser class to be able to use the functions from the library :
volatile CommonChasers cc(nbLights, SIMULATION);

// history for knobs values to filter out jitter :
const int filterHistorySize = 20;
volatile int historyIdx = 0;

// hardware controls :
const int knob1Pin = A0; // knob to control min intensity of lights
volatile int knob1Val = 0; 
volatile int knob1ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob2Pin = A1; // knob to control max intensity of lights
volatile int knob2Val = 0;
volatile int knob2ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob3Pin = A2; // knob to control speed of effect
volatile int knob3Val = 0;
volatile int knob3ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int knob4Pin = A3; // knob to control effect parameter (e.g. ramp speed / fade time of chaser)
volatile int knob4Val = 0;
volatile int knob4ValHistory[filterHistorySize] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const int commutateurPos1Pin = 5; // select 1st programm
int commutateurPos1Val = 0;

const int commutateurPos2Pin = 6; // select 2nd programm
int commutateurPos2Val = 0;

const int commutateurPos3Pin = 7; // select 3rd programm
int commutateurPos3Val = 0;

//const int commutateurPos4Pin = NONE; // select 4th programm => 4th position doesn't work, so choose it when all other 3 are at 0.
//int commutateurPos4Val = 0;

int commutateurPos = 1; // position can be 1 to 4, depending on states of commutateurPos1Val, commutateurPos2Val, and commutateurPos3Val
int commutateurPosOld = 1; // retain previous position of commutateurPos

const int switchPin = 8;
int switchVal = -1;
int switchValOld = 0;

const int grosBoutonRougePin = 9;
int grosBoutonRougeVal = 0;
int grosBoutonRougeValOld = 0;

// program control variables :
const int minMinIntensityValue = 0; // TODO : set at least to 1 for real installation, so that there is always some lights? or 0?
const int maxMinIntensityValue = 60; // TODO : set at least to 1 for real installation, so that there is always some lights? or 0?
volatile int minIntensityValue = 0;
int minIntensityValueOld = 0;
const int minMaxIntensityValue = 5; // TODO : 100 for real installation (otherwise it is too bright, as the leds are too powerfull)
const int maxMaxIntensityValue = 100; // TODO : 100 for real installation (otherwise it is too bright, as the leds are too powerfull)
volatile int maxIntensityValue = 100;
int maxIntensityValueOld = 255;
int min_time_step_ms = 50;
int max_time_step_ms = 1000;
volatile int time_step_ms = 250;
volatile int prog1State = 1;
volatile int prog2State = 0;
volatile int prog3State = 0;
volatile int prog4State = 0;
volatile float effectParam1 = 0.5;

volatile bool runArcElectric = false;

bool KW_state = 0;

// const int CS_pin = 10; // for SD card reading on Uno
const int CS_pin = 53; // for SD card reading on Mega
DmxOut dmxOut2;
File sdcard_file;


// interrupt function :
ISR(TIMER1_COMPA_vect){ // timer1 interrupt at 50Hz (read knob and buttons states every ~20ms)

  // read the position of the 4-positions selector :
  commutateurPosOld = commutateurPos;
  commutateurPos = readCommutateurPosition();
  if(commutateurPos != commutateurPosOld){
    cc.breakFunction();
  }

  // read other buttons states :
  grosBoutonRougeValOld = grosBoutonRougeVal;
  grosBoutonRougeVal = 1 - digitalRead(grosBoutonRougePin);
  //if(grosBoutonRougeVal != grosBoutonRougeValOld){
  if(grosBoutonRougeVal != KW_state){
    //KW_onOff(!KW_state);
    KW_state = grosBoutonRougeVal;
    cc.breakFunction();
  }
  
  switchValOld = switchVal;
  switchVal = 1 - digitalRead(switchPin);
  if((switchVal!=switchValOld) && (switchValOld!=-1)){
    if((millis()-switchResetTimer>switchResetDelay) || (firstTime)){
      runArcElectric = true;
      cc.breakFunction();
    }
  }
  
  // read knobs values
  historyIdx = (historyIdx+1)%filterHistorySize;
  
  knob1Val = 1023 - analogRead(knob1Pin); // analogRead should give values in range [0-1023] for Vcc=5V (686 if Vcc=3.3V) => use 5V
  knob1Val = averageFilter(knob1Val, historyIdx, knob1ValHistory); // smooth values to avoid jitter
  minIntensityValue = knobToIntensity(knob1Val, minMinIntensityValue, maxMinIntensityValue); // default : 1
  
  knob2Val = 1023 - analogRead(knob2Pin); // analogRead should give values in range [0-1023] for Vcc=5V (686 if Vcc=3.3V) => use 5V
  knob2Val = averageFilter(knob2Val, historyIdx, knob2ValHistory); // smooth values to avoid jitter
  maxIntensityValue = knobToIntensity(knob2Val, minMaxIntensityValue, maxMaxIntensityValue); // default : 100
  
  knob3Val = 1023 - analogRead(knob3Pin); // analogRead should give values in range [0-1023] for Vcc=5V (686 if Vcc=3.3V) => use 5V
  knob3Val = averageFilter(knob3Val, historyIdx, knob3ValHistory); // smooth to avoid jitter
  time_step_ms = knobToSpeed(knob3Val); // default : 1000
  
  knob4Val = 1023 - analogRead(knob4Pin); // analogRead should give values in range [0-1023] for Vcc=5V (686 if Vcc=3.3V) => use 5V
  knob4Val = averageFilter(knob4Val, historyIdx, knob4ValHistory); // smooth to avoid jitter
  effectParam1 = map(float(knob4Val), 0, 1023, 0., 1.); // default : depends on effect
  cc.setEffectParam1(effectParam1);

  //Force default values (only for testing) : 
  //minIntensityValue = 0;
  //maxIntensityValue = 255;
  //time_step_ms = 250;

  if(minIntensityValue!=minIntensityValueOld){
    cc.setMinIntensityVal(minIntensityValue);
    minIntensityValueOld = minIntensityValue;
  }
  if(maxIntensityValue!=maxIntensityValueOld){
    cc.setMaxIntensityVal(maxIntensityValue);
    maxIntensityValueOld = maxIntensityValue;
  }
  cc.setTimeStep(time_step_ms);

}


void setup() {
  // init serial port (for simulation or debug, otherwise not necessary) :
  // standard baud rates are 1200, 2400, 4800, 9600, 19200, 38400, 57600, and 115200
  Serial.begin(115200); // choose high baud rate to be able to send a lot of dmx data
  //Serial.begin(9600); //Setting baudrate

  // init SD card :
  pinMode(CS_pin, OUTPUT);//declaring CS pin as output pin
  while(!SD.begin()){ // try until it works, in case it fails (which happens sometimes, dependin on the arduino (UNO or MEGA) and SD card (FAT16 32MB or FAT32 8GB) used, I don't know why...)
    Serial.println(F("SD initialization failed... try again"));
  }
  Serial.println(F("SD card is initialized and ready to use"));
  
  /*if (SD.begin()){
    Serial.println(F("SD card is initialized and it is ready to use"));
  } else {
    Serial.println(F("SD card is not initialized"));
  return;
  }*/

  // Initialize mp3 player :
  Serial3.begin(9600);
  //Serial.println();
  //Serial.println(F("DFRobot DFPlayer Mini Demo"));
  //Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));

  if (!myDFPlayer.begin(Serial3)) {  //Use softwareSerial to communicate with mp3.
    //Serial.println(F("Unable to begin:"));
    //Serial.println(F("1.Please recheck the connection!"));
    //Serial.println(F("2.Please insert the SD card!"));
    while(true);
  }
  //Serial.println(F("DFPlayer Mini online."));

  myDFPlayer.volume(22);  //Set volume value. From 0 to 30 => 25?

  // set-up timer interrupts :
  if(true){ // to activate/deactivate interrupts, for debug purpose (should set to true for final version)
    // set-up interrupt to be able to read hardware input values at regular intervals without stopping running programm:
    cli();//stop interrupts
    
    //set timer1 interrupt at ~10Hz
    TCCR1A = 0;// set entire TCCR1A register to 0
    TCCR1B = 0;// same for TCCR1B
    TCNT1  = 0;//initialize counter value to 0
    // set compare match register for 50hz increments
    OCR1A = 312;// = (16*10^6) / (50*1024) - 1 (must be <65536)
    //// set compare match register for 10hz increments
    //OCR1A = 1562;// = (16*10^6) / (10*1024) - 1 (must be <65536)
    // turn on CTC mode~
    TCCR1B |= (1 << WGM12);
    // Set CS12 and CS10 bits for 1024 prescaler
    TCCR1B |= (1 << CS12) | (1 << CS10);  
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
  
    sei();//allow interrupts
  }

  // set digital I/O in "INPUT_PULLUP" mode to read buttons/switchs values from hardware interface
  pinMode(commutateurPos1Pin, INPUT_PULLUP);
  pinMode(commutateurPos2Pin, INPUT_PULLUP);
  pinMode(commutateurPos3Pin, INPUT_PULLUP);
  //pinMode(commutateurPos4Pin, INPUT_PULLUP);
  pinMode(grosBoutonRougePin, INPUT_PULLUP);
  pinMode(switchPin, INPUT_PULLUP);

  // configure proper dmx channels order of lights :
  cc.setChannelsToDmxAddressArray(channelsToDmxAddressArray);
  
  // set simulation state (or not for real usage) :
  dmxOut2.setSimulationState(SIMULATION);
  
  // send something on channel 32 to initialize dimmer, otherwise it won't work...
  DmxMaster.write(32, 255); // not sure if necessary...
  delay(100); // not sure if necessary ...
  DmxMaster.write(32, 0); // definitely necessary to send at least 1 value (here 0) to channel 32, otherwise nothing will happen!!

  // set all channels to 0 before starting, in case other values were kept in memory
  for(int c=0; c<=32; c++){
    DmxMaster.write(c, 0);
  }
  
  randomSeed(analogRead(6)); // seed the rnd generator with noise from unused pin
  
  //KW_onOff(0); // light KW spots to "normal" (low) level
  runArcElectric = false; // make sure the program doesn't start with runArcElectric effect
  
}

void loop() {
  
  if(runArcElectric){
    if((millis()-switchResetTimer>switchResetDelay) || firstTime){
      if(firstTime){
        firstTime = false;
      }
      myDFPlayer.volume(30); // volume max pour les effets d'arcs électriques
      arcElectricFunction();
      myDFPlayer.volume(22); // un peu moins fort pour la musique
      switchResetTimer = millis();
    }
  }
  
  else if(grosBoutonRougeVal){
    grosButonRougeFunction();
  }
  
  else if(commutateurPos==1){
    if(TEST){
      Serial.println("effet 1");
    } else {

      //neonFlicker();
      
      int chaseNum = int(map(effectParam1, 0., 1., 0, 12));
      effet1Chaser(chaseNum);
    }
  } else if(commutateurPos==2){
    if(TEST){
      Serial.println("randomChaserMultipleLightsPerStep");
    } else {
      bool onOffMode = true;
      nbLightsPerStep = int(map(effectParam1, 0., 1., 1, 9));
      cc.randomChaserMultipleLightsPerStep(nbLightsPerStep, onOffMode); // TODO : régler nombre de lights/step avec un potard
    }
  } else if(commutateurPos==3){
    if(TEST){
      Serial.println("sinRectInterp");
    } else {
      //float freqSin = 1./(time_step_ms/1000.); // set sinusoid frequency for sinusoid effect according to effect speed (time_step_ms) value
      bool directionRight = true;
      //cc.sinusoid(freqSin, nbPeriods, directionRight);
      float freqSin = map(float(knob3Val), 0., 1023., -4., 4.); // set sinusoid frequency
      float threshold = 0.5;
      //float interpFactor = effectParam1;
      float interpFactor = map(float(constrain(effectParam1, 0, 0.5)), 0., 0.5, 0., 1.);
      //float nbPeriods = 1.;
      float nbPeriods = map(float(constrain(effectParam1, 0.5, 1.)), 0.5, 1., 1., 3.); // set nb periods to be displayed
      cc.sinRectInterp(freqSin, nbPeriods, directionRight, interpFactor, threshold);
    }
  } else if(commutateurPos==4){
    if(TEST){
      Serial.println("chenillardExterieurVersInterieur");
    } else {
      chenillardExterieurVersInterieur();
    }
  }
  
}


int knobToSpeed(float knobVal){ // time step is in ms
  float progTimeStep_ms = map(knobVal, 0, 1023, max_time_step_ms, min_time_step_ms); 
  return progTimeStep_ms;
}


int knobToIntensity(float knobVal, int minVal, int maxVal){ // time step is in ms
  int intensity = map(knobVal, 0, 1023, minVal, maxVal); // map analog knob value to dmx value range
  return intensity;
}


float map(float x, float in_min, float in_max, float out_min, float out_max) {
  // rewrite the map function here, since the builtin map function seem to output only integer values
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


int averageFilter(int currentVal, int historyIdx, int valuesArray[]){ // smoothing filter to prevent from jitter on knob values
  valuesArray[historyIdx] = currentVal;
  int sum = 0;
  for (int i=0; i<filterHistorySize; i++){
    sum += valuesArray[i];
  }
  int average = sum/filterHistorySize;
  return average;
}

int readCommutateurPosition(){ // read the 4 states commutateur position
  commutateurPosOld = commutateurPos;
  commutateurPos1Val = 1 - digitalRead(commutateurPos1Pin);
  commutateurPos2Val = 1 - digitalRead(commutateurPos2Pin);
  commutateurPos3Val = 1 - digitalRead(commutateurPos3Pin);
  //commutateurPos4Val = 1 - digitalRead(commutateurPos4Pin);
  if(commutateurPos1Val==1 && commutateurPos2Val==0 && commutateurPos3Val==0){
    commutateurPos = 1;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==1 && commutateurPos3Val==0){
    commutateurPos = 2;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==0 && commutateurPos3Val==1){
    commutateurPos = 3;
  } else if(commutateurPos1Val==0 && commutateurPos2Val==0 && commutateurPos3Val==0){
    commutateurPos = 4;
  } else{ // other states don't exist and should not occur
    commutateurPos = commutateurPosOld;
  }
  return commutateurPos;
}

void chenillardExterieurVersInterieur(){ // custom effect with "home-made" sequence
  // set dmx sequence as a list a [time delay, channel, value] triplets
  // use -1 as time delay value which will be replaced by an appropriate value according to effect speed
  
  // chenillard exterieur vers l'intérieur, avec les lampes qui restent allumées jusqu'au bout de la séquence :
  int nbSteps = 19;
  int dmxSequence[nbSteps][3] = {{0,-1,0},
                            {0,1,255},
                            {-1,18,255},
                            {0,2,255},
                            {-1,17,255},
                            {0,3,255},
                            {-1,16,255},
                            {0,4,255},
                            {-1,15,255},
                            {0,5,255},
                            {-1,14,255},
                            {0,6,255},
                            {-1,13,255},
                            {0,7,255},
                            {-1,12,255},
                            {0,8,255},
                            {-1,11,255},
                            {0,9,255},
                            {-1,10,255}};
                            
  
  // chenillard exterieur vers l'intérieur, avec seulement 2 lampes allumées à la fois :
  /*int nbSteps = 27;
  int dmxSequence[nbSteps][3] = {
                          {0,-1,0}, // set channel to -1 to address all channels at the same time
                          {0,1,255},
                          {-1,18,255},
                          {0,-1,0},
                          {0,2,255},
                          {-1,17,255},
                          {0,-1,0},
                          {0,3,255},
                          {-1,16,255},
                          {0,-1,0},
                          {0,4,255},
                          {-1,15,255},
                          {0,-1,0},
                          {0,5,255},
                          {-1,14,255},
                          {0,-1,0},
                          {0,6,255},
                          {-1,13,255},
                          {0,-1,0},
                          {0,7,255},
                          {-1,12,255},
                          {0,-1,0},
                          {0,8,255},
                          {-1,11,255},
                          {0,-1,0},
                          {0,9,255},
                          {-1,10,255}
                          };*/

  cc.readSequence(dmxSequence, nbSteps);
}

void readSequenceFromFile(String fileName, float speedFactor){

  sdcard_file = SD.open(fileName);
  
  int dmxCommand[3];
  
  if (sdcard_file) { 
    char curChar;
    String timeDelay = "";
    String dmxChannel = "";
    String dmxValue = "";
    
    curChar = char(sdcard_file.read());

    while (sdcard_file.available()) {
      //Serial.println(F("reading something"));

      timeDelay = "";
      dmxChannel = "";
      dmxValue = "";
      while(isDigit(curChar)){
        timeDelay += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxChannel += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar)){
        curChar = char(sdcard_file.read());
      }
      
      while(isDigit(curChar)){
        dmxValue += curChar;
        curChar = char(sdcard_file.read());
      }

      while(!isDigit(curChar) && curChar!=EOF){
        curChar = char(sdcard_file.read());
      }
      
      while(millis()-_timer<(float)timeDelay.toInt()/speedFactor){
        //do nothing ...
      }

      if(!runArcElectric && !grosBoutonRougeVal){
        return;
      }
      
      _timer = millis();

      int value = dmxValue.toInt();
      value = min(max(0, map(float(value), 100., 172., 0., 255.)), 255);
      
      dmxOut2.writeDMXOutput(channelsToDmxAddressArray[dmxChannel.toInt()-1], value);
      
    }
    sdcard_file.close();
  } else {
    //Serial.println(F("failed to read SD card"));
  }
}

void readSequenceFromFile_inline_old_old(String fileName, float remapValMin, float remapValMax){

  sdcard_file = SD.open(fileName);
  
  int dmxCommand[3];
  
  if (sdcard_file) { 
    char curChar;
    String dmxValue = "";
    
    curChar = char(sdcard_file.read());
    
    int idx;
    while (sdcard_file.available()){
      //Serial.println(F("reading something"));
      idx = 0;
      while(idx<=nbLights){
        dmxValue = "";
        while(isDigit(curChar)){
          dmxValue += curChar;
          curChar = char(sdcard_file.read());
        }
  
        while(!isDigit(curChar) && curChar!=EOF){
          curChar = char(sdcard_file.read());
        }

        if(idx>0){ // ignore first value which is the index (for old file, but now I removed the index)
          int value = dmxValue.toInt();
          value = min(max(0, map(float(value), remapValMin, remapValMax, 0., 255.)), 255);
          dmxOut2.writeDMXOutput(channelsToDmxAddressArray[idx-1], value);
        }
        idx += 1;
      }
      
      while(millis()-_timer<33){
        //do nothing ...
      }

      if(!runArcElectric && !grosBoutonRougeVal){ // stop reading when button is released
        return;
      }
      
      _timer = millis();
    }
    sdcard_file.close();
  } else {
    //Serial.println(F("failed to read SD card"));
  }
  //Serial.println("finished reading file");
}

void readSequenceFromFile_inline_old(String fileName, float remapValMin, float remapValMax){

  sdcard_file = SD.open(fileName);
  
  int dmxCommand[3];
  
  if (sdcard_file) { 
    char curChar;
    String dmxValue = "";
    
    curChar = char(sdcard_file.read());
    
    int idx;
    while (sdcard_file.available()){
      //Serial.println(F("reading something"));
      idx = 0;
      
      while(idx<nbLights){
        dmxValue = "";
        while(isDigit(curChar)){
          dmxValue += curChar;
          curChar = char(sdcard_file.read());
        }
  
        while(!isDigit(curChar) && curChar!=EOF){
          curChar = char(sdcard_file.read());
        }

        int value = dmxValue.toInt();
        value = min(max(0, map(float(value), remapValMin, remapValMax, 0., 255.)), 255);
        int channel = channelsToDmxAddressArray[idx];
        channel = channel-1; // micmac chelou pour régler probleme de canal 1 qui semble être le 12 sinon... (pourquoi ce décalage???), mais ça semble faire le taffe!
        if(channel==0){
          channel = nbLights;
        }
        dmxOut2.writeDMXOutput(channel, value);
        idx += 1;
      }
      
      while(millis()-_timer<33){
        //do nothing ...
      }

      if(!runArcElectric && !grosBoutonRougeVal){ // stop reading when button is released
        return;
      }
      
      _timer = millis();
    }
    sdcard_file.close();
  } else {
    //Serial.println(F("failed to read SD card"));
  }
  //Serial.println("finished reading file");
}

void readSequenceFromFile_inline(String fileName, float remapValMin, float remapValMax){

  sdcard_file = SD.open(fileName);
  
  int dmxCommand[3];
  
  if (sdcard_file) { 
    char curChar;
    String dmxValue = "";
    
    curChar = char(sdcard_file.read());
    
    unsigned long lineIdx = 0;
    unsigned long start_time = millis();
    
    int idx;
    while (sdcard_file.available()){
      //Serial.println(F("reading something"));
      idx = 1;

      /*if(millis()-start_time>33*lineIdx){
        //Serial.println("too late");
        int ttt = 0; //do nothing
      } else {*/
      if(1){
        while(millis()-start_time<33*lineIdx){
          //do nothing ...
        }
        
        while(idx<=nbLights){
          dmxValue = "";
    
          while(!isDigit(curChar) && curChar!=EOF){
            curChar = char(sdcard_file.read());
          }
          
          while(isDigit(curChar)){
            dmxValue += curChar;
            curChar = char(sdcard_file.read());
          }
    
          while(!isDigit(curChar) && curChar!=EOF){
            curChar = char(sdcard_file.read());
          }
  
          int value = dmxValue.toInt();
          value = min(max(0, map(float(value), remapValMin, remapValMax, 0., 255.)), 255);
          
          int channel = channelsToDmxAddressArray[idx-1];          
          dmxOut2.writeDMXOutput(channel, value);
          idx += 1;
        }
        lineIdx += 1;
        
        /*while(millis()-_timer<33){
          //do nothing ...
        }*/
      }

      if(!runArcElectric && !grosBoutonRougeVal){ // stop reading when button is released
        return;
      }
      
      _timer = millis();
    }
    sdcard_file.close();
  } else {
    //Serial.println(F("failed to read SD card"));
  }
  //Serial.println("finished reading file");
}

int getNumDigits(int x){  
    x = abs(x);  
    return (x < 10 ? 1 :   
        (x < 100 ? 2 :   
        (x < 1000 ? 3 :   
        (x < 10000 ? 4 :   
        (x < 100000 ? 5 :   
        (x < 1000000 ? 6 :   
        (x < 10000000 ? 7 :  
        (x < 100000000 ? 8 :  
        (x < 1000000000 ? 9 :  
        10)))))))));  
}  

void arcElectricFunction(){
  cc.fixedValue(0); // set all lights to black before starting sequence

  if(TEST){
    Serial.println("arc electric");
  } else {
    byte maxFileNb = 14;
    //byte fileNb = random(1,14+1); // peak a random file in the list
    int possibleFilesNum[] = {2,10};
    byte fileNb = possibleFilesNum[random(0,2)]; // peak a random file in the list
    float remapMinVal;
    float remapMaxVal;
    if(fileNb==2){
      remapMinVal = 50.;
      remapMaxVal = 172.;
    } else if(fileNb==10){
      remapMinVal = 80.;
      remapMaxVal = 140.;
    }
  
    // get corresponding file name for dmx sequence :
    byte charCnt = 3-getNumDigits(fileNb);
    String fileName = String(fileNb);
    while(charCnt>0){
      fileName = "0"+fileName;
      charCnt--;
    }
    //fileName = "DMXSEQ/01/"+fileName+".TXT";
    fileName = "DMXSEQ3/01/"+fileName+".TXT";
    //fileName = "DMXSEQ/01/012.TXT";
    
    myDFPlayer.playFolder(4, fileNb);  //Start playing the mp3 file
    //myDFPlayer.play(5);  //Start playing the mp3 file
    //myDFPlayer.playFolder(1, 12);  //Start playing the mp3 file
    
    float speedFactor = 1.8; // dirty workaround for sound and dmx sequence synchro... => TODO : should fine a proper way to solve this problem
    //readSequenceFromFile(fileName, speedFactor);
    readSequenceFromFile_inline(fileName, remapMinVal, remapMaxVal);
  }
  cc.fixedValue(0); // set all lights to black at the end of sequence
  delay(blackOutDelay); // TODO : adjust delay time?
  runArcElectric = false;
}

void grosButonRougeFunction(){
  if(TEST){
    while(grosBoutonRougeVal){
    Serial.println("gros bouton rouge");
    }
  } else {
    cc.fixedValue(0); // set all lights to black before starting sequence
    //int fileNum = random(1,3);
    int possibleFilesNum[] = {2,3,4};
    byte fileNum = possibleFilesNum[random(0,3)]; // peak a random file in the list
    //int fileNum = 4;
    float remapMinVal;
    float remapMaxVal;
    if(fileNum==2){
      remapMinVal = 50.;
      remapMaxVal = 230.;
    } else if(fileNum==3){
      remapMinVal = 80;
      remapMaxVal = 250.;
    } else if(fileNum==4){
      remapMinVal = 100;
      remapMaxVal = 200.;
    }
  
    // get corresponding file name for dmx sequence :
    byte charCnt = 3-getNumDigits(fileNum);
    String fileName = String(fileNum);
    while(charCnt>0){
      fileName = "0"+fileName;
      charCnt--;
    }
    /*if(fileName=="002"){
      fileName = "005";
    }*/
    //fileName = "DMXSEQ/02/"+fileName+".TXT";
    fileName = "DMXSEQ3/02/"+fileName+".TXT";
    //fileName = "DMXSEQ3/02/005.TXT";
    
    float speedFactor = 1.9; // dirty workaround for sound and dmx sequence synchro... => TODO : should fine a proper way to solve this problem
    
    myDFPlayer.playFolder(2, fileNum);
    //myDFPlayer.play(5);  //Start playing the mp3 file
    while(grosBoutonRougeVal){
      //Serial.println("GROS BOUTON ROUGE!!!");
      //dmxOut2.writeDMXOutput(KW_channel, 255);
      //KW_onOff(1);
      //readSequenceFromFile(fileName, speedFactor);
      readSequenceFromFile_inline(fileName, remapMinVal, remapMaxVal);
    }
    myDFPlayer.pause();
    cc.fixedValue(0); // set all lights to black at the end of sequence
    //KW_onOff(0);
  }
}

void KW_onOff(bool KW){
  if(KW){
    dmxOut2.writeDMXOutput(KW_channel, 255);
  } else {
    dmxOut2.writeDMXOutput(KW_channel, 75);
  }
}

void neonFlicker(){ // custom effect with "home-made" sequence
  // set dmx sequence as a list a [time delay, channel, value] triplets
  // use -1 as time delay value which will be replaced by an appropriate value according to effect speed
  
  // chenillard exterieur vers l'intérieur, avec les lampes qui restent allumées jusqu'au bout de la séquence :
  int nbSteps = 60;
  int dmxSequence[nbSteps][3] = {{0,-1,10},
                                 {-1,-1,10},
                                 {-1,-1,20},
                                 {-1,-1,30},
                                 {-1,-1,30},
                                 {-1,-1,30},
                                 {-1,-1,40},
                                 {-1,-1,50},
                                 {-1,-1,60},
                                 {-1,-1,70},
                                 {-1,-1,80},
                                 {-1,-1,70},
                                 {-1,-1,70},
                                 {-1,-1,60},
                                 {-1,-1,60},
                                 {-1,-1,50},
                                 {-1,-1,50},
                                 {-1,-1,50},
                                 {-1,-1,60},
                                 {-1,-1,70},
                                 {-1,-1,80},
                                 {-1,-1,90},
                                 {-1,-1,100},
                                 {-1,-1,120},
                                 {-1,-1,140},
                                 {-1,-1,160},
                                 {-1,-1,240},
                                 {-1,-1,250},
                                 {-1,-1,100},
                                 {-1,-1,150},
                                 {-1,-1,250},
                                 {-1,-1,250},
                                 {-1,-1,140},
                                 {-1,-1,240},
                                 {-1,-1,230},
                                 {-1,-1,220},
                                 {-1,-1,100},
                                 {-1,-1,80},
                                 {-1,-1,70},
                                 {-1,-1,70},
                                 {-1,-1,70},
                                 {-1,-1,80},
                                 {-1,-1,80},
                                 {-1,-1,140},
                                 {-1,-1,130},
                                 {-1,-1,120},
                                 {-1,-1,110},
                                 {-1,-1,200},
                                 {-1,-1,210},
                                 {-1,-1,220},
                                 {-1,-1,220},
                                 {-1,-1,100},
                                 {-1,-1,90},
                                 {-1,-1,40},
                                 {-1,-1,30},
                                 {-1,-1,30},
                                 {-1,-1,30},
                                 {-1,-1,20},
                                 {-1,-1,10},
                                 {-1,-1,10}};

  cc.readSequence(dmxSequence, nbSteps);
}

void effet1Chaser(byte chaseNum){
                  
  bool AR = 1;
  float speedFactor = 1.;
           
  bool chaser0[][nbLights]={{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                            {0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}};
                           
  bool chaser1[][nbLights]={{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                            {0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},
                            {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0},
                            {0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0},
                            {0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0},
                            {0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0},
                            {0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0}};
                           
  bool chaser2[][nbLights]={{1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
                            {0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,0},
                            {0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0},
                            {0,0,0,0,0,0,1,1,0,0,1,1,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0}};
                           
  bool chaser3[][nbLights]={{1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},
                            {0,0,0,1,1,1,0,0,0,0,0,0,1,1,1,0,0,0},
                            {0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0}};
                           
  bool chaser4[][nbLights]={{1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1},
                            {0,0,0,0,1,1,1,1,0,0,1,1,1,1,0,0,0,0},
                            {0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0}};
                           
  bool chaser5[][nbLights]={{1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1},
                            {0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0}};
                           
  bool chaser6[][nbLights]={{1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0},
                            {0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1}};
                           
  bool chaser7[][nbLights]={{1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1},
                            {0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0}};
                           
  bool chaser8[][nbLights]={{1,1,1,0,0,0,1,1,1,0,0,0,1,1,1,0,0,0},
                            {0,0,0,1,1,1,0,0,0,1,1,1,0,0,0,1,1,1}};
                           
  bool chaser9[][nbLights]={{1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1},
                            {0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0}};

  bool chaser10[][nbLights]={{1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0},
                             {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1}};

  bool chaser11[][nbLights]={{1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0},
                             {0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0},
                             {0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1}};

  int chaserPointer;
  int nbSteps;
  int rowSize;
  
  switch(chaseNum){
    case 0 :
      chaserPointer = &chaser0;
      nbSteps = sizeof(chaser0)/sizeof(chaser0[0]);
      rowSize = sizeof(chaser0[0]);
      AR = 0;
      speedFactor = 2.;
      break;
      
    case 1 :
      chaserPointer = &chaser1;
      nbSteps = sizeof(chaser1)/sizeof(chaser1[0]);
      rowSize = sizeof(chaser1[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 2 :
      chaserPointer = &chaser2;
      nbSteps = sizeof(chaser2)/sizeof(chaser2[0]);
      rowSize = sizeof(chaser2[0]); 
      AR = 1;             
      speedFactor = 1.;
      break;
      
    case 3 :    
      chaserPointer = &chaser3;
      nbSteps = sizeof(chaser3)/sizeof(chaser3[0]);
      rowSize = sizeof(chaser3[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 4 :    
      chaserPointer = &chaser4;
      nbSteps = sizeof(chaser4)/sizeof(chaser4[0]);
      rowSize = sizeof(chaser4[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 5 :    
      chaserPointer = &chaser5;
      nbSteps = sizeof(chaser5)/sizeof(chaser5[0]);
      rowSize = sizeof(chaser5[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 6 :    
      chaserPointer = &chaser6;
      nbSteps = sizeof(chaser6)/sizeof(chaser6[0]);
      rowSize = sizeof(chaser6[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 7 :    
      chaserPointer = &chaser7;
      nbSteps = sizeof(chaser7)/sizeof(chaser7[0]);
      rowSize = sizeof(chaser7[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 8 :    
      chaserPointer = &chaser8;
      nbSteps = sizeof(chaser8)/sizeof(chaser8[0]);
      rowSize = sizeof(chaser8[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 9 :    
      chaserPointer = &chaser9;
      nbSteps = sizeof(chaser9)/sizeof(chaser9[0]);
      rowSize = sizeof(chaser9[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 10 :    
      chaserPointer = &chaser10;
      nbSteps = sizeof(chaser10)/sizeof(chaser10[0]);
      rowSize = sizeof(chaser10[0]);
      AR = 1;
      speedFactor = 1.;
      break;
      
    case 11 :    
      chaserPointer = &chaser11;
      nbSteps = sizeof(chaser11)/sizeof(chaser11[0]);
      rowSize = sizeof(chaser11[0]);
      AR = 0;
      speedFactor = 1.;
      break;
  }

  // Build an array of pointers to rows.
  bool *pchaser[nbSteps];
  for (int i = 0; i < nbSteps; i++){
    pchaser[i] = chaserPointer+rowSize*i; //*chaser[i]; 
  }

  cc.anyChaser(pchaser, nbSteps, nbLights, AR, speedFactor);
  
}



void chaserQuiTourne(){

  bool chaserQuiTourneArray[][nbLights]={{1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0},
                                        {0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0},
                                        {0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1}};

  int chaserPointer;
  int nbSteps;
  int rowSize;
  
  
  chaserPointer = &chaserQuiTourneArray;
  nbSteps = sizeof(chaserQuiTourneArray)/sizeof(chaserQuiTourneArray[0]);
  rowSize = sizeof(chaserQuiTourneArray[0]);

  // Build an array of pointers to rows.
  bool *pchaser[nbSteps];
  for (int i = 0; i < nbSteps; i++){
    pchaser[i] = chaserPointer+rowSize*i; //*chaser[i]; 
  }

  bool AR = 0;
  float speedFactor = 1.;
  cc.anyChaser(pchaser, nbSteps, nbLights, AR, speedFactor);
  
}

void chaserQuiBalaye(){

  bool chaserQuiBalayeArray[][nbLights]={{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                                         {0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                                         {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                                         {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                                         {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0},
                                         {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0},
                                         {0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0},
                                         {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},
                                         {0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0},
                                         {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
                                         {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0},
                                         {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0},
                                         {0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0},
                                         {0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
                                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0},
                                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0},
                                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},
                                         {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}};

  int chaserPointer;
  int nbSteps;
  int rowSize;
  
  
  chaserPointer = &chaserQuiBalayeArray;
  nbSteps = sizeof(chaserQuiBalayeArray)/sizeof(chaserQuiBalayeArray[0]);
  rowSize = sizeof(chaserQuiBalayeArray[0]);

  // Build an array of pointers to rows.
  bool *pchaser[nbSteps];
  for (int i = 0; i < nbSteps; i++){
    pchaser[i] = chaserPointer+rowSize*i; //*chaser[i]; 
  }

  bool AR = 1;
  float speedFactor = 1.;
  cc.anyChaser(pchaser, nbSteps, nbLights, AR, speedFactor);
  
}
