
#include "string.h"
using namespace std;
int NumDigits(int x);

void setup() {
  Serial.begin(9600); //Setting baudrate
}
  
void loop() {
  byte fileNb = 10;
  byte charCnt = 4-NumDigits(fileNb);
  String fileName = String(fileNb);
  while(charCnt>0){
    fileName = "0"+fileName;
    charCnt--;
  }
  Serial.println(fileName);
  while(1){};
}

int NumDigits(int x){  
    x = abs(x);  
    return (x < 10 ? 1 :   
        (x < 100 ? 2 :   
        (x < 1000 ? 3 :   
        (x < 10000 ? 4 :   
        (x < 100000 ? 5 :   
        (x < 1000000 ? 6 :   
        (x < 10000000 ? 7 :  
        (x < 100000000 ? 8 :  
        (x < 1000000000 ? 9 :  
        10)))))))));  
}  
