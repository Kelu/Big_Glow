var selection = [];
outlets = 2;

function update(id){
	idx = selection.indexOf(id);
	if(idx >= 0){
		selection.splice(idx, 1);
	}
	else{
		selection.push(id)
	}
	if(selection.length>0){
		outlet(0, selection);
	}
	else{
		outlet(0, '');
	}
	outlet(1, selection.length);
}
