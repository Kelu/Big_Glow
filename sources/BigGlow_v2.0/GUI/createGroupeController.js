
var bpatcherInstancesGroupGUI = [];
var lengthGroupGUI = 550;
var heightGroupGUI = 37;
var xPosInitGroupGUI = 20;
var yPosInitGroupGUI = 100;
var xPosPresInitGroupGUI = 20;
var yPosPresInitGroupGUI = 100;
var xPosGroupGUI = xPosInitGroupGUI;
var yPosGroupGUI = yPosInitGroupGUI;
var xPosPresGroupGUI = xPosPresInitGroupGUI;
var yPosPresGroupGUI = yPosPresInitGroupGUI;
	
function genBpatchers(nbInstances)
{
	arrayLen = bpatcherInstances.length
	
	for(var i=arrayLen+1;i<=nbInstances;i++)
	{	
		if(i!=1 && (i-1)%maxInstPerCol==0)
		{
			xPosPres += 1.2*length
			yPosPres = yPosInit
		}
			
		bpatcherInstances[i-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "groupGUI",
			"@patching_rect", xPos, yPos, length, height,
			"@presentation_rect", xPosPres, yPosPres, length, height,
			"@border", "0",
			//"@varname", varname,
			"@args", i,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
		
		yPos += 40
		yPosPres += 40
	}
}

function createBpatcherInstance()
{
	lightID = bpatcherInstances.length + 1
	
	if(lightID!=1 && (lightID-1)%maxInstPerCol==0)
		{
			xPosPres += 1.2*length
			yPosPres = yPosInit
		}
		
	bpatcherInstances[lightID-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "groupGUI",
			"@patching_rect", xPos, yPos, length, height,
			"@presentation_rect", xPosPres, yPosPres, length, height,
			"@border", "0",
			//"@varname", varname,
			"@args", lightID,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
	yPos += 40
	yPosPres += 40
}

function delBpatchers()
{
	for(var i=1;i<=bpatcherInstances.length;i++)
	{
		this.patcher.remove(bpatcherInstances[i-1])
	}
	xPosInit = 20
	yPosInit = 100
	xPosPresInit = 20
	yPosPresInit = 100
	maxInstPerCol = 12
	xPos = xPosInit
	yPos = yPosInit
	xPosPres = xPosPresInit
	yPosPres = yPosPresInit
	bpatcherInstances = []
}

function getNbIntances()
{
	outlet(0, bpatcherInstances.length);
}

