
var bpatcherInstances = [];
var length = 550;
var height = 37;
var xPosInit = 20;
var yPosInit = 100;
var xPosPresInit = 20;
var yPosPresInit = 100;
var maxInstPerCol = 12;
var xPos = xPosInit;
var yPos = yPosInit;
var xPosPres = xPosPresInit;
var yPosPres = yPosPresInit;
var curCoordX = 0;
var curCoordY = 0;
var curCoordZ = 0;
var fixtureType = "BigGlow";

function setXCoord(coord)
{
	curCoordX = coord
}


function setYCoord(coord)
{
	curCoordY = coord
}

function setFixtureType(type)
{
	fixtureType = type
}

function genBpatchers(nbInstances)
{
	arrayLen = bpatcherInstances.length
	
	for(var i=arrayLen+1;i<=nbInstances;i++)
	{	
		if(i!=1 && (i-1)%maxInstPerCol==0)
		{
			xPosPres += 1.2*length
			yPosPres = yPosInit
		}
		
		if(fixtureType=="BigGlow"){
			bpatcherInstances[i-1] = (
				this.patcher.newdefault(xPos,yPos,
				"bpatcher",
				"@name", "lightGUI",
				"@patching_rect", xPos, yPos, length, height,
				"@presentation_rect", xPosPres, yPosPres, length, height,
				"@border", "0",
				//"@varname", varname,
				"@args", i,
				"@orderfront", "1",
				//"@hint", varname,
				"@embed", "0",
				"@presentation", "1")
			);
		}
		if(fixtureType=="ledBar"){
			bpatcherInstances[i-1] = (
				this.patcher.newdefault(xPos,yPos,
				"bpatcher",
				"@name", "ledBarGUI",
				"@patching_rect", xPos, yPos, length, height,
				"@presentation_rect", xPosPres, yPosPres, length, height,
				"@border", "0",
				//"@varname", varname,
				"@args", i,
				"@orderfront", "1",
				//"@hint", varname,
				"@embed", "0",
				"@presentation", "1")
			);
		}
		
		yPos += 40
		yPosPres += 40
	}
}

function createBpatcherInstance()
{
	lightID = bpatcherInstances.length + 1
	
	if(lightID!=1 && (lightID-1)%maxInstPerCol==0)
		{
			xPosPres += 1.2*length
			yPosPres = yPosInit
		}
		
	bpatcherInstances[lightID-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "lightGUI",
			"@patching_rect", xPos, yPos, length, height,
			"@presentation_rect", xPosPres, yPosPres, length, height,
			"@border", "0",
			//"@varname", varname,
			"@args", lightID, curCoordX, curCoordY, 0.,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
	yPos += 40
	yPosPres += 40
}

function delBpatchers()
{
	for(var i=1;i<=bpatcherInstances.length;i++)
	{
		this.patcher.remove(bpatcherInstances[i-1])
	}
	xPosInit = 20
	yPosInit = 100
	xPosPresInit = 20
	yPosPresInit = 100
	maxInstPerCol = 12
	xPos = xPosInit
	yPos = yPosInit
	xPosPres = xPosPresInit
	yPosPres = yPosPresInit
	bpatcherInstances = []
}

function getNbIntances()
{
	outlet(0, bpatcherInstances.length);
}

