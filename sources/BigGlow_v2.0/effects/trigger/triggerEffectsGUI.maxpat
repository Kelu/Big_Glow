{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 6,
			"architecture" : "x86",
			"modernui" : 1
		}
,
		"rect" : [ 167.0, 200.0, 1049.0, 680.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 408.0, 131.0, 46.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 125.0, 34.0, 46.0, 20.0 ],
					"style" : "",
					"text" : "trigger"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 267.75, 10.0, 72.0, 22.0 ],
					"style" : "",
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 389.0, 85.0, 84.0, 22.0 ],
					"style" : "",
					"text" : "s triggerMode"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 307.0, 63.0, 63.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 54.0, 56.0, 63.0, 20.0 ],
					"style" : "",
					"text" : "manual"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 307.0, 101.0, 63.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 54.0, 90.25, 63.0, 20.0 ],
					"style" : "",
					"text" : "onsets"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 307.0, 85.0, 53.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 54.0, 74.25, 47.0, 20.0 ],
					"style" : "",
					"text" : "metro"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 267.75, 38.0, 86.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 17.75, 34.0, 90.0, 20.0 ],
					"style" : "",
					"text" : "trigger mode :"
				}

			}
, 			{
				"box" : 				{
					"disabled" : [ 0, 0, 0 ],
					"id" : "obj-41",
					"itemtype" : 0,
					"maxclass" : "radiogroup",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 267.75, 67.25, 18.0, 50.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 24.75, 59.25, 18.0, 50.0 ],
					"size" : 3,
					"style" : "",
					"value" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 307.0, 131.0, 93.0, 22.0 ],
					"style" : "",
					"text" : "sw bangTrigger"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 261.0, 125.5, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 125.0, 57.25, 47.0, 47.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "patternTriggerGUI.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 57.0, 351.0, 251.0, 148.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 782.75, 3.0, 224.0, 129.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-9",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "randomTriggerGUI.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 57.0, 68.5, 194.0, 104.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.0, 17.5, 194.0, 97.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "chenillardsGUI.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 50.0, 168.0, 294.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 377.75, 3.0, 262.0, 131.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "strobeGUI.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 50.0, 298.0, 165.0, 124.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 641.75, 18.5, 137.0, 107.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-2",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 39.0, 13.0, 212.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 37.0, 7.0, 143.0, 24.0 ],
					"style" : "",
					"text" : "Trigger Effects"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.286275, 0.819608, 0.603922, 1.0 ],
					"id" : "obj-3",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 7.0, 3.0, 359.0, 560.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 3.0, 1005.0, 138.0 ],
					"proportion" : 0.39,
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "strobeGUI.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/effects/trigger/strobe",
				"patcherrelativepath" : "./strobe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "sw.maxpat",
				"bootpath" : "/Users/Shared/Max 7/Examples/max-tricks/send-receive-tricks/lib",
				"patcherrelativepath" : "../../../../../../../Shared/Max 7/Examples/max-tricks/send-receive-tricks/lib",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "strobeControl.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/effects/trigger/strobe",
				"patcherrelativepath" : "./strobe",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "routeOnOffControl.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/control",
				"patcherrelativepath" : "../../control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "chenillardsGUI.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/effects/trigger/chenillards",
				"patcherrelativepath" : "./chenillards",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "chaserManager.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/effects/trigger/chenillards",
				"patcherrelativepath" : "./chenillards",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "var.maxpat",
				"bootpath" : "/Users/Shared/Max 7/Examples/max-tricks/send-receive-tricks/lib",
				"patcherrelativepath" : "../../../../../../../Shared/Max 7/Examples/max-tricks/send-receive-tricks/lib",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "genNumberBox.js",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0",
				"patcherrelativepath" : "../..",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "chenillardsControl.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/effects/trigger/chenillards",
				"patcherrelativepath" : "./chenillards",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "routeScaleParam.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/control",
				"patcherrelativepath" : "../../control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "randomTriggerGUI.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/effects/trigger/randomTrigger",
				"patcherrelativepath" : "./randomTrigger",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "randomTriggerControl.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/effects/trigger/randomTrigger",
				"patcherrelativepath" : "./randomTrigger",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "patternTriggerGUI.maxpat",
				"bootpath" : "~/Desktop/Big_Glow/sources/BigGlow_v2.0/effects/trigger/patternTriggers",
				"patcherrelativepath" : "./patternTriggers",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-2",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBrown-1",
				"default" : 				{
					"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjCyan-1",
				"default" : 				{
					"accentcolor" : [ 0.022882, 0.848654, 0.85423, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-2",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjMagenta-1",
				"default" : 				{
					"accentcolor" : [ 0.840663, 0.060168, 0.769195, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjRed-1",
				"default" : 				{
					"accentcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"fontsize" : [ 12.059008 ],
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-2",
				"default" : 				{
					"fontsize" : [ 12.059008 ],
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-2",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "texteditGold",
				"default" : 				{
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 0.68 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
