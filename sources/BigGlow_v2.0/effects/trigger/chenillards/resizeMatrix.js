var leftPos = 118;
var topPos = 300;
var colSize = 20;
var rowSize = 20;

function resizeMatrix(nbRows, nbCols)
{
	var matrixObj = this.patcher.getnamed("chaserCtrlMatrix");
	var rightPos = leftPos + colSize*nbCols;
	var bottomPos = topPos + rowSize*nbRows;
	matrixObj.rect = [leftPos, topPos, rightPos, bottomPos];
	matrixObj.message("presentation_rect", leftPos, topPos, rightPos, bottomPos);
	post(matrixObj.rect);
	post("\n");
	post(matrixObj.getattr("presentation_rect"));
	post("\n");
}