var lightsPos = [];
var lightsStates = [];
var previousClosestLight = -1;
var nbClosestChoice = 2;
var maxRandNb = 1;
var randInd = 0;
outlets = 3;
var targetLight = -1;
	
function addLight(lightID, XPos, YPos)
{
	lightsPos[lightID-1] = [XPos, YPos];
	lightsStates[lightID-1] = 0;
}

function updateLightState(lightID, lightVal){
	lightsStates[lightID-1] = (lightVal > 0);
	if(Math.min.apply(Math, lightsStates) > 0)
	{
		outlet(2,"stop");
	}
}

function setPreviousLight(prevLight)
{
	previousClosestLight = prevLight;
}

function delAllLights()
{
	lightsPos = [];
	lightsStates = [];
}

function printLightsPos()
{
	nbLights = lightsPos.length;
	for(var i=0;i<nbLights;i++)
	{
		post(lightsPos[i]);
		post("\n");
	}
}

function getLightCoords(lightID)
{
	outlet(1, lightsPos[lightID-1]);
}

function setMaxRandNb(maxNb){
	maxRandNb = maxNb;
	}

function getNewRandTarget(){
	randInd = Math.floor(Math.random()*maxRandNb);
	}

function printRandInd(){
	post("randInd = ");
	post(randInd);
	post("\n");
	}

function getClosestLight(XPos, YPos)
{	
	if(targetLight==-1 || lightsStates[targetLight]){
		distList = [];
		nbLights = lightsPos.length;
		for(var i=0;i<nbLights;i++)
		{	
			if(lightsStates[i] || (i==(previousClosestLight-1))){
				distList[i] = 100000;
			}
			else{
				XPos2 = lightsPos[i][0];
				YPos2 = lightsPos[i][1];
				dist = Math.sqrt(Math.pow((XPos - XPos2),2) + Math.pow((YPos - YPos2),2));
				distList[i] = dist;
			}
		}
		getNewRandTarget();
		orderedDistList = distList.slice(0); // copy to not modify original array
		orderedDistList = orderedDistList.sort(function(a, b){return a-b}); // order distance array
		//closestLight = distList.indexOf(Math.min.apply(Math, distList));
		targetLight = distList.indexOf(orderedDistList[randInd]);
		//post("previousClosestLight = ");
		//post(previousClosestLight);
		//post("targetLight = ");
		//post(targetLight);
		//post("\n");
	}
	outlet(0, targetLight);
}
