var xDim = 5.;
var yDim = 5.;
var started = false;
var xPos = 0;
var yPos = 0;
var speed = 1.;

function setDims(xFieldLen, yFieldLen)
{
	xDim = xFieldLen;
	yDim = yFieldLen;
}

function setInitPos()
{
	xPos = Math.random()*xDim
	yPos = Math.random()*yDim
	outlet(0, [xPos, yPos])
}

function setSpeed(newSpeed)
{
	speed = newSpeed
}

function move(xTarget, yTarget)
{
	distX = xTarget - xPos
	//post(distX)
	distY = yTarget - yPos
	//post(distY)
	maxVal = Math.max(Math.abs(distX), Math.abs(distY))
	//post(maxVal)
	xIncr = distX * speed / maxVal
	yIncr = distY * speed / maxVal
	xPos = xPos + xIncr
	yPos = yPos + yIncr
	outlet(0, [xPos, yPos])
}


