
var lightsPos = [];
outlets = 2;
	
function addLight(lightID, XPos, YPos)
{
	lightsPos[lightID-1] = [XPos, YPos]
}

function delAllLights()
{
	lightsPos = []
}

function printLightsPos()
{
	nbLights = lightsPos.length
	for(var i=0;i<nbLights;i++)
	{
		post(lightsPos[i])
		post("\n")
	}
}

function getClosestLight(XPos, YPos)
{
	distList = []
	nbLights = lightsPos.length
	for(var i=0;i<nbLights;i++)
	{
		XPos2 = lightsPos[i][0]
		YPos2 = lightsPos[i][1]
		dist = Math.sqrt(Math.pow((XPos - XPos2),2) + Math.pow((YPos - YPos2),2))
		distList[i] = dist
	}
	closestLight = distList.indexOf(Math.min.apply(Math, distList))
	/*post("closest light : ")
	post(closestLight)
	post("\n")*/
	outlet(0, closestLight);
}

function dumpPos()
{
	nbLights = lightsPos.length
	for(var i=0;i<nbLights;i++)
	{
		outList = [i, lightsPos[i][0], lightsPos[i][1]]
		post(outList)
		outlet(1, outList);
	}
}