
var numberBoxInstances = [];
var xPosInit = 3;
var yPosInit = 90;
var xPosPresInit = 3;
var yPosPresInit = 90;
var xPos = xPosInit;
var yPos = yPosInit;
var xPosPres = xPosPresInit;
var yPosPres = yPosPresInit;
length = 10;
height = 10;
	
function genNumberBoxes(nbInstances)
{
	yPos = yPosInit;
	yPosPres = yPosPresInit;
	
	arrayLen = numberBoxInstances.length
	
	for(var i=arrayLen+1;i<nbInstances+1;i++)
	{	
		var iStr =  new String(i);
		numberBoxInstances[i] = (this.patcher.newdefault(xPos, yPos, "comment",
														"@text", "."+iStr+".",
														"@presentation", "1",
														"@patching_rect", xPos, yPos, length, height,
														"@presentation_rect", xPosPres, yPosPres, length, height));
		yPos += 20
		yPosPres += 20
	}
}

function getNbIntances()
{
	outlet(0, numberBoxInstances.length);
}

function clear()
{
	for(var i=1;i<=numberBoxInstances.length;i++)
	{
		this.patcher.remove(numberBoxInstances[i-1])
	}
	numberBoxInstances = [];
	yPos = yPosInit;
}
