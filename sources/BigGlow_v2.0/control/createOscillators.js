
inlets = 1;
var oscillators = [];
var length = 174;
var height = 169;
var xPosInit = 0;
var yPos = 41;
var nbInstances = 4;

function genOscillators()
{
	for(var i=1;i<=nbInstances;i++)
	{		
		xPos = xPosInit + (i-1)*length;
		oscillators[i-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "oscillatorCtrl",
			"@patching_rect", xPos, yPos, length, height,
			"@presentation_rect", xPos, yPos, length, height,
			"@border", "0",
			//"@varname", varname,
			"@args", i,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
			);
	}
}
