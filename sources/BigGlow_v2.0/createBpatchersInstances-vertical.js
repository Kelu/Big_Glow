
var bpatcherInstances = [];
var width = 65;
var height = 350;
var xPosInit = 30;
var yPosInit = 300;
var xPosPresInit = 30;
var yPosPresInit = 100;
var xPos = xPosInit;
var yPos = yPosInit;
var xPosPres = xPosPresInit;
var yPosPres = yPosPresInit;
	
function genBpatchers(nbInstances)
{
	arrayLen = bpatcherInstances.length
	
	for(var i=arrayLen+1;i<=nbInstances;i++)
	{		
		bpatcherInstances[i-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "lightGUI-vertical",
			"@patching_rect", xPos, yPos, width, height,
			"@presentation_rect", xPosPres, yPosPres, width, height,
			"@border", "0",
			//"@varname", varname,
			"@args", i,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
		
		xPos += width
		xPosPres += width
	}
}

function createBpatcherInstance()
{
	lightID = bpatcherInstances.length + 1
		
	bpatcherInstances[lightID-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "lightGUI",
			"@patching_rect", xPos, yPos, width, height,
			"@presentation_rect", xPosPres, yPosPres, width, height,
			"@border", "0",
			//"@varname", varname,
			"@args", lightID,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
	xPos += width
	xPosPres += width
}

function delBpatchers()
{
	for(var i=1;i<=bpatcherInstances.length;i++)
	{
		this.patcher.remove(bpatcherInstances[i-1])
	}
	xPosInit = 30
	yPosInit = 300
	xPosPresInit = 30
	yPosPresInit = 100
	xPos = xPosInit
	yPos = yPosInit
	xPosPres = xPosPresInit
	yPosPres = yPosPresInit
	bpatcherInstances = []
}

function getNbIntances()
{
	outlet(0, bpatcherInstances.length);
}

