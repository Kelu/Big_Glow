var groupGUIInstances = [];
var length = 90;
var height = 350;
var xPosInit = 15;
var yPosInit = 220;
var xPosPresInit = 15;
var yPosPresInit = 100;
var maxInstPerRow = 50;
var xPos = xPosInit;
var yPos = yPosInit;
var xPosPres = xPosPresInit;
var yPosPres = yPosPresInit;

function genGroupGUIs(nbInstances)
{
	arrayLen = groupGUIInstances
	
	for(var i=arrayLen+1;i<=nbInstances;i++)
	{	
		if(i!=1 && (i-1)%maxInstPerRow==0)
		{
			xPosPres = xPosInit
			yPosPres += 1.2*height
		}
			
		groupGUIInstances[i-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "groupGUI",
			"@patching_rect", xPos, yPos, length, height,
			"@presentation_rect", xPosPres, yPosPres, length, height,
			"@border", "0",
			//"@varname", varname,
			"@args", i,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
		
		xPos += 100
		xPosPres += 100
	}
}

function addNewGroup()
{
	groupID = groupGUIInstances.length + 1
	
	if(groupID!=1 && (groupID-1)%maxInstPerRow==0)
		{
			xPosPres = xPosInit
			yPosPres += 1.2*height
		}
		
	groupGUIInstances[groupID-1] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "groupGUI",
			"@patching_rect", xPos, yPos, length, height,
			"@presentation_rect", xPosPres, yPosPres, length, height,
			"@border", "0",
			//"@varname", varname,
			"@args", groupID,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
		
	xPos += 100
	xPosPres += 100
}

function delAllGroups()
{
	for(var i=1;i<=groupGUIInstances.length;i++)
	{
		this.patcher.remove(groupGUIInstances[i-1])
	}
	xPosInit = 15
	yPosInit = 220
	xPosPresInit = 15
	yPosPresInit = 100
	xPos = xPosInit
	yPos = yPosInit
	xPosPres = xPosPresInit
	yPosPres = yPosPresInit
	groupGUIInstances = []
}

function delGroupByID(groupID)
{
	this.patcher.remove(groupGUIInstances[groupID])
	//TODO : should move the position of following groups to fille the gap due to the deleted group
}

function delLastGroup()
{
	this.patcher.remove(groupGUIInstances[groupGUIInstances.length-1])
	groupGUIInstances.pop()
	
	xPos -= 100
	xPosPres -= 100
}

function getNbGroups()
{
	outlet(0, groupGUIInstances.length);
}

