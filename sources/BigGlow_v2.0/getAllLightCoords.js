
function genAllCoordsList(xCoord, yCoord, lightDiameter)
{
	halfDiameter = ceil(lightDiameter)
	xStart = xCoord - halfDiameter
	xStop = xCoord + halfDiameter
	yStart = yCoord - halfDiameter
	yStop = yCoord + halfDiameter
	
	dist = 0
	i = 0
	
	lightCoords = []
	
	for(var xPos=xStart;xPos<=xStop;xPos++)
	{
		for(var yPos=yStart;yPos<=yStop;yPos++)
		{
			xDist = xCoord - xPos
			yDist = yCoord - yPos
			dist = sqrt(xDist**2 + yDist**2)
			if(dist <= halfDiameter){
				lightCoords[i] = [xPos, yPos]
				i += 1
			}
		}
	}
	
	return lightCoords
}

