{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 285.0, 108.0, 847.0, 686.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 683.0, 90.0, 77.0, 22.0 ],
					"style" : "",
					"text" : "sw dmxData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 582.0, 90.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "sw dmxOut"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 297.0, 15.0, 98.0, 22.0 ],
					"style" : "",
					"text" : "r DMXRecOnOff"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 198.0, 15.0, 84.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 105.0, 20.0, 84.0, 20.0 ],
					"style" : "",
					"text" : "DMX recorder"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 39.0, 310.0, 31.0, 22.0 ],
					"style" : "",
					"text" : "thru"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 179.0, 42.0, 35.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 20.0, 53.0, 35.0, 22.0 ],
					"style" : "",
					"text" : "read"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 261.0, 641.0, 55.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 153.0, 100.0, 55.0, 20.0 ],
					"style" : "",
					"text" : "finished"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 39.0, 139.0, 104.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 81.0, 133.0, 104.0, 20.0 ],
					"style" : "",
					"text" : "playback speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 39.0, 45.0, 75.0, 22.0 ],
					"style" : "",
					"text" : "loadmess 1."
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-29",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 39.0, 75.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 20.0, 131.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 133.0, 42.0, 33.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 20.0, 98.0, 33.0, 22.0 ],
					"style" : "",
					"text" : "play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 581.0, 419.0, 157.0, 20.0 ],
					"style" : "",
					"text" : "set all channels to 0 at start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 421.0, 359.0, 150.0, 33.0 ],
					"style" : "",
					"text" : "get time duration after previous event"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 228.5, 639.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 127.0, 100.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "float" ],
					"patching_rect" : [ 375.5, 342.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "t f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 381.5, 380.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "-"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 218.0, 249.0, 44.0, 22.0 ],
					"style" : "",
					"text" : "gate 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "float" ],
					"patching_rect" : [ 349.5, 173.0, 40.0, 22.0 ],
					"style" : "",
					"text" : "t i i 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 133.0, 104.0, 33.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 65.0, 98.0, 33.0, 22.0 ],
					"style" : "",
					"text" : "stop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 243.0, 194.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "r dmxData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 218.0, 350.0, 106.0, 22.0 ],
					"style" : "",
					"text" : "prepend dmxData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 39.0, 110.0, 61.0, 22.0 ],
					"style" : "",
					"text" : "tempo $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 133.0, 69.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 375.5, 263.0, 34.0, 22.0 ],
					"style" : "",
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 467.5, 419.0, 108.0, 22.0 ],
					"style" : "",
					"text" : "set dmxData 0 0 \\;"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 218.0, 529.0, 57.0, 22.0 ],
					"style" : "",
					"text" : "append \\;"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 179.0, 80.0, 36.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 65.0, 53.0, 36.0, 22.0 ],
					"style" : "",
					"text" : "write"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "bang", "bang" ],
					"patching_rect" : [ 218.0, 594.0, 40.0, 22.0 ],
					"save" : [ "#N", "qlist", ";", "#X", "insert", "dmxData", 0, 0, ";", ";", "#X", "insert", 1882, "dmxData", 36, 102, ";", ";", "#X", "insert", 0, "dmxData", 35, 102, ";", ";", "#X", "insert", 0, "dmxData", 34, 102, ";", ";", "#X", "insert", 0, "dmxData", 33, 102, ";", ";", "#X", "insert", 2, "dmxData", 32, 102, ";", ";", "#X", "insert", 0, "dmxData", 31, 102, ";", ";", "#X", "insert", 0, "dmxData", 30, 102, ";", ";", "#X", "insert", 0, "dmxData", 29, 102, ";", ";", "#X", "insert", 0, "dmxData", 28, 102, ";", ";", "#X", "insert", 0, "dmxData", 27, 102, ";", ";", "#X", "insert", 0, "dmxData", 26, 102, ";", ";", "#X", "insert", 0, "dmxData", 25, 102, ";", ";", "#X", "insert", 0, "dmxData", 24, 102, ";", ";", "#X", "insert", 0, "dmxData", 23, 102, ";", ";", "#X", "insert", 0, "dmxData", 22, 102, ";", ";", "#X", "insert", 2, "dmxData", 21, 102, ";", ";", "#X", "insert", 0, "dmxData", 20, 102, ";", ";", "#X", "insert", 0, "dmxData", 19, 102, ";", ";", "#X", "insert", 0, "dmxData", 18, 102, ";", ";", "#X", "insert", 0, "dmxData", 17, 102, ";", ";", "#X", "insert", 0, "dmxData", 16, 102, ";", ";", "#X", "insert", 0, "dmxData", 15, 102, ";", ";", "#X", "insert", 0, "dmxData", 14, 102, ";", ";", "#X", "insert", 0, "dmxData", 13, 102, ";", ";", "#X", "insert", 0, "dmxData", 12, 102, ";", ";", "#X", "insert", 0, "dmxData", 11, 102, ";", ";", "#X", "insert", 0, "dmxData", 10, 102, ";", ";", "#X", "insert", 2, "dmxData", 9, 102, ";", ";", "#X", "insert", 0, "dmxData", 8, 102, ";", ";", "#X", "insert", 0, "dmxData", 7, 102, ";", ";", "#X", "insert", 0, "dmxData", 6, 102, ";", ";", "#X", "insert", 0, "dmxData", 5, 102, ";", ";", "#X", "insert", 0, "dmxData", 4, 102, ";", ";", "#X", "insert", 0, "dmxData", 3, 102, ";", ";", "#X", "insert", 0, "dmxData", 2, 102, ";", ";", "#X", "insert", 0, "dmxData", 1, 102, ";", ";", "#X", "insert", 125, "dmxData", 36, 112, ";", ";", "#X", "insert", 0, "dmxData", 35, 112, ";", ";", "#X", "insert", 0, "dmxData", 34, 112, ";", ";", "#X", "insert", 0, "dmxData", 33, 112, ";", ";", "#X", "insert", 0, "dmxData", 32, 112, ";", ";", "#X", "insert", 0, "dmxData", 31, 112, ";", ";", "#X", "insert", 0, "dmxData", 30, 112, ";", ";", "#X", "insert", 0, "dmxData", 29, 112, ";", ";", "#X", "insert", 0, "dmxData", 28, 112, ";", ";", "#X", "insert", 0, "dmxData", 27, 112, ";", ";", "#X", "insert", 0, "dmxData", 26, 112, ";", ";", "#X", "insert", 0, "dmxData", 25, 112, ";", ";", "#X", "insert", 2, "dmxData", 24, 112, ";", ";", "#X", "insert", 0, "dmxData", 23, 112, ";", ";", "#X", "insert", 0, "dmxData", 22, 112, ";", ";", "#X", "insert", 0, "dmxData", 21, 112, ";", ";", "#X", "insert", 0, "dmxData", 20, 112, ";", ";", "#X", "insert", 0, "dmxData", 19, 112, ";", ";", "#X", "insert", 0, "dmxData", 18, 112, ";", ";", "#X", "insert", 0, "dmxData", 17, 112, ";", ";", "#X", "insert", 0, "dmxData", 16, 112, ";", ";", "#X", "insert", 0, "dmxData", 15, 112, ";", ";", "#X", "insert", 0, "dmxData", 14, 112, ";", ";", "#X", "insert", 0, "dmxData", 13, 112, ";", ";", "#X", "insert", 2, "dmxData", 12, 112, ";", ";", "#X", "insert", 0, "dmxData", 11, 112, ";", ";", "#X", "insert", 0, "dmxData", 10, 112, ";", ";", "#X", "insert", 0, "dmxData", 9, 112, ";", ";", "#X", "insert", 0, "dmxData", 8, 112, ";", ";", "#X", "insert", 0, "dmxData", 7, 112, ";", ";", "#X", "insert", 0, "dmxData", 6, 112, ";", ";", "#X", "insert", 0, "dmxData", 5, 112, ";", ";", "#X", "insert", 0, "dmxData", 4, 112, ";", ";", "#X", "insert", 0, "dmxData", 3, 112, ";", ";", "#X", "insert", 0, "dmxData", 2, 112, ";", ";", "#X", "insert", 0, "dmxData", 1, 112, ";", ";", "#X", "insert", 16, "dmxData", 36, 128, ";", ";", "#X", "insert", 0, "dmxData", 35, 128, ";", ";", "#X", "insert", 0, "dmxData", 34, 128, ";", ";", "#X", "insert", 0, "dmxData", 33, 128, ";", ";", "#X", "insert", 0, "dmxData", 32, 128, ";", ";", "#X", "insert", 0, "dmxData", 31, 128, ";", ";", "#X", "insert", 0, "dmxData", 30, 128, ";", ";", "#X", "insert", 0, "dmxData", 29, 128, ";", ";", "#X", "insert", 0, "dmxData", 28, 128, ";", ";", "#X", "insert", 0, "dmxData", 27, 128, ";", ";", "#X", "insert", 0, "dmxData", 26, 128, ";", ";", "#X", "insert", 2, "dmxData", 25, 128, ";", ";", "#X", "insert", 0, "dmxData", 24, 128, ";", ";", "#X", "insert", 0, "dmxData", 23, 128, ";", ";", "#X", "insert", 0, "dmxData", 22, 128, ";", ";", "#X", "insert", 0, "dmxData", 21, 128, ";", ";", "#X", "insert", 0, "dmxData", 20, 128, ";", ";", "#X", "insert", 0, "dmxData", 19, 128, ";", ";", "#X", "insert", 0, "dmxData", 18, 128, ";", ";", "#X", "insert", 0, "dmxData", 17, 128, ";", ";", "#X", "insert", 0, "dmxData", 16, 128, ";", ";", "#X", "insert", 0, "dmxData", 15, 128, ";", ";", "#X", "insert", 0, "dmxData", 14, 128, ";", ";", "#X", "insert", 2, "dmxData", 13, 128, ";", ";", "#X", "insert", 0, "dmxData", 12, 128, ";", ";", "#X", "insert", 0, "dmxData", 11, 128, ";", ";", "#X", "insert", 0, "dmxData", 10, 128, ";", ";", "#X", "insert", 0, "dmxData", 9, 128, ";", ";", "#X", "insert", 0, "dmxData", 8, 128, ";", ";", "#X", "insert", 0, "dmxData", 7, 128, ";", ";", "#X", "insert", 0, "dmxData", 6, 128, ";", ";", "#X", "insert", 0, "dmxData", 5, 128, ";", ";", "#X", "insert", 0, "dmxData", 4, 128, ";", ";", "#X", "insert", 0, "dmxData", 3, 128, ";", ";", "#X", "insert", 0, "dmxData", 2, 128, ";", ";", "#X", "insert", 0, "dmxData", 1, 131, ";", ";", "#X", "insert", 77, "dmxData", 36, 254, ";", ";", "#X", "insert", 0, "dmxData", 35, 254, ";", ";", "#X", "insert", 0, "dmxData", 34, 254, ";", ";", "#X", "insert", 0, "dmxData", 33, 254, ";", ";", "#X", "insert", 0, "dmxData", 32, 254, ";", ";", "#X", "insert", 0, "dmxData", 31, 254, ";", ";", "#X", "insert", 0, "dmxData", 30, 254, ";", ";", "#X", "insert", 0, "dmxData", 29, 254, ";", ";", "#X", "insert", 0, "dmxData", 28, 254, ";", ";", "#X", "insert", 0, "dmxData", 27, 254, ";", ";", "#X", "insert", 0, "dmxData", 26, 254, ";", ";", "#X", "insert", 2, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 24, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 1, "dmxData", 15, 254, ";", ";", "#X", "insert", 0, "dmxData", 14, 254, ";", ";", "#X", "insert", 0, "dmxData", 13, 254, ";", ";", "#X", "insert", 0, "dmxData", 12, 254, ";", ";", "#X", "insert", 0, "dmxData", 11, 254, ";", ";", "#X", "insert", 0, "dmxData", 10, 254, ";", ";", "#X", "insert", 0, "dmxData", 9, 254, ";", ";", "#X", "insert", 8, "dmxData", 8, 254, ";", ";", "#X", "insert", 0, "dmxData", 7, 254, ";", ";", "#X", "insert", 0, "dmxData", 6, 254, ";", ";", "#X", "insert", 0, "dmxData", 5, 254, ";", ";", "#X", "insert", 0, "dmxData", 4, 254, ";", ";", "#X", "insert", 0, "dmxData", 3, 254, ";", ";", "#X", "insert", 0, "dmxData", 2, 254, ";", ";", "#X", "insert", 0, "dmxData", 1, 254, ";", ";", "#X", "insert", 9, "dmxData", 36, 254, ";", ";", "#X", "insert", 0, "dmxData", 35, 254, ";", ";", "#X", "insert", 0, "dmxData", 34, 254, ";", ";", "#X", "insert", 0, "dmxData", 33, 254, ";", ";", "#X", "insert", 0, "dmxData", 32, 254, ";", ";", "#X", "insert", 0, "dmxData", 31, 254, ";", ";", "#X", "insert", 0, "dmxData", 30, 254, ";", ";", "#X", "insert", 0, "dmxData", 29, 254, ";", ";", "#X", "insert", 0, "dmxData", 28, 254, ";", ";", "#X", "insert", 0, "dmxData", 27, 254, ";", ";", "#X", "insert", 0, "dmxData", 26, 254, ";", ";", "#X", "insert", 2, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 24, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 1, "dmxData", 15, 254, ";", ";", "#X", "insert", 0, "dmxData", 14, 254, ";", ";", "#X", "insert", 0, "dmxData", 13, 254, ";", ";", "#X", "insert", 0, "dmxData", 12, 254, ";", ";", "#X", "insert", 0, "dmxData", 11, 254, ";", ";", "#X", "insert", 0, "dmxData", 10, 254, ";", ";", "#X", "insert", 0, "dmxData", 9, 254, ";", ";", "#X", "insert", 8, "dmxData", 8, 254, ";", ";", "#X", "insert", 0, "dmxData", 7, 254, ";", ";", "#X", "insert", 0, "dmxData", 6, 254, ";", ";", "#X", "insert", 0, "dmxData", 5, 254, ";", ";", "#X", "insert", 0, "dmxData", 4, 254, ";", ";", "#X", "insert", 0, "dmxData", 3, 254, ";", ";", "#X", "insert", 0, "dmxData", 2, 254, ";", ";", "#X", "insert", 0, "dmxData", 1, 254, ";", ";", "#X", "insert", 64, "dmxData", 36, 254, ";", ";", "#X", "insert", 0, "dmxData", 35, 254, ";", ";", "#X", "insert", 0, "dmxData", 34, 254, ";", ";", "#X", "insert", 0, "dmxData", 33, 254, ";", ";", "#X", "insert", 0, "dmxData", 32, 254, ";", ";", "#X", "insert", 0, "dmxData", 31, 254, ";", ";", "#X", "insert", 0, "dmxData", 30, 254, ";", ";", "#X", "insert", 0, "dmxData", 29, 254, ";", ";", "#X", "insert", 0, "dmxData", 28, 254, ";", ";", "#X", "insert", 0, "dmxData", 27, 254, ";", ";", "#X", "insert", 2, "dmxData", 26, 254, ";", ";", "#X", "insert", 0, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 24, 254, ";", ";", "#X", "insert", 9, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 32, "dmxData", 36, 254, ";", ";", "#X", "insert", 0, "dmxData", 35, 254, ";", ";", "#X", "insert", 0, "dmxData", 34, 254, ";", ";", "#X", "insert", 0, "dmxData", 33, 254, ";", ";", "#X", "insert", 0, "dmxData", 32, 254, ";", ";", "#X", "insert", 0, "dmxData", 31, 254, ";", ";", "#X", "insert", 2, "dmxData", 30, 254, ";", ";", "#X", "insert", 0, "dmxData", 29, 254, ";", ";", "#X", "insert", 0, "dmxData", 28, 254, ";", ";", "#X", "insert", 0, "dmxData", 27, 254, ";", ";", "#X", "insert", 0, "dmxData", 26, 254, ";", ";", "#X", "insert", 0, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 24, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 2, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 4, "dmxData", 15, 254, ";", ";", "#X", "insert", 0, "dmxData", 14, 254, ";", ";", "#X", "insert", 0, "dmxData", 13, 254, ";", ";", "#X", "insert", 0, "dmxData", 12, 254, ";", ";", "#X", "insert", 0, "dmxData", 11, 254, ";", ";", "#X", "insert", 0, "dmxData", 10, 254, ";", ";", "#X", "insert", 0, "dmxData", 9, 254, ";", ";", "#X", "insert", 0, "dmxData", 8, 254, ";", ";", "#X", "insert", 0, "dmxData", 7, 254, ";", ";", "#X", "insert", 0, "dmxData", 6, 254, ";", ";", "#X", "insert", 0, "dmxData", 5, 254, ";", ";", "#X", "insert", 0, "dmxData", 4, 254, ";", ";", "#X", "insert", 0, "dmxData", 3, 254, ";", ";", "#X", "insert", 0, "dmxData", 2, 254, ";", ";", "#X", "insert", 0, "dmxData", 1, 254, ";", ";", "#X", "insert", 41, "dmxData", 15, 251, ";", ";", "#X", "insert", 0, "dmxData", 14, 251, ";", ";", "#X", "insert", 0, "dmxData", 13, 251, ";", ";", "#X", "insert", 0, "dmxData", 12, 251, ";", ";", "#X", "insert", 0, "dmxData", 11, 251, ";", ";", "#X", "insert", 0, "dmxData", 10, 251, ";", ";", "#X", "insert", 0, "dmxData", 9, 251, ";", ";", "#X", "insert", 0, "dmxData", 8, 251, ";", ";", "#X", "insert", 0, "dmxData", 7, 251, ";", ";", "#X", "insert", 0, "dmxData", 6, 251, ";", ";", "#X", "insert", 0, "dmxData", 5, 251, ";", ";", "#X", "insert", 0, "dmxData", 4, 251, ";", ";", "#X", "insert", 0, "dmxData", 3, 251, ";", ";", "#X", "insert", 0, "dmxData", 2, 251, ";", ";", "#X", "insert", 0, "dmxData", 1, 251, ";", ";", "#X", "insert", 30, "dmxData", 15, 238, ";", ";", "#X", "insert", 0, "dmxData", 14, 238, ";", ";", "#X", "insert", 0, "dmxData", 13, 238, ";", ";", "#X", "insert", 0, "dmxData", 12, 238, ";", ";", "#X", "insert", 0, "dmxData", 11, 238, ";", ";", "#X", "insert", 0, "dmxData", 10, 238, ";", ";", "#X", "insert", 0, "dmxData", 9, 238, ";", ";", "#X", "insert", 0, "dmxData", 8, 238, ";", ";", "#X", "insert", 0, "dmxData", 7, 238, ";", ";", "#X", "insert", 0, "dmxData", 6, 238, ";", ";", "#X", "insert", 0, "dmxData", 5, 238, ";", ";", "#X", "insert", 0, "dmxData", 4, 238, ";", ";", "#X", "insert", 0, "dmxData", 3, 238, ";", ";", "#X", "insert", 0, "dmxData", 2, 238, ";", ";", "#X", "insert", 0, "dmxData", 1, 238, ";", ";", "#X", "insert", 80, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 9, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 1, "dmxData", 15, 0, ";", ";", "#X", "insert", 58, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 5, "dmxData", 14, 0, ";", ";", "#X", "insert", 0, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 2, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 56, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 2, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 0, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 2, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 3, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 13, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 2, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 0, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 2, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 3, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 13, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 44, "dmxData", 20, 0, ";", ";", "#X", "insert", 3, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 0, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 3, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 3, "dmxData", 1, 0, ";", ";", "#X", "insert", 0, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 1, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 50, "dmxData", 20, 0, ";", ";", "#X", "insert", 0, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 0, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 2, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 0, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 42, "dmxData", 23, 2, ";", ";", "#X", "insert", 0, "dmxData", 22, 2, ";", ";", "#X", "insert", 0, "dmxData", 21, 2, ";", ";", "#X", "insert", 12, "dmxData", 36, 95, ";", ";", "#X", "insert", 0, "dmxData", 35, 95, ";", ";", "#X", "insert", 0, "dmxData", 34, 95, ";", ";", "#X", "insert", 0, "dmxData", 33, 95, ";", ";", "#X", "insert", 0, "dmxData", 32, 95, ";", ";", "#X", "insert", 0, "dmxData", 31, 95, ";", ";", "#X", "insert", 0, "dmxData", 30, 95, ";", ";", "#X", "insert", 0, "dmxData", 20, 95, ";", ";", "#X", "insert", 0, "dmxData", 14, 95, ";", ";", "#X", "insert", 0, "dmxData", 13, 95, ";", ";", "#X", "insert", 0, "dmxData", 12, 95, ";", ";", "#X", "insert", 0, "dmxData", 11, 95, ";", ";", "#X", "insert", 0, "dmxData", 10, 95, ";", ";", "#X", "insert", 0, "dmxData", 29, 95, ";", ";", "#X", "insert", 0, "dmxData", 28, 95, ";", ";", "#X", "insert", 0, "dmxData", 27, 95, ";", ";", "#X", "insert", 0, "dmxData", 26, 95, ";", ";", "#X", "insert", 62, "dmxData", 36, 156, ";", ";", "#X", "insert", 0, "dmxData", 35, 156, ";", ";", "#X", "insert", 0, "dmxData", 34, 156, ";", ";", "#X", "insert", 0, "dmxData", 33, 156, ";", ";", "#X", "insert", 0, "dmxData", 32, 156, ";", ";", "#X", "insert", 0, "dmxData", 31, 156, ";", ";", "#X", "insert", 0, "dmxData", 30, 156, ";", ";", "#X", "insert", 0, "dmxData", 29, 156, ";", ";", "#X", "insert", 0, "dmxData", 28, 156, ";", ";", "#X", "insert", 0, "dmxData", 27, 156, ";", ";", "#X", "insert", 0, "dmxData", 26, 156, ";", ";", "#X", "insert", 0, "dmxData", 20, 156, ";", ";", "#X", "insert", 0, "dmxData", 14, 156, ";", ";", "#X", "insert", 0, "dmxData", 13, 156, ";", ";", "#X", "insert", 2, "dmxData", 12, 156, ";", ";", "#X", "insert", 0, "dmxData", 11, 156, ";", ";", "#X", "insert", 0, "dmxData", 10, 156, ";", ";", "#X", "insert", 2, "dmxData", 25, 156, ";", ";", "#X", "insert", 0, "dmxData", 24, 156, ";", ";", "#X", "insert", 0, "dmxData", 19, 156, ";", ";", "#X", "insert", 0, "dmxData", 18, 156, ";", ";", "#X", "insert", 0, "dmxData", 17, 156, ";", ";", "#X", "insert", 0, "dmxData", 16, 156, ";", ";", "#X", "insert", 0, "dmxData", 15, 156, ";", ";", "#X", "insert", 0, "dmxData", 9, 156, ";", ";", "#X", "insert", 0, "dmxData", 8, 156, ";", ";", "#X", "insert", 0, "dmxData", 7, 156, ";", ";", "#X", "insert", 0, "dmxData", 6, 156, ";", ";", "#X", "insert", 0, "dmxData", 5, 156, ";", ";", "#X", "insert", 0, "dmxData", 4, 156, ";", ";", "#X", "insert", 0, "dmxData", 3, 156, ";", ";", "#X", "insert", 0, "dmxData", 2, 156, ";", ";", "#X", "insert", 0, "dmxData", 1, 156, ";", ";", "#X", "insert", 7, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 74, "dmxData", 36, 254, ";", ";", "#X", "insert", 41, "dmxData", 35, 254, ";", ";", "#X", "insert", 0, "dmxData", 34, 254, ";", ";", "#X", "insert", 0, "dmxData", 33, 254, ";", ";", "#X", "insert", 0, "dmxData", 32, 254, ";", ";", "#X", "insert", 0, "dmxData", 31, 254, ";", ";", "#X", "insert", 0, "dmxData", 30, 254, ";", ";", "#X", "insert", 0, "dmxData", 29, 254, ";", ";", "#X", "insert", 0, "dmxData", 28, 254, ";", ";", "#X", "insert", 0, "dmxData", 27, 254, ";", ";", "#X", "insert", 0, "dmxData", 26, 254, ";", ";", "#X", "insert", 0, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 14, 254, ";", ";", "#X", "insert", 0, "dmxData", 13, 254, ";", ";", "#X", "insert", 0, "dmxData", 12, 254, ";", ";", "#X", "insert", 0, "dmxData", 11, 254, ";", ";", "#X", "insert", 0, "dmxData", 10, 254, ";", ";", "#X", "insert", 0, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 24, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 49, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 0, "dmxData", 15, 254, ";", ";", "#X", "insert", 0, "dmxData", 9, 254, ";", ";", "#X", "insert", 0, "dmxData", 8, 254, ";", ";", "#X", "insert", 0, "dmxData", 7, 254, ";", ";", "#X", "insert", 0, "dmxData", 6, 254, ";", ";", "#X", "insert", 0, "dmxData", 5, 254, ";", ";", "#X", "insert", 0, "dmxData", 4, 254, ";", ";", "#X", "insert", 0, "dmxData", 3, 254, ";", ";", "#X", "insert", 0, "dmxData", 2, 254, ";", ";", "#X", "insert", 0, "dmxData", 1, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 7, "dmxData", 36, 254, ";", ";", "#X", "insert", 42, "dmxData", 35, 248, ";", ";", "#X", "insert", 0, "dmxData", 34, 248, ";", ";", "#X", "insert", 0, "dmxData", 33, 248, ";", ";", "#X", "insert", 0, "dmxData", 32, 248, ";", ";", "#X", "insert", 0, "dmxData", 31, 248, ";", ";", "#X", "insert", 0, "dmxData", 30, 248, ";", ";", "#X", "insert", 0, "dmxData", 29, 248, ";", ";", "#X", "insert", 0, "dmxData", 28, 248, ";", ";", "#X", "insert", 0, "dmxData", 27, 248, ";", ";", "#X", "insert", 0, "dmxData", 26, 248, ";", ";", "#X", "insert", 0, "dmxData", 25, 248, ";", ";", "#X", "insert", 0, "dmxData", 24, 248, ";", ";", "#X", "insert", 0, "dmxData", 20, 248, ";", ";", "#X", "insert", 0, "dmxData", 19, 248, ";", ";", "#X", "insert", 0, "dmxData", 18, 248, ";", ";", "#X", "insert", 0, "dmxData", 14, 248, ";", ";", "#X", "insert", 0, "dmxData", 13, 248, ";", ";", "#X", "insert", 0, "dmxData", 12, 248, ";", ";", "#X", "insert", 0, "dmxData", 11, 248, ";", ";", "#X", "insert", 0, "dmxData", 10, 248, ";", ";", "#X", "insert", 21, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 3, "dmxData", 2, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 0, "dmxData", 36, 0, ";", ";", "#X", "insert", -4, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 25, 0, ";", ";", "#X", "insert", 64, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 3, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 2, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 5, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 0, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 0, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 12, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 45, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 10, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 41, "dmxData", 1, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 0, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 0, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 2, "dmxData", 25, 117, ";", ";", "#X", "insert", 0, "dmxData", 23, 117, ";", ";", "#X", "insert", 0, "dmxData", 22, 117, ";", ";", "#X", "insert", 0, "dmxData", 21, 117, ";", ";", "#X", "insert", 0, "dmxData", 17, 117, ";", ";", "#X", "insert", 0, "dmxData", 16, 117, ";", ";", "#X", "insert", 0, "dmxData", 15, 117, ";", ";", "#X", "insert", 0, "dmxData", 9, 117, ";", ";", "#X", "insert", 0, "dmxData", 8, 117, ";", ";", "#X", "insert", 0, "dmxData", 7, 117, ";", ";", "#X", "insert", 0, "dmxData", 6, 117, ";", ";", "#X", "insert", 0, "dmxData", 5, 117, ";", ";", "#X", "insert", 2, "dmxData", 4, 117, ";", ";", "#X", "insert", 0, "dmxData", 3, 117, ";", ";", "#X", "insert", 0, "dmxData", 2, 117, ";", ";", "#X", "insert", 3, "dmxData", 36, 149, ";", ";", "#X", "insert", 1, "dmxData", 35, 149, ";", ";", "#X", "insert", 0, "dmxData", 34, 149, ";", ";", "#X", "insert", 0, "dmxData", 33, 149, ";", ";", "#X", "insert", 0, "dmxData", 32, 149, ";", ";", "#X", "insert", 0, "dmxData", 31, 149, ";", ";", "#X", "insert", 0, "dmxData", 30, 149, ";", ";", "#X", "insert", 0, "dmxData", 29, 149, ";", ";", "#X", "insert", 0, "dmxData", 28, 149, ";", ";", "#X", "insert", 0, "dmxData", 27, 149, ";", ";", "#X", "insert", 0, "dmxData", 26, 149, ";", ";", "#X", "insert", 12, "dmxData", 24, 254, ";", ";", "#X", "insert", 0, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 0, "dmxData", 14, 254, ";", ";", "#X", "insert", 0, "dmxData", 13, 254, ";", ";", "#X", "insert", 0, "dmxData", 12, 254, ";", ";", "#X", "insert", 0, "dmxData", 11, 254, ";", ";", "#X", "insert", 0, "dmxData", 10, 254, ";", ";", "#X", "insert", 0, "dmxData", 1, 254, ";", ";", "#X", "insert", 2, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 0, "dmxData", 15, 254, ";", ";", "#X", "insert", 0, "dmxData", 9, 254, ";", ";", "#X", "insert", 0, "dmxData", 8, 254, ";", ";", "#X", "insert", 0, "dmxData", 7, 254, ";", ";", "#X", "insert", 0, "dmxData", 6, 254, ";", ";", "#X", "insert", 0, "dmxData", 5, 254, ";", ";", "#X", "insert", 2, "dmxData", 4, 254, ";", ";", "#X", "insert", 0, "dmxData", 3, 254, ";", ";", "#X", "insert", 0, "dmxData", 2, 254, ";", ";", "#X", "insert", 3, "dmxData", 36, 254, ";", ";", "#X", "insert", 1, "dmxData", 35, 254, ";", ";", "#X", "insert", 0, "dmxData", 34, 254, ";", ";", "#X", "insert", 0, "dmxData", 33, 254, ";", ";", "#X", "insert", 0, "dmxData", 32, 254, ";", ";", "#X", "insert", 0, "dmxData", 31, 254, ";", ";", "#X", "insert", 0, "dmxData", 30, 254, ";", ";", "#X", "insert", 0, "dmxData", 29, 254, ";", ";", "#X", "insert", 0, "dmxData", 28, 254, ";", ";", "#X", "insert", 0, "dmxData", 27, 254, ";", ";", "#X", "insert", 0, "dmxData", 26, 254, ";", ";", "#X", "insert", 12, "dmxData", 24, 254, ";", ";", "#X", "insert", 0, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 0, "dmxData", 14, 254, ";", ";", "#X", "insert", 0, "dmxData", 13, 254, ";", ";", "#X", "insert", 0, "dmxData", 12, 254, ";", ";", "#X", "insert", 0, "dmxData", 11, 254, ";", ";", "#X", "insert", 0, "dmxData", 10, 254, ";", ";", "#X", "insert", 73, "dmxData", 1, 254, ";", ";", "#X", "insert", 1, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 0, "dmxData", 15, 254, ";", ";", "#X", "insert", 0, "dmxData", 9, 254, ";", ";", "#X", "insert", 0, "dmxData", 8, 254, ";", ";", "#X", "insert", 0, "dmxData", 7, 254, ";", ";", "#X", "insert", 0, "dmxData", 6, 254, ";", ";", "#X", "insert", 0, "dmxData", 5, 254, ";", ";", "#X", "insert", 2, "dmxData", 4, 254, ";", ";", "#X", "insert", 0, "dmxData", 3, 254, ";", ";", "#X", "insert", 0, "dmxData", 2, 254, ";", ";", "#X", "insert", 0, "dmxData", 36, 254, ";", ";", "#X", "insert", 0, "dmxData", 35, 254, ";", ";", "#X", "insert", 0, "dmxData", 34, 254, ";", ";", "#X", "insert", 0, "dmxData", 33, 254, ";", ";", "#X", "insert", 0, "dmxData", 32, 254, ";", ";", "#X", "insert", 2, "dmxData", 31, 254, ";", ";", "#X", "insert", 0, "dmxData", 30, 254, ";", ";", "#X", "insert", 0, "dmxData", 29, 254, ";", ";", "#X", "insert", 0, "dmxData", 28, 254, ";", ";", "#X", "insert", 0, "dmxData", 27, 254, ";", ";", "#X", "insert", 0, "dmxData", 26, 254, ";", ";", "#X", "insert", 2, "dmxData", 24, 254, ";", ";", "#X", "insert", 0, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 0, "dmxData", 14, 254, ";", ";", "#X", "insert", 0, "dmxData", 13, 254, ";", ";", "#X", "insert", 0, "dmxData", 12, 254, ";", ";", "#X", "insert", 0, "dmxData", 11, 254, ";", ";", "#X", "insert", 0, "dmxData", 10, 254, ";", ";", "#X", "insert", 13, "dmxData", 1, 254, ";", ";", "#X", "insert", 1, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 0, "dmxData", 15, 254, ";", ";", "#X", "insert", 0, "dmxData", 9, 254, ";", ";", "#X", "insert", 0, "dmxData", 8, 254, ";", ";", "#X", "insert", 0, "dmxData", 7, 254, ";", ";", "#X", "insert", 0, "dmxData", 6, 254, ";", ";", "#X", "insert", 0, "dmxData", 5, 254, ";", ";", "#X", "insert", 2, "dmxData", 36, 254, ";", ";", "#X", "insert", 0, "dmxData", 35, 254, ";", ";", "#X", "insert", 0, "dmxData", 34, 254, ";", ";", "#X", "insert", 0, "dmxData", 33, 254, ";", ";", "#X", "insert", 0, "dmxData", 32, 254, ";", ";", "#X", "insert", 0, "dmxData", 4, 254, ";", ";", "#X", "insert", 0, "dmxData", 3, 254, ";", ";", "#X", "insert", 0, "dmxData", 2, 254, ";", ";", "#X", "insert", 2, "dmxData", 31, 254, ";", ";", "#X", "insert", 0, "dmxData", 30, 254, ";", ";", "#X", "insert", 0, "dmxData", 29, 254, ";", ";", "#X", "insert", 0, "dmxData", 28, 254, ";", ";", "#X", "insert", 0, "dmxData", 27, 254, ";", ";", "#X", "insert", 0, "dmxData", 26, 254, ";", ";", "#X", "insert", 2, "dmxData", 24, 254, ";", ";", "#X", "insert", 0, "dmxData", 20, 254, ";", ";", "#X", "insert", 0, "dmxData", 19, 254, ";", ";", "#X", "insert", 0, "dmxData", 18, 254, ";", ";", "#X", "insert", 0, "dmxData", 14, 254, ";", ";", "#X", "insert", 0, "dmxData", 13, 254, ";", ";", "#X", "insert", 0, "dmxData", 12, 254, ";", ";", "#X", "insert", 0, "dmxData", 11, 254, ";", ";", "#X", "insert", 0, "dmxData", 10, 254, ";", ";", "#X", "insert", 56, "dmxData", 1, 254, ";", ";", "#X", "insert", 0, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 31, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 0, "dmxData", 1, 254, ";", ";", "#X", "insert", 37, "dmxData", 25, 254, ";", ";", "#X", "insert", 0, "dmxData", 23, 254, ";", ";", "#X", "insert", 0, "dmxData", 22, 254, ";", ";", "#X", "insert", 0, "dmxData", 21, 254, ";", ";", "#X", "insert", 0, "dmxData", 17, 254, ";", ";", "#X", "insert", 0, "dmxData", 16, 254, ";", ";", "#X", "insert", 0, "dmxData", 1, 254, ";", ";", "#X", "insert", 3, "dmxData", 15, 193, ";", ";", "#X", "insert", 0, "dmxData", 9, 193, ";", ";", "#X", "insert", 0, "dmxData", 8, 193, ";", ";", "#X", "insert", 0, "dmxData", 7, 193, ";", ";", "#X", "insert", 0, "dmxData", 6, 193, ";", ";", "#X", "insert", 0, "dmxData", 5, 193, ";", ";", "#X", "insert", 0, "dmxData", 36, 193, ";", ";", "#X", "insert", 1, "dmxData", 35, 193, ";", ";", "#X", "insert", 0, "dmxData", 34, 193, ";", ";", "#X", "insert", 0, "dmxData", 33, 193, ";", ";", "#X", "insert", 0, "dmxData", 32, 193, ";", ";", "#X", "insert", 0, "dmxData", 4, 193, ";", ";", "#X", "insert", 0, "dmxData", 3, 193, ";", ";", "#X", "insert", 0, "dmxData", 2, 193, ";", ";", "#X", "insert", 0, "dmxData", 31, 193, ";", ";", "#X", "insert", 0, "dmxData", 30, 193, ";", ";", "#X", "insert", 0, "dmxData", 29, 193, ";", ";", "#X", "insert", 0, "dmxData", 28, 193, ";", ";", "#X", "insert", 0, "dmxData", 27, 193, ";", ";", "#X", "insert", 0, "dmxData", 26, 193, ";", ";", "#X", "insert", 0, "dmxData", 24, 193, ";", ";", "#X", "insert", 0, "dmxData", 20, 193, ";", ";", "#X", "insert", 3, "dmxData", 19, 193, ";", ";", "#X", "insert", 0, "dmxData", 18, 193, ";", ";", "#X", "insert", 0, "dmxData", 14, 193, ";", ";", "#X", "insert", 0, "dmxData", 13, 193, ";", ";", "#X", "insert", 0, "dmxData", 12, 193, ";", ";", "#X", "insert", 0, "dmxData", 11, 193, ";", ";", "#X", "insert", 0, "dmxData", 10, 193, ";", ";", "#X", "insert", 13, "dmxData", 25, 161, ";", ";", "#X", "insert", 0, "dmxData", 23, 161, ";", ";", "#X", "insert", 0, "dmxData", 22, 161, ";", ";", "#X", "insert", 0, "dmxData", 21, 161, ";", ";", "#X", "insert", 0, "dmxData", 17, 161, ";", ";", "#X", "insert", 0, "dmxData", 16, 161, ";", ";", "#X", "insert", 0, "dmxData", 1, 161, ";", ";", "#X", "insert", 3, "dmxData", 36, 161, ";", ";", "#X", "insert", 0, "dmxData", 15, 161, ";", ";", "#X", "insert", 0, "dmxData", 9, 161, ";", ";", "#X", "insert", 0, "dmxData", 8, 161, ";", ";", "#X", "insert", 0, "dmxData", 7, 161, ";", ";", "#X", "insert", 0, "dmxData", 6, 161, ";", ";", "#X", "insert", 0, "dmxData", 5, 161, ";", ";", "#X", "insert", 1, "dmxData", 35, 139, ";", ";", "#X", "insert", 0, "dmxData", 34, 139, ";", ";", "#X", "insert", 0, "dmxData", 33, 139, ";", ";", "#X", "insert", 0, "dmxData", 32, 139, ";", ";", "#X", "insert", 0, "dmxData", 31, 139, ";", ";", "#X", "insert", 0, "dmxData", 30, 139, ";", ";", "#X", "insert", 0, "dmxData", 29, 139, ";", ";", "#X", "insert", 0, "dmxData", 28, 139, ";", ";", "#X", "insert", 0, "dmxData", 27, 139, ";", ";", "#X", "insert", 0, "dmxData", 26, 139, ";", ";", "#X", "insert", 0, "dmxData", 24, 139, ";", ";", "#X", "insert", 0, "dmxData", 20, 139, ";", ";", "#X", "insert", 0, "dmxData", 4, 139, ";", ";", "#X", "insert", 0, "dmxData", 3, 139, ";", ";", "#X", "insert", 0, "dmxData", 2, 139, ";", ";", "#X", "insert", 3, "dmxData", 19, 78, ";", ";", "#X", "insert", 0, "dmxData", 18, 78, ";", ";", "#X", "insert", 0, "dmxData", 14, 78, ";", ";", "#X", "insert", 0, "dmxData", 13, 78, ";", ";", "#X", "insert", 0, "dmxData", 12, 78, ";", ";", "#X", "insert", 0, "dmxData", 11, 78, ";", ";", "#X", "insert", 0, "dmxData", 10, 78, ";", ";", "#X", "insert", 69, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 14, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 3, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 2, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 2, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", -1, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 14, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 3, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 2, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 1, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 1, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 74, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 0, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 0, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 2, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 4, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 39, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 2, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 55, "dmxData", 25, 0, ";", ";", "#X", "insert", 0, "dmxData", 24, 0, ";", ";", "#X", "insert", 0, "dmxData", 23, 0, ";", ";", "#X", "insert", 0, "dmxData", 22, 0, ";", ";", "#X", "insert", 0, "dmxData", 21, 0, ";", ";", "#X", "insert", 0, "dmxData", 20, 0, ";", ";", "#X", "insert", 0, "dmxData", 19, 0, ";", ";", "#X", "insert", 0, "dmxData", 18, 0, ";", ";", "#X", "insert", 0, "dmxData", 17, 0, ";", ";", "#X", "insert", 0, "dmxData", 16, 0, ";", ";", "#X", "insert", 0, "dmxData", 14, 0, ";", ";", "#X", "insert", 40, "dmxData", 13, 0, ";", ";", "#X", "insert", 0, "dmxData", 12, 0, ";", ";", "#X", "insert", 0, "dmxData", 11, 0, ";", ";", "#X", "insert", 0, "dmxData", 10, 0, ";", ";", "#X", "insert", 0, "dmxData", 4, 0, ";", ";", "#X", "insert", 0, "dmxData", 3, 0, ";", ";", "#X", "insert", 0, "dmxData", 2, 0, ";", ";", "#X", "insert", 0, "dmxData", 1, 0, ";", ";", "#X", "insert", 2, "dmxData", 36, 0, ";", ";", "#X", "insert", 0, "dmxData", 15, 0, ";", ";", "#X", "insert", 0, "dmxData", 9, 0, ";", ";", "#X", "insert", 0, "dmxData", 8, 0, ";", ";", "#X", "insert", 0, "dmxData", 35, 0, ";", ";", "#X", "insert", 0, "dmxData", 34, 0, ";", ";", "#X", "insert", 0, "dmxData", 33, 0, ";", ";", "#X", "insert", 0, "dmxData", 32, 0, ";", ";", "#X", "insert", 0, "dmxData", 31, 0, ";", ";", "#X", "insert", 0, "dmxData", 30, 0, ";", ";", "#X", "insert", 0, "dmxData", 29, 0, ";", ";", "#X", "insert", 0, "dmxData", 28, 0, ";", ";", "#X", "insert", 0, "dmxData", 27, 0, ";", ";", "#X", "insert", 0, "dmxData", 26, 0, ";", ";", "#X", "insert", 0, "dmxData", 7, 0, ";", ";", "#X", "insert", 0, "dmxData", 6, 0, ";", ";", "#X", "insert", 0, "dmxData", 5, 0, ";", ";" ],
					"style" : "",
					"text" : "qlist"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 218.0, 498.0, 98.0, 22.0 ],
					"style" : "",
					"text" : "prepend append"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 381.5, 419.0, 74.0, 22.0 ],
					"style" : "",
					"text" : "prepend set"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 218.0, 468.0, 55.0, 22.0 ],
					"style" : "",
					"text" : "prepend"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 218.0, 278.0, 30.0, 22.0 ],
					"style" : "",
					"text" : "t l b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 360.0, 228.0, 50.0, 22.0 ],
					"style" : "",
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "float", "" ],
					"patching_rect" : [ 375.5, 309.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "timer"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 281.0, 44.0, 181.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 153.0, 55.0, 181.0, 20.0 ],
					"style" : "",
					"text" : "start / stop recording dmx data"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 249.0, 42.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 127.0, 53.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"border" : 1,
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"grad1" : [ 1.0, 1.0, 1.0, 0.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-48",
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 27.0, 6.0, 453.0, 158.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 8.0, 9.0, 331.0, 158.0 ],
					"proportion" : 0.39,
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-33", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 1 ],
					"source" : [ "obj-35", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-41", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-43", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 1 ],
					"source" : [ "obj-43", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-54", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 1 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-58", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 1 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "thru.maxpat",
				"bootpath" : "C74:/patchers/m4l/Pluggo for Live resources/patches",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "sw.maxpat",
				"bootpath" : "/Users/Shared/Max 7/Examples/max-tricks/send-receive-tricks/lib",
				"patcherrelativepath" : "../../../../Shared/Max 7/Examples/max-tricks/send-receive-tricks/lib",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
