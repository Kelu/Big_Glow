
var bpatcherInstances = [];

function genBpatchers(nbInstances)
{
	xPosInit = 20
	yPosInit = 100
	
	xPos = xPosInit
	yPos = yPosInit
	
	xPosPres = 20
	yPosPres = 100
	
	length = 500
	height = 37
	
	colIdx = 0
	
	maxInstPerCol = 12
	
	for(var i=1;i<=nbInstances;i++)
	{
		if(i!=1 && (i-1)%maxInstPerCol==0)
		{
			xPosPres += 1.2*length
			yPosPres = yPosInit
		}
			
		bpatcherInstances[i] = (
			this.patcher.newdefault(xPos,yPos,
			"bpatcher",
			"@name", "setLightPos",
			"@patching_rect", xPos, yPos, length, height,
			"@presentation_rect", xPosPres, yPosPres, length, height,
			"@border", "0",
			//"@varname", varname,
			"@args", i,
			"@orderfront", "1",
			//"@hint", varname,
			"@embed", "0",
			"@presentation", "1")
		);
		
		yPos += 40
		yPosPres += 40
	}
}

function delBpatchers()
{
	for(var i=1;i<=bpatcherInstances.length;i++)
	{
		post(bpatcherInstances[i])
		this.patcher.remove(bpatcherInstances[i])
	}
}

